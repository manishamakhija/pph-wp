<?php 
get_header(); ?>
<div class="container">
	<div class="about-value-wrap">
		<div class="container">
			<div class="title">
				<h2>404 </h2><br>
				<div class="title-sub-text"><br>
					<p><strong>THE PAGE YOU WERE LOOKING FOR DOESN'T EXIST.</strong></p>
					<p>YOU MAY HAVE MISTYPED THE ADDRESS OR THE PAGE MAY HAVE MOVED.</p>
				</div>		
			</div>
			
		</div>
	</div>
</div>
<?php get_footer(); ?>