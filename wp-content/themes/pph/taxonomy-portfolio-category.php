<?php 

get_header();
$term = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
global $wp_query; global $post;
$original_query = $wp_query;
?>
<div class="work-banner dot-pattern">
	<div class="container">
		<h1><?php echo get_the_title(7); ?></h1>
	</div>
	<div class="work-nav">
		<button class="filter-btn"><i class="fa fa-filter"></i> Filter</button>
		<div class="work-nav-list">
			<ul>
				<li><a href="<?php echo get_permalink(7); ?>">ALL</a></li><?php 
				$portfolio_terms = get_terms( 'portfolio-category',array('hide_empty'=>false) );
				if (!is_wp_error($portfolio_terms) ): 
					foreach($portfolio_terms as $pterm){ ?>
						<li class="<?php echo ($term->slug == $pterm->slug) ? "active" : ""; ?>"><a href="<?php echo get_term_link($pterm->term_id); ?>" ><?php echo $pterm->name; ?></a></li><?php 
					}
				endif; ?>
			</ul>
		</div>
	</div>
</div>

<div class="work-list-wrap bg-light-blue">
	<div class="container"><?php $counter = 1; 
		
		$work_query = new WP_Query(array('post_type'=>'portfolio','paged'=> ( get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1 ),"taxonomy"=>"portfolio-category","term"=>$term->slug));
		$wp_query = $work_query;
		if ($work_query->have_posts()): ?>
			<div class="work-list">
				<ul><?php 
					while($work_query->have_posts()):$work_query->the_post(); ?>
						<li data-hover-bg="#<?php the_field('background_color'); ?>">
							<?php $website_link = get_field('website_link'); ?>
							<a href="<?php echo ($website_link != '' ? $website_link : 'javascript:;'); ?>" <?php echo ($website_link != '' ? 'target="_blank"' : ''); ?>>
								<div class="work-img">
									<?php the_post_thumbnail(); ?>
								</div>
								<h3><?php the_title(); ?></h3>
								<div class="tags-cloud"><?php 
									$technologies = get_field('technologies_used');
									if (!empty($technologies)):
										foreach($technologies as $technology){
											echo '<span>'. $technology .'</span>';
										} 
									endif;
									?>
								</div>
								<div class="text">
									<?php the_excerpt(); ?>
								</div>
								<div class="cta-btn">
									<div class="cta-link cta-style2"><span>VISIT WEBSITE</span></div>
								</div>
							</a>
						</li><?php 
						if ($counter == 3 || $counter == 8):
							$args = array(
								  'numberposts' => 1,
								  'post_type'   => 'testimonial'
								); 
							$testimonial_blocks = get_posts($args); 
							foreach($testimonial_blocks as $testimonial){ ?>
								<li class="quote-block">
									<div class="quote-box">
										<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/quote-icon.png" alt="">
										<div class="text">
											<?php echo $testimonial->post_content; ?>
										</div>
										<h3><?php echo $testimonial->post_title; ?></h3>
									</div>
								</li><?php 
							}
						endif;
						if ($counter == get_option('posts_per_page')) $counter = 1;
						$counter++;
					endwhile; wp_reset_postdata();
					?>
				</ul>
			</div>
			<div class="pagination-wrap">
				<?php do_action('pph_pagination'); ?>
			</div><?php 
		else:
			echo '<h2 style="text-align: center;">No Post Found</h2>';
		endif; ?>
	</div>
</div>

<?php $wp_query = $original_query; wp_reset_postdata();
get_template_part('template-parts/cta','banner'); ?>
<?php get_footer();?>