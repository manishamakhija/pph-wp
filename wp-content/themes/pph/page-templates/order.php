<?php
/*
** Template Name: Order
*/

get_header();
?>

<div class="work-banner dot-pattern">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<div class="banner-sub-title"><?php the_field('sub_title'); ?></div>
	</div>
</div>

<div class="order-wrap bg-light-blue">
	<div class="container"  id="order-form-wrap">
		<form method="POST" name="order_form" id="order-form" class="order_form" enctype="multipart/form-data" v-on:submit.prevent="onSubmit">
			<div class="order-column" id="orderform">
				<div class="left" id="fullpage2">
					<div class="step-nav">
						<nav class="navbar">
							<ul class="nav nav-pills">
								<li class="nav-item">
									<a class="nav-link" href="#step1">Step <span>1</span></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#step2">Step <span>2</span></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#step3">Step <span>3</span></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#step4">Step <span>4</span></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#step5">Step <span>5</span></a>
								</li>
							</ul>
						</nav>
					</div>
					<div class="step1 step-box" id="step1">
						<div class="title text-left">
							<h2>1. SELECT SERVICE</h2>
						</div>
						<div class="form-wrap">
							<div class="quote-service-select single-select">
								<ul><?php $counter = 1;
									$service_list = new WP_Query(array('post_type'=>'product','posts_per_page'=>-1,'post__not_in' => array('269','270')));
									while($service_list->have_posts()):$service_list->the_post();
										$service_post_id = get_the_id(); ?>
										<li <?php echo $counter == 1 ? 'class="select"' : '' ?> @click="fetchServiceData(<?php echo $service_post_id; ?>)">
											<div class="block">
												<input type="radio" name="service" hidden v-model="service" value="<?php echo $service_post_id; ?>"  <?php echo $counter == 1 ? 'checked' : '' ?>>
												<?php $service_icon = get_field('service_icon'); ?>
												<div class="icon"><img src="<?php echo $service_icon['url']; ?>" alt="<?php echo $service_icon['alt']; ?>" class="svg"></div>
												<h4><?php the_title(); ?></h4>
											</div>
										</li><?php $counter++;
									endwhile; wp_reset_postdata(); ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="step2 step-box" id="step2">
						<div class="title text-left">
							<h2>2. SELECT PACKAGE</h2>
						</div>
						<div class="form-wrap">
							<div class="package-select">
								<ul><?php $package_count = 1;
									while(has_sub_field('packages')): ?>
										<li <?php echo $package_count == 1 ? 'class="select"' : '' ?>  @click="$refs.packageRadio<?php echo $package_count; ?>.click()">
											<input type="radio" name="package_name" value="<?php the_sub_field('title'); ?>" ref="packageRadio<?php echo $package_count; ?>" @click="packageChange" v-model="selectedPackage" hidden>
											<h3><?php the_sub_field('title'); ?></h3>
											<?php $package_image = get_sub_field('image'); ?>
											<div class="icon"><img src="<?php echo $package_image['url']; ?>" alt="<?php echo $package_image['alt']; ?>"></div>
											<div class="package-features">
												<ul>
													<?php if ($package_count == 1 ): ?>
														<li v-for="basic in basicFeatures">{{ basic }}</li>
													<?php else: ?>	
														<li><strong>All Basic Features plus</strong></li>
														<li v-for="premium in premiumFeatures">{{ premium }}</li><?php 
													endif; ?>
												</ul>
											</div>
											<div class="cta-btn text-center">
												<a href="javascript:;" class="cta-link cta-style2"><span>Read <i class="more">More</i> <i class="less">Less</i></span></a>
											</div>
										</li> <?php $package_count++;
									endwhile; ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="step3 step-box" id="step3">
						<div class="title text-left">
							<h2>3. PAGES & DELIVERY</h2>
						</div>
						<div class="number-of-pages">
							<h3>NUMBER OF PAGES</h3>
							<div class="qty">
								<span class="minus handlar" @click="pageCountChange(-1)">-</span>
								<input type="number" class="count" name="qty" value="1" min="1" v-model="pages" >
								<span class="plus handlar" @click="pageCountChange(1)">+</span>
							</div>
							<div class="total-page-count pages-breakdown">
								1 Homepage  <template v-if = "pages > 1">+ {{ innerPages }} inner page</template>
							</div>
						</div>
						<div class="delivery-time-select">
							<h4>DELIVERY TIME</h4>
							<ul>
								<li class="select" @click="$refs.stdDeliveryRadio.click()">
									<div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/icon-_standar-delivery.png" alt="tortoise"></div>
									<input type="radio" name="delivery" value="Standard" ref="stdDeliveryRadio" v-model="selectedDelivery" @click="deliveryTimeChange" hidden>
									<h3>STANDARD DELIVERY</h3>
								</li>
								<li @click="$refs.expressDeliveryRadio.click()">
									<div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/icon-_express-delivery.png" alt="rabbit"></div>
									<input type="radio" name="delivery" value="Express" ref="expressDeliveryRadio" v-model="selectedDelivery" @click="deliveryTimeChange" hidden>
									<h3>EXPRESS DELIVERY</h3>
								</li>
							</ul><?php
							/*$first_service = new WP_Query(array('p'=>97,'post_type'=>'product'));
							while($first_service->have_posts()):$first_service->the_post(); ?>
							<div class="total-page-count delivery-duration">
								<span><?php while(has_sub_field('standard_delivery')): the_sub_field('days'); endwhile; ?></span> business days for 1 page.
							</div><?php endwhile;*/ ?>
						</div>
					</div>
					<div class="step4 step-box" id="step4">
						<div class="title text-left">
							<h2>4. ENTER YOUR ORDER DETAILS</h2>
						</div>
						<?php if (is_user_logged_in()): 
							$current_user_id = get_current_user_id();
							$user_obj =  get_user_by('id',get_current_user_id());
							$user_email = $user_obj->user_email;
							$name = get_user_meta($current_user_id, 'first_name', true);
							$phone =  get_user_meta($current_user_id, 'billing_phone', true);
							$skype =  get_user_meta($current_user_id, 'skype', true);

						endif;  ?>
						<div class="form-wrap small-wrap">
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<input type="text" placeholder="Your Name*" name="name" class="form-control" v-validate="'required'" <?php echo (is_user_logged_in() && $name != '' ? 'value="'. $name .'"' : 'v-model="name"'); ?>>
										<span v-show="errors.has('name')" class="text-danger">{{ errors.first('name') }}</span>
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										
										<input type="text" placeholder="Email Address*" name="email"  class="form-control" v-validate="'required|email'" <?php echo (is_user_logged_in() ? 'value="'. $user_email .'" readonly' : 'v-model="email"'); ?>>
										<span v-show="errors.has('email')" class="text-danger">{{ errors.first('email') }}</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<input type="text" placeholder="Skype Name" name="skype_name" class="form-control" <?php echo (is_user_logged_in() && $skype != '' ? 'value="'. $skype .'"' : ''); ?>>
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<input type="tel" placeholder="Phone*" name="phone"  class="form-control" v-validate="'required|numeric'" <?php echo (is_user_logged_in() && $phone != '' ? 'value="'. $phone .'"' : 'v-model="phone"'); ?>>
										<span v-show="errors.has('phone')" class="text-danger">{{ errors.first('phone') }}</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<input type="text" placeholder="Project Name*" v-model="projectName" name="projectName" class="form-control" v-validate="'required'">
										<span v-show="errors.has('projectName')" class="text-danger">{{ errors.first('projectName') }}</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<textarea placeholder="Project Note" class="form-control" name="project_note"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<input type="file" class="my-pond" name="filepond[]" multiple />
									</div>
								</div>
							</div>
							<div class="form-note">Multiple files supported ( We take AI, PSD, PNG, JPG, TIFF, WEBP, SVG, PDF, SKETCH, EPS. )</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									<?php $portfolio_discount = get_field('portfolio_discount'); 
									$terms_page_id = wc_terms_and_conditions_page_id(); ?>
									<input type="checkbox" class="custom-control-input" name="use_in_portfolio" v-model="portfolioDiscount" id="customCheck3" <?php /*value="<?php echo $portfolio_discount; ?>"*/ ?> :value="calculatePortfolioCharges" @click="portfolioChargesAdded($event)">
									<label class="custom-control-label" for="customCheck3">You cannot use this project in your portfolio section. Read <a href="<?php echo get_permalink($terms_page_id); ?>" target="_blank">Terms of Service</a> for more detail. <?php /*<span>($<?php echo $portfolio_discount; ?> discount)</span> */ ?></label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="form-group promo-code-box">
										<input type="text" placeholder="Have a Coupon" name="coupon" class="form-control" v-model="couponCode" :readonly="couponApplied == true">
										<div class="cta-btn">
											<button class="cta-link cta-blue" name="coupon_applied_btn" @click.prevent="applyCoupon" :disabled="couponCode == '' || couponApplied == true">APPLY</button>
										</div>
									</div>
									<div class="discount-status" v-html="discountStatus"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="step5 step-box" id="step5">
						<div class="title text-left">
							<h2>5. PAYMENT OPTION</h2>
						</div>
						<div class="payment-selection">
							<ul>
								<li class="credit-card select" @click="$refs.stripePaymentRadio.click()"> 
									<input type="radio" name="payment_type" value="stripe" class="card-pay" v-model="paymentMethod" checked ref="stripePaymentRadio" hidden>
									<div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/icon-_creditcard-payment.png" alt=""></div>
									<h3>CREDIT CARD</h3>
								</li>
								
								<li class="paypal" @click="$refs.paypalPaymentRadio.click()">
									<input type="radio" name="payment_type" value="paypal" class="paypal-pay" v-model="paymentMethod" ref="paypalPaymentRadio" hidden>
									<div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/icon-paypal-payment.png" alt=""></div>
									<h3>PAYPAL</h3>

								</li> 
							</ul>
						</div>
						<div class="collapse show" id="credit-card-box">
							<div class="card-detail-add">
								<div class="form-wrap">
									<div class="credit-card-form">
										<div class="row">
											<div class="col-md-12 col-sm-12">
												<div class="form-group" id="card-no">
												</div>
											</div>
											<div class="col-md-12 col-sm-12">
												<div class="form-group">
													<input type="text" name="name_on_card" id="name_on_card" placeholder="Name on Card">
												</div>
											</div>
											<div class="col-md-6 col-sm-12">
												<div class="form-group"  id="cvv-no">
												</div>
											</div>
											<div class="col-md-6 col-sm-12">
												<div class="form-group" id="expiry-date">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="card-errors" class="text-danger"></div>
							</div>
						</div>
					</div>
				<div class="order-form-alert-msg"></div>
				</div>
				<div class="right">
					<div class="summary-box">
						<div class="selected-service"> <!-- v-bind:src="serviceIcon" -->
							<div class="icon"><img src="<?php echo get_field('service_icon',97)['url']; ?>" alt=""></div>
							<div class="s-name"><?php echo get_the_title(97); ?> </div>
						</div>
						<div class="selected-service-list">
							<ul>
								<li>
									<div class="service-name">
										<h6>NUMBER OF PAGES</h6>
										1 Homepage

									</div>
									<div class="service-price">
										+ ${{ homePagePrice }}
									</div>
									<div v-if = "pages > 1" class="selected-innerpage">
									<div class="service-name">{{ innerPages }} Inner page</div>
									<div class="service-price">
										+ ${{ innerPagePrice }}
									</div></div>
								</li>
								<li>
									<div class="service-name">
										<h6>PACKAGE</h6>
										{{ selectedPackage }}
									</div>
								</li>
								<li>
									<div class="service-name">
										<h6>TURNAROUND</h6>
										{{ selectedDelivery }}- {{ deliveryDays }} b. days
									</div>
									<div class="service-price">
										${{ expressPrice }}
									</div>
								</li>
								<li v-show="portfolioDiscount">
									<div class="service-name">
										<h6>SHOW IN PORTFOLIO</h6>
										Portfolio
									</div>
									<div class="service-price">
										+ ${{ calculatePortfolioCharges }}
									</div>
								</li>
								<li v-show="couponDiscount != 0">
									<div class="service-name">
										<h6>DISCOUNT CODE</h6>
										<span class="coupon-code">{{ couponCode }}</span> <a href="javascript:;" class="pph-remove-coupon" @click="removeCoupon">Remove</a>
									</div>
									<div class="service-price">
										- ${{ couponDiscount }}
									</div>
								</li>
							</ul>
						</div>
						<div class="service-total">
							<span class="tag">TOTAL</span>
							<span class="total">${{ totalCost }}</span>
							<input type="hidden" name="home_page" v-model="homePagePrice">
							<input type="hidden" name="single_inner_page" v-model="singleInnerPagePrice">
							<input type="hidden" name="inner_page" v-model="innerPagePrice">
							<input type="hidden" name="duration" v-model="deliveryDays">
							<input type="hidden" name="coupon_discount_amount" :value="couponDiscount">
							<input type="hidden" name="total_cost" :value="totalCost">
						</div>
					</div>
				</div>
			</div>
			<div class="order-submit-wrap">
				<div class="left">
					<div class="row align-items-center">
						<div class="col-xl-8 col-lg-6 col-md-6">
							<div class="form-wrap">
								<div class="form-group mb-md-0">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="agree_policy" id="customCheck5" v-model="agreePolicy" v-validate="'required'">
										<label class="custom-control-label" for="customCheck5">By Submitting your request, you agree with our <a href="<?php echo esc_url( get_permalink( get_option( 'wp_page_for_privacy_policy' ) ) ); ?>" target="_blank">Policy</a> </label>
										<span v-show="errors.has('agreePolicy')" class="text-danger">{{ errors.first('agreePolicy') }}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-6 col-md-6">
							<div class="cta-btn text-md-right">
								<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/ajax-loader.gif" class="ajax-loader avoid-lazyload" alt="loader" />
								<button class="cta-link" type="submit" id="place-order" :disabled="errors.any()"><span>PLACE ORDER</span></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<form method="post" action="https://www.sandbox.paypal.com/cgi-bin/webscr" id="paypal_form" class="paypal-button" target="_top">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="nicolecross1579@gmail.com">
            <input type="hidden" name="lc" value="US">
            <input type="hidden" name="item_name" value="">
            <input type="hidden" name="quantity" value="1">
            <input type="hidden" name="amount" :value="totalCost">
            <input type="hidden" name="currency_code" value="USD">
            <input type="hidden" name="button_subtype" value="services">
		    <input type="hidden" name="no_note" value="0">
		    <input type="hidden" name="bn" value="PP-BuyNowBF:order-now-btn.png:NonHostedGuest">
            <input type='hidden' name='first_name' :value="name">
            <input type='hidden' name='email' :value="email">
            <input type="hidden" name="custom" value=""/>
            <input type="hidden" name="notify_url" value="https://pixelperfecthtml.com/paypal-payment-status/">
            <input type="hidden" name="return" value="" /> 
            <input type="hidden" name="env" value="www">
        </form>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vee-validate/2.2.15/vee-validate.js"></script>

<?php get_footer(); ?>