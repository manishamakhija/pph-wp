<?php 
/*
** Template Name: Services
*/
get_header(); ?>
<div class="services-menu">
	<ul id="services-menu">
		<li class="active"><a href="#section0">Intro</a></li>
		<?php global $post; 
		$page_title = get_the_title(); $page_sub_title = $post->post_content;
		$product_list = new WP_Query(array('post_type'=>'product','posts_per_page'=>-1));
		while($product_list->have_posts()):$product_list->the_post(); ?>
			<li ><a href="#<?php echo sanitize_title(get_the_title()); ?>"><?php the_title(); ?></a></li><?php 
		endwhile; ?>
	</ul>
</div>

<div id="fullpage" class="dot-pattern">
	<div class="section fp-auto-height services-block" id="section0">
		<div class="intro">
			<div class="services-wrap">
				<h1 class="service-title"><?php echo $page_title; ?></h1>
				<?php echo $page_sub_title; ?>
			</div>
		</div>
	</div><?php $counter = 1; 
	while($product_list->have_posts()):$product_list->the_post(); 
		$service_id = get_the_id(); ?>
		<div class="section fp-auto-height services-block" id="<?php echo sanitize_title(get_the_title()); ?>" <?php echo ($counter % 2 != 0) ? 'data-bgcolor="#f9faff"' : ''; ?>>
			<div class="intro">
				<div class="services-wrap">
					<div class="left">
						<?php the_post_thumbnail(); ?>
					</div>
					<div class="right">
						<h3><?php the_title(); ?></h3>
						<?php echo $post->post_excerpt; ?>
						<div class="services-feature-box">
							<ul><?php $package_details = get_field('prices');
							$stardard_package_details = $package_details[0]['delivery_time'];
							if ( !empty ( $stardard_package_details ) ):
								foreach($stardard_package_details as $key => $value){ ?>
									<li>
										<ul>
											<li><?php echo $value['package_name'].' Package'; ?></li><?php 
												$page_type = $value['package_type'][0]['page_type'];
												foreach($page_type as $k => $v){   
													echo '<li>' . $v['page_name'] . ' Page : <strong>$' . $v['page_price'] . '</strong></li>';
												} ?>
										</ul>
									</li><?php 
								} 
							endif; ?>
							</ul>
						</div>

						<div class="cta-btn">
							<a href="<?php echo ( $service_id == 269 || $service_id == 270 ) ? get_permalink().'#maintenance-package' : get_permalink(36); ?>" class="cta-link"><span>ORDER NOW</span></a>
							<a href="<?php the_permalink(); ?>" class="cta-link cta-style2"><span>VIEW MORE</span></a>
						</div>
					</div>
				</div>
			</div>
		</div><?php $counter++;
	endwhile; wp_reset_postdata(); ?>
</div>
<?php 
if (!is_user_logged_in()){
	echo get_template_part('template-parts/newsletter','form'); 
} ?>
<?php get_footer(); ?>