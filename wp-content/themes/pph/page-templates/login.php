<?php
/*
** Template Name: Login
*/

if ( is_user_logged_in() ){
	wp_redirect(get_permalink(249230)); // dashboard projects list
	exit;
}
get_header();
?>

		<div class="login-page-wrap dot-pattern blue w-40">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/login-lego.png" alt="pph-logo" class="d-md-block d-none">
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="login-box">
							<div class="form-wrap login-form">
								<!-- title start -->
								<div class="title text-left">
									<h2>LOGIN</h2>
								</div>
								<!-- title end -->
								<div class="msg-status"></div>
								<form method="post" name="login_form" action="" id="login_form_page" class="login_form">
									<?php wp_nonce_field(); ?>
									<div class="form-group">
										<div class="input-icon">
											<input type="text" class="form-control" name="login_username" placeholder="E-mail">
											<!-- <input type="text" class="form-control" placeholder="Username or e-mail"> -->
											<span class="icon-input"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/user.svg" alt="" class="svg"></span>
										</div>
									</div>
									<div class="form-group">
										<div class="input-icon">
											<input type="Password" class="form-control" name="login_password" placeholder="Password">
											<span class="icon-input"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/lock.svg" alt="" class="svg"></span>
										</div>
									</div>
									<div class="form-group">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="remember_me" id="remember_me">
											<!-- <input type="checkbox" class="custom-control-input" id="customCheck12"> -->
											<label class="custom-control-label" for="remember_me">Remember me</label>
										</div>
									</div>
									<div class="row align-items-center">
										<div class="col">
											<a href="<?php echo wp_lostpassword_url(); ?>">Forgot Password?</a>
										</div>
										<div class="col">
											<div class="submit-btn cta-btn">
												<!-- <button type="submit" class="cta-link btn-block">LOGIN</button> -->
												<button type="submit" class="cta-link btn-block login_submit">LOGIN</button>
											</div>
										</div>
									</div>
								</form>

							</div>
							<div class="cta-btn text-center">
								<a href="<?php echo get_site_url(); ?>" class="cta-link cta-style2"><span>BACK TO HOME</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>


	<?php wp_footer(); ?>
</body>
</html>