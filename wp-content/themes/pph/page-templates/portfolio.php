<?php
/*
** Template Name: Portfolio
*/
get_header();
global $wp_query; global $post;
$original_query = $wp_query;
?>
<div class="work-banner dot-pattern">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<div class="banner-sub-title"><?php the_field('sub_title'); ?></div>
	</div>
	<div class="work-nav">
		<button class="filter-btn"><i class="fa fa-filter"></i> Filter</button>
		<div class="work-nav-list">
			<ul>
				<li class="active"><a href="javascript:;">ALL</a></li><?php
				$portfolio_terms = get_terms( 'portfolio-category',array('hide_empty'=>false) );
				if (!is_wp_error($portfolio_terms) ):
					foreach($portfolio_terms as $term){ ?>
						<li><a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a></li><?php
					}
				endif; ?>
			</ul>
		</div>
	</div>
</div>

<div class="work-list-wrap bg-light-blue">
	<div class="container">
		<div class="work-list">
			<ul><?php $counter = 1; $exclude_testimonial = array();
				$work_query = new WP_Query(array('post_type'=>'portfolio','paged'=> ( get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1 ) ));
				$wp_query = $work_query;
				while($work_query->have_posts()):$work_query->the_post(); ?>
					<li data-hover-bg="#<?php the_field('background_color'); ?>">
						<?php $website_link = get_field('website_link'); ?>
							<a href="<?php echo ($website_link != '' ? $website_link : 'javascript:;'); ?>" <?php echo ($website_link != '' ? 'target="_blank"' : ''); ?>>
							<div class="work-img">
								<?php the_post_thumbnail(); ?>
							</div>
							<h3><?php the_title(); ?></h3>
							<div class="tags-cloud"><?php
								$technologies = get_field('technologies_used');
								if (!empty($technologies)):
									foreach($technologies as $technology){
										echo '<span>'. $technology .'</span>';
									}
								endif;
								?>
							</div>
							<div class="text">
								<?php the_excerpt(); ?>
							</div>
							<div class="cta-btn">
								<div class="cta-link cta-style2"><span>VISIT WEBSITE</span></div>
							</div>
						</a>
					</li><?php
					if ($counter == 3 || $counter == 9):
						$args = array(
							  'numberposts' => 1,
							  'post_type'   => 'testimonial',
							  'orderby' => 'rand',
							  'post__not_in' => $exclude_testimonial
							);
						$testimonial_blocks = get_posts($args);
						foreach($testimonial_blocks as $testimonial){ ?>
							<li class="quote-block">
								<div class="quote-box">
									<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/quote-icon.png" alt="">
									<div class="text">
										<?php echo $testimonial->post_content; ?>
									</div>
									<h3><?php echo $testimonial->post_title; ?></h3>
								</div>
							</li><?php 
							array_push( $exclude_testimonial, $testimonial->ID );
						}
					endif;
					if ($counter == get_option('posts_per_page')) $counter = 1;
					$counter++;
					endwhile;  wp_reset_postdata(); ?>

			</ul>
		</div>
		<div class="pagination-wrap">
			<?php do_action('pph_pagination'); ?>
		</div>
	</div>
</div>

<?php $wp_query = $original_query; wp_reset_postdata();
get_template_part('template-parts/cta','banner'); ?>
<?php get_footer(); ?>