<?php
/*
** Template Name: About Us
*/
get_header(); ?>

<div class="work-banner dot-pattern">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<div class="banner-sub-title"><?php the_field('sub_title'); ?></div>
	</div>
</div>

<div class="about-intro bg-light-blue">
	<div class="container">
		<div class="title text-center"><h2>Pixel Perfect HTML Helps Your Business Grow Exponentially</h2></div>
		<div class="row align-items-center">
			<div class="col-md-6">
				<?php while(have_posts()):the_post();
					the_content();
				endwhile; ?>
			</div>
			<div class="col-md-6 text-center">
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
	</div>
</div>

<div class="about-value-wrap">
	<div class="container">
		<div class="title">
			<?php the_field('title_&_desc'); ?>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical"><?php $tabs = 1;
					while(has_sub_field('value_tabs')): ?>
						<a class="nav-link <?php echo ($tabs == 1 ? 'active' : ''); ?>" id="v-pills-home-tab<?php echo $tabs; ?>" data-toggle="pill" href="#v-pills-home<?php echo $tabs; ?>" role="tab" aria-controls="v-pills-home<?php echo $tabs; ?>" aria-selected="true">
							<h5><?php the_sub_field('title'); ?></h5>
							<?php the_sub_field('desc'); ?>
						</a><?php $tabs++;
					endwhile; ?>
				</div>
			</div>
			<div class="col-md-7">
				<div class="tab-content" id="v-pills-tabContent"><?php $tab_content_count = 1;
					while(has_sub_field('value_tabs')): ?>
						<div class="tab-pane fade <?php echo ($tab_content_count == 1 ? 'show active' : ''); ?>" id="v-pills-home<?php echo $tab_content_count; ?>" role="tabpanel" aria-labelledby="v-pills-home-tab<?php echo $tab_content_count; ?>">
							<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>">
						</div><?php $tab_content_count++;
					endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="why-wrap bg-light-blue">
	<div class="container">
		<div class="title">
			<h2>WHY?</h2>
		</div>
		<div class="why-list">
			<ul><?php
				while(has_sub_field('why_pph')): ?>
					<li>
						<div class="row">
							<div class="col-md-4">
								<h5><?php the_sub_field('feature_title'); ?></h5>
							</div>
							<div class="col-md-8">
								<?php the_sub_field('feature_description'); ?>
							</div>
						</div>
					</li><?php
				endwhile; ?>
			</ul>
		</div>
	</div>
</div>

<div class="statistics-wrap">
	<div class="container">
		<ul><?php
			while(has_sub_field('statistics_&_clients')):
				if (get_row_layout() == 'statistics'): ?>
					<li>
						<div class="icon-box">
							<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>">
						</div>
						<?php the_sub_field('figures'); ?>
					</li><?php
				endif;
			endwhile; ?>
		</ul>
	</div>
</div>

<div class="client-list-wrap bg-light-blue">
	<div class="container">
		<div class="title">
			<h2>OUR CLIENTS</h2>
		</div>
		<div class="client-list">
			<ul><?php
				while(has_sub_field('statistics_&_clients')):
					if (get_row_layout() == 'our_clients'): ?>
						<li><img src="<?php echo get_sub_field('clients_logo')['url']; ?>" alt="<?php echo get_sub_field('clients_logo')['alt']; ?>"></li><?php
					endif;
				endwhile; ?>
			</ul>
		</div>
	</div>
</div>

<?php echo get_template_part('template-parts/cta','banner'); ?>

<?php get_footer(); ?>