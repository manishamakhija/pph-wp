<?php 
/*
** Template Name: Quick Quote
*/

get_header();

?>
<div class="service-quote-wrap quote-page-wrap">
	<div class="container">
		<div class="title">
			<h2>GET A QUICK QUOTE</h2>
			<div class="title-sub-text small-text-block">
				Select the service you are interested in and fill out the form below. We will get back to you within the next 24 hours.
			</div>
		</div>
		<form name="quick_quote" id="quick_quote" class="quick_quote multiple_uploads_form" enctype="multipart/form-data" method="POST">
			<div class="form-wrap">
				<div class="quote-service-select single-select">
					<ul><?php $counter = 1;
						$service_list = new WP_Query(array('post_type'=>'product','posts_per_page'=>-1,'post__not_in' => array('269','270')));
						while($service_list->have_posts()):$service_list->the_post(); ?>
							<li <?php echo $counter == 1 ? 'class="select"' : '' ?>>
								<div class="block">
									<input type="radio" name="service" value="<?php the_title(); ?>" hidden <?php echo $counter == 1 ? 'checked' : '' ?>>
									<?php $service_icon = get_field('service_icon'); ?>
									<div class="icon"><img src="<?php echo $service_icon['url']; ?>" alt="<?php echo $service_icon['alt']; ?>" class="svg"></div>
									<h4><?php the_title(); ?></h4>
								</div>
							</li><?php $counter++;
						endwhile; wp_reset_postdata(); ?>
					</ul>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<input type="text" placeholder="Your Name*" class="form-control required" name="name">
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<input type="text" placeholder="Email Address*" name="email" class="form-control required email">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="form-group">
							<textarea placeholder="Message" class="form-control" name="message"></textarea>
						</div>
					</div>
				</div>
				<div class="form-note">We keep all information confidential and automatically agree to NDA.</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="form-group">
							<input type="file" class="my-pond" name="filepond[]" multiple />
						</div>
					</div>
				</div>
				<div class="form-note">Multiple files supported ( We take AI, PSD, PNG, JPG, TIFF, WEBP, SVG, PDF, SKETCH, EPS. ).</div>
				<div class="form-group">
					<div class="cta-btn text-center">
						<button class="cta-link btn-block quick-quote-submit" type="submit" name="submit">SEND</button>
						<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/ajax-loader.gif" class="ajax-loader" alt="loader" />
						<input type="hidden" value="quick quote" name="form_type">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<?php echo get_template_part('template-parts/cta','banner'); ?>

<?php get_footer(); ?>