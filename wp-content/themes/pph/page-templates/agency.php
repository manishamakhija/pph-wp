<?php 
/*
** Template Name: Agency
*/
get_header(); ?>
<div class="work-banner dot-pattern">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<div class="banner-sub-title"><?php the_field('sub_title'); ?></div>
	</div>
</div>

<div class="service-intro bg-light-blue">
	<div class="container">
		<?php while(have_posts()):the_post();
			the_content();
		endwhile; ?>
	</div>
</div>

<div class="about-value-wrap">
	<div class="container">
		<div class="title">
			<h2>Ensuring Dedicated Assistance to Our Partners</h2>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical"><?php $tabs = 1;
					while(has_sub_field('pros_of_business')): ?>
						<a class="nav-link <?php echo ($tabs == 1 ? 'active' : ''); ?>" id="v-pills-home-tab<?php echo $tabs; ?>" data-toggle="pill" href="#v-pills-home<?php echo $tabs; ?>" role="tab" aria-controls="v-pills-home<?php echo $tabs; ?>" aria-selected="true">
							<?php the_sub_field('text'); ?>
						</a><?php $tabs++;
					endwhile; ?>
				</div>
			</div>
			<div class="col-md-7">
				<div class="tab-content" id="v-pills-tabContent"><?php $tab_content_count = 1;
					while(has_sub_field('pros_of_business')): ?>
						<div class="tab-pane fade <?php echo ($tab_content_count == 1 ? 'show active' : ''); ?>" id="v-pills-home<?php echo $tab_content_count; ?>" role="tabpanel" aria-labelledby="v-pills-home-tab<?php echo $tab_content_count; ?>">
							<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>">
						</div><?php $tab_content_count++;
					endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="why-wrap bg-light-blue">
	<div class="container">
		<div class="title">
			<h2>OUR PARTNER PROGRAM MAIN FEATURES</h2>
		</div>
		<div class="why-list">
			<ul><?php
				while(has_sub_field('partner_program_features')): ?>
					<li>
						<div class="row">
							<div class="col-md-4">
								<h5><?php the_sub_field('title'); ?></h5>
							</div>
							<div class="col-md-8">
								<?php the_sub_field('description'); ?>
							</div>
						</div>
					</li><?php 
				endwhile; ?>
			</ul>
		</div>
	</div>
</div>

<div class="faq-form">
	<div class="container">
		<div class="title">
			<h2>JOIN THE AGENCY</h2>
			<div class="title-sub-text">
				Team up with realiable and experienced coders to convert your design ideas into reality
			</div>
		</div>
		<?php echo do_shortcode('[contact-form-7 id="310" title="Agency Contact Form"]'); ?>
	</div>
</div>

<!-- CTA -->
<div class="cta-banner agency-cta-banner">
	<div class="container">
		<?php the_field('cta_text'); ?>
	</div>
</div>
<?php get_footer(); ?>