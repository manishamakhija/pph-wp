<?php 
/*
** Template Name: Contact us
*/
get_header(); ?>
<div class="contact-banner">
	<div class="container">
		<h1>CONTACT US</h1>
		<div class="contact-text"><?php 
			while(have_posts()):the_post();
				the_content();
			endwhile; ?>
		</div>
		<div class="contact-info-list">
			<div class="row justify-content-center">
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="contact-info-box">
						<div class="icon">
							<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/email.png" alt="">
						</div>
						<h3>EMAIL</h3>
						<ul><?php 
						if ($email = get_field('email', 'option')): ?>
							<li><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li><?php 
						endif;
						if (!empty (get_field('email_address') ) ): 
							while(has_sub_field('email_address')): ?>
								<li><a href="mailto:<?php echo get_sub_field('email_id'); ?>"><?php the_sub_field('email_id'); ?></a></li><?php 
							endwhile; 
						endif; ?>
						</ul>
					</div>
				</div>

				<?php if (!empty (get_field('phone_number') ) ): ?>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="contact-info-box">
						<div class="icon">
							<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/telephone.png" alt="">
						</div>
						<h3>PHONE NUMBER</h3>
						<ul> <?php 
							while(has_sub_field('phone_number')): 
								$phone = get_sub_field('phone'); ?>
								<li><a href="tel:<?php echo str_replace('-','',$phone); ?>"><?php echo $phone; ?></a></li><?php 
							endwhile; ?>
						</ul>
					</div>
				</div><?php 
				endif; 
				
				if ($skype = get_field('skype', 'option')): ?>
					<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
						<div class="contact-info-box">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/skype.png" alt="">
							</div>
							<h3>SKYPE</h3>
							<ul>
								<li><a href="skype:<?php echo $skype; ?>?chat"><?php echo $skype; ?></a></li>
							</ul>
						</div>
					</div><?php 
				endif; ?>

				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
					<div class="contact-info-box">
						<div class="icon">
							<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/location.png" alt="">
						</div>
						<h3>ADDRESS</h3>
						<ul>
							<li><a href="<?php the_field('google_map_link'); ?>" target="_blank"><?php the_field('address'); ?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="faq-form bg-light-blue">
	<div class="container">
		<div class="title">
			<h2>Get In Touch</h2>
			<div class="title-sub-text">
				<?php the_field('contact_form_excerpt'); ?>
			</div>
		</div>
		<?php echo do_shortcode('[contact-form-7 id="413" title="Contact us"]'); ?>
	</div>
</div>

<?php get_footer(); ?>