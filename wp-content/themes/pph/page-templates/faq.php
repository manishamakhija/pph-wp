<?php 
/*
** Template Name: FAQ
*/
get_header(); ?>
<div class="work-banner dot-pattern">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<div class="banner-sub-title"><?php the_field('sub_title'); ?></div>
	</div>
</div>

<div class="faq-wrap bg-light-blue">
	<div class="container">
		<div class="row faq-wrap-content">
			<div class="col-md-12 col-lg-3 col-sm-12">
				<div class="faq-nav">
					<ul><?php $k = 1;
						while(has_sub_field('faqs')): ?>
							<li><a href="javascript:;" class="<?php echo ($k == 1 ? 'active' : ''); ?>" data-id="<?php echo sanitize_title(get_sub_field('title')); ?>"><?php the_sub_field('title'); ?></a></li><?php 
							$k++;  
						endwhile; ?>
					</ul>
				</div>
			</div>
			<div class="col-md-12 col-lg-9 col-sm-12">
				<div class="faq-list-wrap"><?php $i = 1;
					while(has_sub_field('faqs')): ?>
						<div class="faq-box" id="<?php echo sanitize_title(get_sub_field('title')); ?>">
							<div class="title text-left">
								<h2><?php the_sub_field('title'); ?></h2>
							</div>
							<?php $section_name = sanitize_title(get_sub_field('title'));
							$suffix = substr($section_name,0,3); ?>
							<div class="accordion" id="section-<?php echo $section_name; ?>"><?php 
								while(has_sub_field('faq')): ?>
									<div class="card">
										<div class="card-header">
											<button class="<?php echo ( $i == 1 ? '' : 'collapsed' ); ?>" type="button" data-toggle="collapse" data-target="#<?php echo $suffix.$i; ?>">
												<?php the_sub_field('ques'); ?>
											</button>
										</div>

										<div id="<?php echo $suffix.$i; ?>" class="collapse <?php echo ( $i == 1 ? 'show' : '' ); ?>" data-parent="#section-<?php echo $section_name; ?>">
											<div class="card-body">
												<?php the_sub_field('answer'); ?>
											</div>
										</div>
									</div><?php $i++;
								endwhile; ?>
							</div>
						</div><?php $i++;
					endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="faq-form">
	<div class="container">
		<div class="title">
			<h2>QUESTION NOT ON THE LIST?</h2>
			<div class="title-sub-text">
				Drop your question below and we will back with an answer
			</div>
		</div>
		<?php echo do_shortcode('[contact-form-7 id="476" title="FAQ Contact form"]'); ?>
	</div>
</div>

<?php echo get_template_part('template-parts/cta','banner'); ?>
<?php get_footer(); ?>