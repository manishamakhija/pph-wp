<?php 
/*
** Template Name: Terms & Conditions
*/
get_header(); ?>

<div class="work-banner dot-pattern">
	<div class="container">
		<h1><?php the_title(); ?></h1>
	</div>
</div>

<div class="tc-wrap bg-light-blue">
	<div class="container">
		<?php while(have_posts()):the_post();
			the_content(); 
		endwhile; ?>
	</div>
</div>

<?php echo get_template_part('template-parts/cta','banner'); ?>
<?php get_footer(); ?>