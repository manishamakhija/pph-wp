<?php 
/*
** Template Name: Thank you
*/
get_header(); ?>

	<div class="thankyou-pages-wrap dot-pattern blue w-35">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-5 col-sm-12">
					<?php the_post_thumbnail('',array('class'=>'d-md-block d-block m-auto')); ?>
				</div>
				<div class="col-md-7 col-sm-12">
					<div class="thankyou-page-text">
						<?php while(have_posts()):the_post();
								the_content();
								endwhile; 
						?>
						<?php 
						if ( get_field('button_label') ): ?>
							<div class="cta-btn">
								<a href="<?php the_field('button_link'); ?>" class="cta-link"><span><?php the_field('button_label'); ?></span></a>
							</div><?php 
						endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>