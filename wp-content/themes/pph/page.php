<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

$classes = get_body_class();
if ( is_dashboard_page() ){
	get_template_part('dashboard/header');

}else{
	get_header(); 
}
/*
if ( !is_wc_endpoint_url( 'lost-password' ) ): ?>
	<div class="container">
		<div class="about-value-wrap">
			<div class="container">
				<div class="title">
					<h2><?php the_title(); ?> </h2><br>
					<div class="title-sub-text"><br>
						<?php
							
							while ( have_posts() ) :
								the_post();
								the_content();

							endwhile; // End of the loop.
						?>
					</div>		
				</div>
				
			</div>
		</div>
	</div><?php
else:
	while ( have_posts() ) :
		the_post();
		the_content();
	endwhile;
endif; */

if ( is_wc_endpoint_url( 'lost-password' ) || is_dashboard_page() ):
	while ( have_posts() ) :
		the_post();
		the_content();
	endwhile;
else: ?>
	<div class="container">
		<div class="about-value-wrap">
			<div class="container">
				<div class="title">
					<h2><?php the_title(); ?> </h2><br>
					<div class="title-sub-text"><br>
						<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();
								the_content();

							endwhile; // End of the loop.
						?>
					</div>		
				</div>
				
			</div>
		</div>
	</div> <?php
endif;

// if ( in_array('pph-dashboard',$classes) ) {
if ( is_dashboard_page() ){
	get_template_part('dashboard/footer');
}else{
	get_footer();
}
?>