<?php 
/*
** Template Name: Service Maintainance Page
** Template Post Type: product
*/
get_header(); ?>

<div class="services-detail-block maintenance-banner">
	<div class="container">
		<h1 class="service-title"><?php the_title(); ?> </h1>
		<?php while(have_posts()):the_post();
				the_content(); 
			endwhile; ?>
	</div>
</div>

<div class="reasons-list-wrap bg-light-blue">
	<div class="container">
		<div class="title">
			<h2><?php the_title(); ?> SERVICE FEATURES INCLUDES</h2>
		</div>
		<ul class="reasons-list"><?php 
			while( has_sub_field('service_features') ):  ?>
				<li>
					<div class="reasons-list-box"><?php 
						$feature_icon = get_sub_field('icon'); ?>
						<div class="icon"><img src="<?php echo $feature_icon['url']; ?>" class="svg" alt="<?php echo $feature_icon['alt']; ?>"></div>
						<?php the_sub_field('description'); ?>
					</div>
				</li><?php 
			endwhile; ?>		
		</ul>
	</div>
</div>

<div class="why-pph-section bg-light-blue">
	<div class="container">
		<div class="title">
			<h2>WHY TO CHOOSE PPH?</h2>
		</div>
		<div class="why-pph-points">
			<ul class="row justify-content-center"><?php 
				while(has_sub_field('why_pph')): ?>
					<li class="col-md-3 col-sm-6 col-6">
						<div class="icon"><?php 
							$pros_icon = get_sub_field('icon'); ?>
							<img src="<?php echo $pros_icon['url']; ?>" class="svg" alt="<?php echo $pros_icon['alt']; ?>">
						</div>
						<h4><?php the_sub_field('title'); ?></h4>
					</li> <?php 
				endwhile; ?>
			</ul>
		</div>
	</div>
</div>

<div class="maintenance-wrap bg-light-blue">
	<div class="container">
		<div class="title">
			<h2><?php the_title(); ?> PACKAGES </h2>
		</div>
		<div class="maintenance-include">
			<h3>All Packages Includes</h3>
			<ul class="bullet-list"><?php 
				while(has_sub_field('packages_include_features')): ?>
					<li><?php the_sub_field('name'); ?></li><?php 
				endwhile; ?>
			</ul>
		</div>
		<div class="maintenance-package" id="maintenance-package">
			<h3>Select the amount of support hours you would like to purchase</h3>
			<ul class="row"><?php 
				while(has_sub_field('package_pricing')): ?>
					<li class="col-md-4 col-sm-12">
						<div class="box"><?php 
							if (get_sub_field('best_seller')): ?>
								<div class="best-seller"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/maintenance-package-best-seller.png" alt="best-seller-badge"></div><?php 
							endif; 

							$package_icon = get_sub_field('icon'); ?>
							<div class="icon"><img src="<?php echo $package_icon['url']; ?>" alt="<?php echo $package_icon['alt']; ?>"></div>
							<?php the_sub_field('name'); ?>
							<div class="price">$<?php the_sub_field('price'); ?></div>
							<div class="cta-btn">
								<?php echo paypal_button( get_the_title(), get_sub_field('price') ); ?>
								<!-- <a href="#" class="cta-link"><span>ORDER NOW</span></a> -->
							</div>
						</div>
					</li><?php 
				endwhile; ?>
			</ul>
		</div>
	</div>
</div>

<div class="service-quote-wrap">
	<div class="container">
		<div class="title">
			<h2>GET A QUOTE</h2>
			<div class="title-sub-text">
				Need a custom package to fit your needs?
			</div>
		</div>
		<form method="POST" enctype="multipart/form-data" class="maintainance-quote multiple_uploads_form" name="maintainance-quote" id="maintainance-quote">
			<div class="form-wrap small-wrap">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<input type="text" placeholder="Your Name*" name="name" class="form-control required">
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<input type="text" placeholder="Email Address*" name="email" class="form-control required email">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="form-group">
							<textarea placeholder="Message" class="form-control" name="message"></textarea>
						</div>
					</div>
				</div>
				<div class="form-note">We keep all information confidential and automatically agree to NDA.</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="form-group">
							<input type="file" class="my-pond" name="filepond[]" multiple />
						</div>
					</div>
				</div>
				<div class="form-note">Multiple files supported.</div>
				<div class="form-group">
					<div class="cta-btn text-center">
						<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/ajax-loader.gif" class="ajax-loader" alt="loader" />
						<button class="cta-link btn-block" type="submit">SEND</button>
						<input type="hidden" value="<?php echo ucwords( strtolower ( get_the_title() ) ); ?>" name="form_type">
						<input type="hidden" value="<?php echo ucwords( strtolower ( get_the_title() ) ); ?>" name="service">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php get_footer(); ?>