<?php
require_once('../../../../wp-config.php');
// Comment if you don't want to allow posts from other domains
header('Access-Control-Allow-Origin: *');

// Allow the following methods to access this file
header('Access-Control-Allow-Methods: POST');

// Load the FilePond class
require_once('FilePond.class.php');

// Load our configuration for this server
require_once('config.php');

// Catch server exceptions and auto jump to 500 response code if caught
FilePond\catch_server_exceptions();
FilePond\route_form_post(ENTRY_FIELD, [
    'FILE_OBJECTS' => 'handle_file_post',
    'BASE64_ENCODED_FILE_OBJECTS' => 'handle_base64_encoded_file_post',
    'TRANSFER_IDS' => 'handle_transfer_ids_post'
]);

function handle_file_post($files) {

    // This is a very basic implementation of a classic PHP upload function, please properly
    // validate all submitted files before saving to disk or database, more information here
    // http://php.net/manual/en/features.file-upload.php
    
    foreach($files as $file) {
        FilePond\move_file($file, UPLOAD_DIR);
    }
}

function handle_base64_encoded_file_post($files) {

    foreach ($files as $file) {

        // Suppress error messages, we'll assume these file objects are valid
        /* Expected format:
        {
            "id": "iuhv2cpsu",
            "name": "picture.jpg",
            "type": "image/jpeg",
            "size": 20636,
            "metadata" : {...}
            "data": "/9j/4AAQSkZJRgABAQEASABIAA..."
        }
        */
        $file = @json_decode($file);

        // Skip files that failed to decode
        if (!is_object($file)) continue;

        // write file to disk
        FilePond\write_file(
            UPLOAD_DIR, 
            base64_decode($file->data), 
            FilePond\sanitize_filename($file->name)
        );
    }

}

// this function calls on submit
function handle_transfer_ids_post($ids) {
    global $filenames;
    foreach ($ids as $id) {
        
        // create transfer wrapper around upload
        $transfer = FilePond\get_transfer(TRANSFER_DIR, $id);
        // transfer not found
        if (!$transfer) continue;
        
        // move files
        $files = $transfer->getFiles(defined('TRANSFER_PROCESSOR') ? TRANSFER_PROCESSOR : null);
        foreach($files as $file) {
            $filenames[$id]['new_name'] = $id . '.' . pathinfo($file['name'], PATHINFO_EXTENSION); 
            $filenames[$id]['original'] = $file['name'];
            FilePond\move_file($file, UPLOAD_DIR);
        }

        // remove transfer directory
        FilePond\remove_transfer_directory(TRANSFER_DIR, $id);
    }

}

// get project deadline date
function get_estimate_delivery_date($no_of_business_days, $order_created_date){
    $non_business_days = array('6','7');
    $i = 0;
    // $order_created_date = new DateTime('24 May, 2019');
    $date = new DateTime($order_created_date);
    while ($i < $no_of_business_days) {
        $date->modify("+1 day");
        if (!in_array( (int)$date->format('N'), $non_business_days) ) {
            $i++;
        }
    }
    return $date->format('j F, Y');
}

// quick quote and services detail page quote form
if ( isset( $_POST['form_type'] ) ){
    $post_title = $_POST['service'].' '.sanitize_text_field( $_POST['name'] ) . '-' . date('F j, Y H:i:s');
    $name = sanitize_text_field( $_POST['name'] );
    $email = sanitize_email( $_POST['email'] );
    $message = $_POST['message'];
    foreach($filenames as $key => $value){
        $filepath = get_template_directory_uri().'/filepond/uploads/'.$value['new_name'];
        $link .= '<a href = "'. $filepath .'" download target="_blank">'.$value['original'].'</a>'."<br>";
    }
    $content = 
    'Service: '.$_POST['service']."\r\n".
    'Name: '.$name."\r\n".
    'Email: <a href="mailto:"'.$email.'">'.$email.'<a>'."\r\n".
    'Message: '. $message ."\r\n".
    'Files Attached: '. $link ."\r\n";
    $arg = array('post_title' => $post_title,'post_type' => 'feedback','post_status'=>'publish','post_content' => $content);
    $form_id = wp_insert_post($arg);
    update_post_meta($form_id,'form_type',$_POST['form_type']);
    include ( '../email-templates/quote-mail.php');
    $msg = array('result'=>1, 'message'=>'success', 'enquire_person' => $name, 'service'=> $_POST['service'] );
    echo json_encode( $msg, true );
}


// order page form
if ( isset( $_POST['total_cost'] ) ){
    $product_id = $_POST['service'];
    $name = $_POST['name'];
    $skype = $_POST['skype_name'];
    $email = $_POST['email'];
    $package_name = $_POST['package_name'];
    $delivery = $_POST['delivery'];
    $phone = $_POST['phone'];
    $project_note = $_POST['project_note'];
    $coupon_code = $_POST['coupon'];
    $payment_method = $_POST['payment_type'];
    $total_cost = $_POST['total_cost'];
    $project_name = $_POST['projectName'];
    
    // to be used in mail
    $home_page = $_POST['home_page'];
    $single_inner_page = $_POST['single_inner_page'];
    $inner_page = $_POST['inner_page'];
    $coupon_discount_amount = $_POST['coupon_discount_amount'];
    $turnaround = $_POST['duration'];
    $show_in_portfolio_discount = $_POST['use_in_portfolio'];
    

    if ($phone == '' || $project_name == '' || $_POST['agree_policy'] == ''){
        $msg = array( 'result'=> 3,'message'=> 'Please enter the required fields' );
        echo wp_json_encode($msg);
        exit;
    }

    $user_id = email_exists( $email );
    $default_password = wp_generate_password();
    if ( !$user_id ){
        $userdata = array(
            'user_login' =>  $email,
            'user_pass'  =>  $default_password,
            'user_email' => $email,
            'role' => 'customer'
        );
        $user_id = wp_insert_user( $userdata ) ;
        if( is_wp_error( $user_id ) ) {
            $user_error_msg =  $user_id->get_error_message();
            $msg = array( 'result'=> 3,'message'=> $user_error_msg );
            echo wp_json_encode($msg);
            exit;
        }else{
            // account details notification to user
            include ( '../email-templates/account-credentials-mail.php');
        }
    }else{ // converting subscriber to customer role
        $user_obj = get_user_by('id',$user_id);
        $user_role = $user_obj->roles;
        if ( in_array('subscriber', $user_role ) ){

            $userdata = array(
                'ID' => $user_id,
                'user_pass'  =>  $default_password,
                'role' => 'customer'
            );

            $updated_userid = wp_update_user( $userdata ) ;

            if( is_wp_error( $updated_userid ) ) {
                $user_error_msg =  $updated_userid->get_error_message();
                $msg = array( 'result'=> 4,'message'=> $user_error_msg );
                echo wp_json_encode($msg);
                exit;
            }else{
                // account details notification to subscriber user
                include ( '../email-templates/account-credentials-mail.php');
            }
           
        }
    }

      //create the order
      $args = array('customer_id' => $user_id, 'customer_note' => $project_note);
      $order = wc_create_order( $args );
      $order_id = $order->get_id();
      $order_created_date = $order->get_date_created()->format ('j F, Y');
      update_post_meta($order_id,'project_name',$project_name );
      update_post_meta($order_id,'package',$package_name );
      update_post_meta($order_id,'delivery',$delivery );
      update_post_meta($order_id, 'skype_name', $skype );
      update_post_meta($order_id, 'files_included', $filenames );
      update_post_meta($order_id, 'total_order_amount', $total_cost ); // for pending/failed orders
      update_post_meta($order_id, 'payment_method', $payment_method );
       $address = array(
          'first_name' => $name,
          'email'      => $email,
          'phone'      => $phone,
      );
      $order->set_address( $address, 'billing' );
      $order->add_product( wc_get_product($product_id), 1); // This is an existing SIMPLE product
      if ( $coupon_discount_amount > 0 ){
        $order->add_coupon($coupon_code, $coupon_discount_amount);
      }
      $order->set_total($total_cost);
      $order->set_payment_method($payment_method);
      // update_post_meta($order_id, 'service', $product_id );
      foreach ($order->get_items() as $item_key => $item ):

            // Item ID is directly accessible from the $item_key in the foreach loop or
            $item_id = $item->get_id();
            $inner_page_count = $_POST['qty'] - 1 ;
            wc_add_order_item_meta($item_id,'home_page',$home_page);
            wc_add_order_item_meta($item_id,'single_inner_page',$single_inner_page);
            wc_add_order_item_meta($item_id,'inner_page',$inner_page);
            wc_add_order_item_meta($item_id,'discount_amount',$coupon_discount_amount);
            if ($inner_page_count > 0){
                wc_add_order_item_meta($item_id,'inner_page_count', $inner_page_count );
            }
            wc_add_order_item_meta($item_id,'turnaround',$turnaround); 
            if ( $show_in_portfolio_discount != '' ){

                wc_add_order_item_meta($item_id,'show_in_portfolio_discount', $show_in_portfolio_discount); 
            }
        endforeach;

        $project_deadline = get_estimate_delivery_date($turnaround, $order_created_date);
        update_post_meta($order_id, 'project_deadline', $project_deadline );
      // update_post_meta($order_id, 'portfolio_include_discount', $_POST['use_in_portfolio'] );
      // update_post_meta($order_id, 'inner_pages', $_POST['qty'] );
      // update_post_meta($order_id, 'phone', $phone );
      // var_dump($order);
      // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
      // $order->set_payment_method( $payment_gateways['bacs'] );
      // $order->calculate_totals();
       
      // pph_fetch_prices(97);
      // var_dump($_POST, $order_id); exit; 
    if ($payment_method == 'stripe'){
        require_once '../stripe/vendor/autoload.php';
       
        // test account
        \Stripe\Stripe::setApiKey('sk_test_FeyHbI6z5Fh8PthJVLBPzqKS');
      
        // \Stripe\Stripe::setApiKey('sk_live_4vMOMBN0JQeJrUWGJIoVk443');

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
         //add customer to stripe
        $customer = \Stripe\Customer::create(array(
            'email' => $email,
            'source'  => $token,
            'name' => 'Jenny Rosen',
            'address' => [
                'line1' => '510 Townsend St',
                'postal_code' => '98140',
                'city' => 'San Francisco',
                'state' => 'CA',
                'country' => 'US'
            ],
        ));
        $description = 'PPH order for '.get_the_title($product_id).' service';
        // var_dump($description); exit;
        $card_holder_name = $_POST['name_on_card'] != "" ? $_POST['name_on_card'] : "";
      // var_dump($card_holder_name);exit;
        $charge = \Stripe\Charge::create([
            'amount' => $total_cost * 100,
            'customer' => $customer->id,
            'currency' => 'usd',
            'description' => $description,
            'metadata' => [
                'card_holder_name' => $card_holder_name,
                'recipient_name' => $name,
                'email' => $email,
                'phone' => $phone,
                'order_id' => $order_id
            ],
        ]);
        $chargeJson = $charge->jsonSerialize();

        // check whether the charge is successful
        if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
            //order details 
            $amount = $chargeJson['amount'];
            $balance_transaction = $chargeJson['balance_transaction'];
            $currency = $chargeJson['currency'];
            $status = $chargeJson['status'];
            $date = date("Y-m-d H:i:s");
            $note = 'Charge ID: '.$chargeJson['id'];
            $transaction_id = $chargeJson['id'];
            $note_comment_id = $order->add_order_note( $note );
            global $wpdb;
            $wpdb->update( 'wp_comments', array('comment_karma' => 1), array('comment_ID' => $note_comment_id ) );
            $order->update_status("processing"); 
            include ( '../email-templates/order-mail.php');
            $msg = array( "result" => 1, "message" => "Payment done Successfully", "order_id" => $order_id, "name" => $name, "service" => get_the_title($product_id) );
            // $url = get_site_url().'/thank-you';
            // wp_redirect($url);
            // wp_die();
        }else{
            $msg = array( "result" => 0, "message" => "Error in payment processing!! Please try after sometime." );
        }
    }else if ($payment_method == 'paypal'){ 
        include ( '../email-templates/paypal-intimation-mail.php');
        $msg = array( "result" => 2, "message" => "Paypal Redirection", "order_id" => $order_id, "name" => $name, "service" => get_the_title($product_id)  );
    }
    echo json_encode( $msg, true );
}