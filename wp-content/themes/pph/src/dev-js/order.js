( function() {
	var _this;
	var stripe;
	var cardNumber, cardExpiry, cardCvc;

	jQuery.fn.extend({
		orderProcess: function() {
			_this = this;

			jQuery('.count').prop('readonly', true);
			
			jQuery(document).on("click",".payment-selection ul li", function(){
				jQuery('input[name="payment_type"]').attr('checked',false);
				jQuery(this).find('input[name="payment_type"]').attr('checked',true);
			});

			// customize error messages
			VeeValidate.Validator.localize('en', {
			  custom: {
			    email: {
			      required: 'Please enter your email.',
			      email: 'You have entered an invalid e-mail address.'
			    },
			    name: {
			      required: 'Please enter your full name.'
			    },
			    phone: {
			      required: 'Please enter your phone.',
			      numeric: 'The entry can only contain numbers.'
			    },
			    projectName: {
			      required: 'Please enter your project name.'	
			    }
			  }
			})
				// price calculation via vue js
				Vue.use(VeeValidate);
				
				var app = new Vue({
				  	el: '#order-form-wrap',
				      	data: {
				        	service: 97,
				        	prices: obj.dynamic_data.content,
				        	pages: 1,
				        	selectedPackage: 'Basic',
				        	selectedDelivery: 'Standard',
				        	portfolioDiscount: false,
				        	pDiscountValue: 0.00,
				        	couponCode: "",
				        	couponApplied: false,
				        	couponDiscount: 0,
				        	discountStatus: null,
				        	name: null,
				        	email: null,
				        	phone: null,
				        	projectName: null,
				        	paymentMethod: 'stripe',
				        	agreePolicy: false,
				        	basicFeatures: obj.dynamic_data.basic_features,
				        	premiumFeatures: obj.dynamic_data.premium_features
				      	},
				      	methods: {
				      		fetchServiceData: function(serviceId) {
				      			this.couponDiscountReset();
				      			this.service = serviceId;
				      			// console.log(serviceId);
				      			axios.get(obj.ajax_url,{
									  params:{
									  	action: 'pph_fetch_service_prices',
									    service: serviceId
									  }
									})
							      .then(function (response) {
							         app.prices = response.data.content;
							         app.basicFeatures = response.data.basic_features;
							         app.premiumFeatures = response.data.premium_features;
							      })
							      .catch(function (error) {
							         console.log(error);
							      });
				      		},
				      		pageCountChange: function(count) {
				      			this.couponDiscountReset();
				      			this.pages +=  count;
				      			if (this.pages == 0){
				      				this.pages = 1;
				      			}
				      			if (this.pages > 20){
				      				this.pages = 20;
				      			}
				      		},
				      		packageChange: function() {
				      			this.couponDiscountReset();
				      		},
				      		deliveryTimeChange: function(){
				      			this.couponDiscountReset();
				      		},
				      		portfolioChargesAdded: function(event) {
				      			this.couponDiscountReset();
				      			// if (event.target.checked){
				      			// 	this.pDiscountValue = parseFloat(event.target.value).toFixed(2);
				      			// }else{
				      			// 	this.pDiscountValue = 0.00;
				      			// }
				      		},
				      		applyCoupon: function() {
				      			jQuery.blockUI({ message: '<h1> Just a moment...</h1>' , css: { backgroundColor: '#fff',border: '0', padding: '20px' }});
				      			axios.get(obj.ajax_url,{
								  	params:{
								  		action: 'pph_discount_coupon',
									    productId: this.service,
									    couponCode: this.couponCode,
									    totalCost: this.totalCost
								  	}
								})
							    .then(function (response) {
							        jQuery.unblockUI();
							      	if (response.data.result == 1){
								        app.couponDiscount = parseFloat(response.data.discount).toFixed(2);
								        app.couponApplied = true;
								    }
							        app.discountStatus = response.data.message;
							        setTimeout(function(){
							        	app.discountStatus = null;
							        },2000);
							    })
						      	.catch(function (error) {
						         	console.log(error);
						      	});
				      		},
				      		couponDiscountReset: function() {
				      			if ( this.couponCode != "" || this.couponDiscount != 0 ){
				      				this.couponCode = "";
				      				this.couponDiscount = 0;
				      				this.couponApplied = false;
				      			}
				      		},
				      		removeCoupon: function() {
				      			jQuery.blockUI({ message: '<h1> Just a moment...</h1>' });
				      			axios.get(obj.ajax_url,{
				      				params:{
				      					action: 'pph_remove_discount_coupon'
				      				}
				      			})
				      			.then( function (response) {
				      				jQuery.unblockUI();
				      				if (response.data.result == 1){
				      					app.couponDiscountReset();
				      					app.discountStatus = response.data.message;
				      				}
				      			})
				      			.catch( function (error) {
				      				console.log(error);
				      			});
				      		},
				      		onSubmit: function() {
						      	this.$validator.validateAll().then(function(result) {
						      		// console.log(result);
									if(result){
										// console.log('form is valid', this.model);
											jQuery('#place-order').attr('disabled',true);
										if (app.paymentMethod == 'paypal'){
											jQuery( '.order_form' ).paypalPayment();
										}
										// return;
									}
									}).catch(function() {
									});
						    },

				      	},
				      	computed: {
				      		homePagePrice: function() {
				      			return parseFloat( this.prices[this.selectedDelivery][this.selectedPackage]['Home']['rate'] ).toFixed(2);
				      		},
						    innerPages: function () {
						      return this.pages - 1 ;
						    },
						    singleInnerPagePrice: function() {
						    	return parseFloat( this.prices[this.selectedDelivery][this.selectedPackage]['Inner']['rate'] );
						    },
						    innerPagePrice: function() {
						    	return parseFloat( parseFloat( this.innerPages ) * parseFloat(this.singleInnerPagePrice) ).toFixed(2);
						    },
						    deliveryDays: function() {
						    	var homePageTime = parseFloat( this.prices[this.selectedDelivery][this.selectedPackage]['Home']['days'] );
						    	var innerPageTime = this.innerPages * parseFloat(this.prices[this.selectedDelivery][this.selectedPackage]['Inner']['days']);
						    		return Math.round(homePageTime + innerPageTime);
						    },
						    expressPrice: function() {
						    	if (this.selectedDelivery == 'Express'){
						    		var stdDeliveryPrice = parseFloat( this.prices['Standard'][this.selectedPackage]['Home']['rate'] ) + parseFloat( this.innerPages * this.prices['Standard'][this.selectedPackage]['Inner']['rate'] ) ;
						    		var expressDeliveryPrice = parseFloat(this.homePagePrice) + parseFloat( this.innerPagePrice );
						    		return parseFloat(expressDeliveryPrice - stdDeliveryPrice).toFixed(2);
						    	}else{
						    		return '0.00';
						    	}
						    },
						    calculatePortfolioCharges: function() {
						    	if (this.portfolioDiscount){
						    		var addedFees = ((20 * (parseFloat(this.homePagePrice) + parseFloat(this.innerPagePrice)))/100);
						    	}else{
						    		var addedFees = 0.0;
						    	}
						    	return addedFees;
						    },
						    totalCost: function() {
						    	return parseFloat(this.homePagePrice) + parseFloat(this.innerPagePrice) + parseFloat(this.calculatePortfolioCharges) - this.couponDiscount;
						    }
						}
				})

		},
		stripePayment: function(){
			// test account
			stripe = Stripe('pk_test_Djuy9wagvInT35OiDZrIOotO');

			// var stripe = Stripe('pk_live_f8MmULN3dSyVk80mc9wQENTW');
			

		  	var elements = stripe.elements({
			    fonts: [
			      {
			        cssSrc: 'https://fonts.googleapis.com/css?family=Montserrat:400,500,600&display=swap',
			      },
			    ]
		  	});

		  	// Floating labels
		  	var displayError = document.getElementById('card-errors');
		  	var inputs = document.querySelectorAll('.card-detail-add .input');
		  	Array.prototype.forEach.call(inputs, function(input) {
			    input.addEventListener('focus', function() {
			      input.classList.add('focused');
			    });
			    input.addEventListener('blur', function() {
			      input.classList.remove('focused');
			    });
			    input.addEventListener('keyup', function() {
			      if (input.value.length === 0) {
			        input.classList.add('empty');
			      } else {
			        input.classList.remove('empty');
			      }
			    });
		  	});
		  	var elementStyles = {
		    	base: {
			      color: '#32325D',
			      fontWeight: 500,
			      fontFamily: 'montserrat, sans-serif',
			      fontSize: '16px',
			      fontSmoothing: 'antialiased',

			      '::placeholder': {
			        color: '#909090',
			      },
			      ':-webkit-autofill': {
			        color: '#909090',
			      },
			    },
			    invalid: {
			      color: '#E25950',

			      '::placeholder': {
			        color: '#E25950',
			      },
			    },
		  	};

		  	var elementClasses = {
		    	focus: 'focused',
			    empty: 'empty',
			    invalid: 'invalid',
		  	};

		  	cardNumber = elements.create('cardNumber', {
			  	placeholder: 'Card Number',
			    style: elementStyles,
			    classes: elementClasses,
		  	});
		  	cardNumber.mount('#card-no');

		  	cardExpiry = elements.create('cardExpiry', {
		    	style: elementStyles,
			    classes: elementClasses,
		  	});
		  	cardExpiry.mount('#expiry-date');

		  	cardCvc = elements.create('cardCvc', {
		    	style: elementStyles,
	    		classes: elementClasses,
			    placeholder: 'CVV'
		  	});
		  	cardCvc.mount('#cvv-no');

			// Listen for errors from each Element, and show error messages in the UI.
		  	Array.prototype.forEach.call(elements, function (element) {
		    	element.on('change', function(event) {
		      		if (event.error) {
		        		displayError.textContent = event.error.message;
		      		} else {
		        		displayError.textContent = '';
		      		}
		    	});
		  	});


			var form = document.getElementById('order-form');
			form.addEventListener('submit', function(event) {
			  	event.preventDefault();
			  	if (stripe != undefined){
				  	stripe.createToken(cardNumber).then(function(result) {
				    	if (result.error) {
					      	// Inform the customer that there was an error.
					      	var errorElement = document.getElementById('card-errors');
					      	errorElement.textContent = result.error.message;
					      	document.getElementById("place-order").disabled = false;
					    } else {
					      	// Send the token to your server.
					      	stripeTokenHandler(result.token);
					    }
				  	});
				 }
			});

			function stripeTokenHandler(token) {
				  // Insert the token ID into the form so it gets submitted to the server
				  var form = document.getElementById('order-form');
				  var hiddenInput = document.createElement('input');
				  hiddenInput.setAttribute('type', 'hidden');
				  hiddenInput.setAttribute('name', 'stripeToken');
				  hiddenInput.setAttribute('value', token.id);
				  form.appendChild(hiddenInput);
				  document.getElementById("place-order").disabled = true;
				  
				  jQuery('.order-form').orderFormSubmit();
			  		
			}
		},
		removeStripePayment: function(){
			
			cardNumber.unmount('#card-no');
		  	cardExpiry.unmount('#expiry-date');
		 	cardCvc.unmount('#cvv-no');
		  	stripe = undefined;
		  	delete(stripe);
		},
		paypalPayment: function(){
			// remove stripe elements first
			jQuery('.order-form').orderFormSubmit();
		},
		orderFormSubmit: function(){
			jQuery.ajax({
				url: obj.template_url + '/filepond/submit.php',
				type: "POST",
				dataType: "json",
				data: jQuery('.order_form').serialize(),
				beforeSend: function(){
					jQuery('.ajax-loader').show();
				},
				success: function(data)
				{
					if (data.result == 1){
						location.href = obj.site_url + '/thank-you-order?enquire_name=' + data.name + '&service_name=' + data.service + '&order_id=' + data.order_id;

					}else if (data.result == 2){
						var paypalForm = jQuery('#paypal_form');
						var thankYouUrl = obj.site_url + '/thank-you-order?enquire_name=' + data.name + '&service_name=' + data.service + '&order_id=' + data.order_id;
						paypalForm.find('input[name="item_name"]').val(data.service);
						paypalForm.find('input[name="custom"]').val(data.order_id);
						paypalForm.find('input[name="return"]').val(thankYouUrl);
						jQuery('#paypal_form').submit();
					}
					else if ( data.result == 3 ){
						jQuery('.order-form-alert-msg').addClass('alert alert-danger').html(data.message);
					}
					else{
						location.href = obj.site_url;
					}
				},
				error: function(msg){
					console.log(msg);
				},
				complete: function(){
					jQuery('.ajax-loader').hide();
				}
			});
		}

	});

} )( jQuery );