// login form ajax calling.
	jQuery('.login_submit').click(function(e){
		e.preventDefault();
		var loginForm = jQuery(this).parents('form.login_form');
		var form_data = loginForm.serialize();
		// jQuery('.success').html('');
		// jQuery('.error').html('');
		// var user_name= jQuery(this).parents('.management').find('#username').val();
		// var user_pass= jQuery(this).parents('.management').find('#password').val();
		// var wpnonce= jQuery(this).parents('#login_form').find('#_wpnonce').val();
		// var _this=jQuery(this);
		// var remember= null;
		// if(jQuery('#rememberme').is(':checked')){
		// 	remember=  jQuery(this).parents('.management').find('#rememberme').val();
		// }
		// jQuery(this).parents('.management').find('img').show();
		var pdata = {
			 	action: "login_user_form",
			 	formData: form_data
			 	// username : user_name,
     //            password : user_pass,
     //            remember_me : remember,
     //            wpnonce : wpnonce
	        }
	    	jQuery.post(js_object.ajax_url,pdata,
	            function ( response ) {
	            	var ajax_result = jQuery.parseJSON(response);
	            	if(ajax_result.result == '1'){
	            		// loginForm.prev('.msg-status').addClass('alert alert-success').html(ajax_result.message);
	            		loginForm.prev('.msg-status').removeClass('alert-danger');
	            		window.location.href = ajax_result.url;
						// jQuery(_this).parents('.management').find('img').hide();
						// jQuery('.login .success').html(ajax_result.message);
						// window.location.href = success_url.login;
					}
					else{
						loginForm.prev('.msg-status').addClass('alert alert-danger').html(ajax_result.message);
						// loginForm.prev('.msg-status').removeClass('alert-success');
						// jQuery('.login .error').html(ajax_result.message);
					}
	            }  
	        ); 
	});

		// service select quick quote
	jQuery(document).on('click','.quote-service-select.single-select ul li', function(){
		var _this = jQuery(this);
		_this.parents('.quote-service-select').find('input[type="radio"]').attr('checked',false);
		_this.find('input[type="radio"]').attr('checked',true);
		jQuery('.selected-service').find('.s-name').text(_this.find('h4').text());
		// icon
		var iconURL = _this.find('.icon').data('src');
		jQuery('.selected-service .icon').find('img').attr('src',iconURL);
	});
	// jQuery(document).on('click','.quick-quote-submit',function(){
	// 	var form_data = {
	// 		action: "quick_quote_data_processed",
	// 		data: jQuery('form[name="quick_quote"]').serialize()
	// 	}
	// 	jQuery.post(js_obj.ajax_url,form_data,
	//             function ( response ) {
	//             	var ajax_result = jQuery.parseJSON(response);
	//             	// console.log(response);
	//             	if(ajax_result.result == '1'){
	//             		jQuery('.msg-status').addClass('success').html(ajax_result.message);
	// 					// jQuery(_this).parents('.management').find('img').hide();
	// 					// jQuery('.login .success').html(ajax_result.message);
	// 					// window.location.href = success_url.login;
	// 				}
	// 				else{
	// 					jQuery('.msg-status').addClass('alert').html(ajax_result.message);
	// 					// jQuery('.login .error').html(ajax_result.message);
	// 				}
	//             }  
	//         ); 

	// })

	jQuery(document).ready(function(){
		jQuery(".multiple_uploads_form").validate({
		// errorClass:"error",
		// errorElement:"span",
		// rules: {
		// 		    phone: {
		// 		    	phone_validate: true
		// 		    }
		// 	  	},
			submitHandler:function() {
			    jQuery.ajax({
				url: js_object.template_url + '/filepond/submit.php', 
				type: "POST", 
				dataType: "json",            
				data: jQuery('.multiple_uploads_form').serialize(),
				beforeSend: function(){
					jQuery('.ajax-loader').show();
				} ,
				success: function(data)   
				{
					if (data.result == 1){
						// console.log(data);
						location.href = js_object.site_url + '/thank-you-quote' + '?enquire_name=' + data.enquire_person + '&service_name=' + data.service;
					}else{
						location.href = js_object.site_url;
					}
					// if (data.result != '0'){
					// 	window.open("<?php echo get_site_url() ?>/labour-preview?recipient_id="+ data.result ,"_blank");
					// 	jQuery('.preview-btn').val('Update');
					// 	jQuery('.update_id').val(data.result);
					// 	jQuery('#insertion-error').text(data.message);
					// }	
					// else{
					// 	jQuery('#insertion-error').text(data.message);
					// }
				},
				error: function(msg){
					console.log(msg);
				},
				complete: function(){
					jQuery('.ajax-loader').hide();
				}
				});
				
			}
		});

		// // on page load stripe payment calls
		// var paymentMethod = jQuery('input[name="payment_type"]');
		// if (paymentMethod.is(':checked') &&  paymentMethod.val() == 'stripe'){
		// 	jQuery( '.order_form' ).stripePayment();
		// }

	});

	// jQuery(document).on('click','.payment-selection ul li input', function(){
	// 	// var _this = jQuery(this);
	// 	// var selectedPay = _this.find('input[name="payment_type"]');
	// 	var selectedPay = jQuery(this);
	// 	if (selectedPay.is(':checked') &&  selectedPay.val() == 'stripe'){
	// 		jQuery( '.order_form' ).stripePayment();
	// 	}else if(selectedPay.is(':checked') &&  selectedPay.val() == 'paypal'){
	// 		jQuery( '.order_form' ).removeStripePayment();
	// 	}
	// });
// jQuery(document).on('click','.quote-service-select.single-select ul li', function(){
// 	var _this = jQuery(this);
// 	// _this.parent('ul').removeClass("select");
// 	// _this.addClass("select");
// 	_this.parents('.quote-service-select').find('input[type="radio"]').attr('checked',false);
// 	_this.find('input[type="radio"]').prop('checked',true);
// 	// var title = _this.find('h4').text();
// 	// var image = _this.find('img').prop('src'); console.log(image);
// 	jQuery('.selected-service').find('.s-name').text(_this.find('h4').text());
// 	// jQuery('.selected-service').find('.icon img').prop('src',image);
// });

// order page
// jQuery(document).on('click','.plus',function(){
// 	var pages = jQuery(this).siblings('.count').val();
// 	var newVal = jQuery('.count').val(parseInt(pages) + 1 );
// 	if (pages == 1){
// 		jQuery('.pages-breakdown .inner-page-count').show();
// 	}else{
// 		jQuery('.pages-breakdown .inner-page-count span').html(pages);
// 	}
// });

// jQuery(document).on('click','.minus',function(){
// 	var pages = jQuery(this).siblings('.count').val();
// 	var innerPagesDiv = jQuery('.pages-breakdown');
// 	var newVal = parseInt(pages) - 1 ;
// 	jQuery('.count').val(newVal);
// 	if (pages == 2){
// 		innerPagesDiv.find('.inner-page-count').hide();
// 	}else{
// 		innerPagesDiv.find('.inner-page-count span').html(parseInt(newVal) - 1);
// 	}

// 	if (jQuery('.count').val() == 0) {
// 		jQuery('.count').val(1);
// 		innerPagesDiv.find('.inner-page-count span').html(1);
// 	}
// });

// jQuery(document).on('click','button[name="coupon_applied_btn"]',function(){
// 	var postData = {
// 		action: "pph_discount_coupon",
//       	productId: jQuery('input[name="service"]').val(),
//       	couponCode: jQuery('input[name="coupon"]').val()
// 	} 

// 	jQuery.post(js_obj.ajax_url,postData, 
//      function( response ) {
//      	jQuery('.discount-status').html(response);
//      	// var ajax_result = jQuery.parseJSON(response);
//      	// if(ajax_result.result == '1'){
      		 
//       // 	}else{
//       // 	}    
//     } ); 
// });
// jQuery(document).on("click",".payment-selection ul li", function(){
// 	jQuery('input[name="payment_type"]').prop('checked',false);
// 	jQuery(this).find('input[name="payment_type"]').prop('checked',true);
// });



// if ( jQuery( 'body' ).hasClass( 'page-template-order' ) ) {
// 		jQuery( '.order_form' ).orderProcess();
// }

document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '413' == event.detail.contactFormId ) {		// contact form
    	var enquireName = event.detail.inputs[1].value;
       window.location.href = js_object.site_url + '/thank-you' + '?enquire_name=' + enquireName;
    }

    if ( '310' == event.detail.contactFormId ) { 		// agency 
    	var enquireName = event.detail.inputs[1].value;
    	var agencyName = event.detail.inputs[2].value;
       window.location.href = js_object.site_url + '/thank-you-agency' + '?enquire_name=' + enquireName + '&agency_name=' + agencyName;
    }

    if ( '476' == event.detail.contactFormId ) { 		// FAQ 
    	
    	var enquireName = event.detail.inputs[1].value;
       window.location.href = js_object.site_url + '/thank-you-faq' + '?enquire_name=' + enquireName;
    }


}, false );

// smooth scroll 
jQuery(document).ready(function(){
	var target = window.location.hash;
	if ( target != '' ){
	    var $target = jQuery(target); 
	    var header = jQuery('.navbar-wrap').height();
	    jQuery('html, body').stop().animate({
	    'scrollTop': $target.offset().top - header},
	    900, 
	    'swing',function () {
	    });
	}
});


// RPDP plugin js
jQuery(".pph-thumb-img").each(function(i, elem) {
	var img = jQuery(elem);
	jQuery(this).hide();
	jQuery(this).parent().css({
		background: "url(" + img.attr("src") + ") no-repeat center center",
	});
});