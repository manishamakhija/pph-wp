
jQuery(document).ready(function($){
	
	// multiple file uploads
	$.fn.filepond.registerPlugin(FilePondPluginFileEncode, FilePondPluginFileValidateType);
	jQuery('.my-pond').filepond({
		allowMultiple: 'true',
		abortProcessing: 'true',
		abortLoad: 'true',
		labelIdle: 'Drag & Drop your Files <span class="filepond--label-action">Browse</span>',
		// acceptedFileTypes: ['image/png','image/jpeg','application/pdf','application/vnd.sketchup.skp','application/zip','image/vnd.adobe.photoshop','application/postscript','application/illustrator','image/tiff','image/x-tiff','image/webp','image/svg+xml','application/octet-stream'],
		fileValidateTypeLabelExpectedTypes: '',
		labelFileTypeNotAllowed: 'File must be AI,PSD,PNG,JPG,TIFF,WEBP,SVG,PDF,SKETCH or EPS'
	});
	FilePond.setOptions({
	    server: js_object.folder_uri +'/filepond/'
	});

});