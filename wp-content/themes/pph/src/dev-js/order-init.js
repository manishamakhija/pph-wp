	if ( jQuery( 'body' ).hasClass( 'page-template-order' ) ) {
			jQuery( '.order_form' ).orderProcess();
	}


	jQuery(document).ready(function(){
		// on page load stripe payment calls
		var paymentMethod = jQuery('input[name="payment_type"]');
		if (paymentMethod.is(':checked') &&  paymentMethod.val() == 'stripe'){
			jQuery( '.order_form' ).stripePayment();
		}

	});


	jQuery(document).on('click','.payment-selection ul li input', function(){
		// var _this = jQuery(this);
		// var selectedPay = _this.find('input[name="payment_type"]');
		var selectedPay = jQuery(this);
		if (selectedPay.is(':checked') &&  selectedPay.val() == 'stripe'){
			jQuery( '.order_form' ).stripePayment();
		}else if(selectedPay.is(':checked') &&  selectedPay.val() == 'paypal'){
			jQuery( '.order_form' ).removeStripePayment();
		}
	});