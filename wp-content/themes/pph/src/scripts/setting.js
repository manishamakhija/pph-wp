// var slideCount = null;

jQuery(window).on('scroll load', function () {
	// header on scroll shrink
	if ( jQuery(this).scrollTop() > 0 ) {
		jQuery('.navbar-wrap').addClass( "shrink-nav" );
	} else {
		jQuery('.navbar-wrap').removeClass( "shrink-nav" );
	}

	if(jQuery(window).width() < 1601){
		if ( jQuery(this).scrollTop() > 120 ) {
			jQuery('.services-menu').addClass( "services-menu-shrink" );
		} else {
			jQuery('.services-menu').removeClass( "services-menu-shrink" );
		}
	}
});

jQuery(window).on('load', function() {
	jQuery(".pre-loader").fadeOut("medium");
	/*==============================================================*/
	// mobile menu toggle init
	/*==============================================================*/
	jQuery("<span class='nav-link-toggle'><i class='fa fa-plus'></i></span>").insertBefore(jQuery('.mobile-menu ul.sub-menu'))
	// jQuery('.mobile-menu ul.sub-menu').each(function( index, element ) {
	// 	//jQuery(this).parent('li').prepend("<span class='nav-link-toggle'><i class='fa fa-plus'></i></span>");
	// 	jQuery(this).parent('li').insertBefore(jQuery(this).find('ul.sub-menu'));
	// 	//var currentlimobile=jQuery(this).find('ul.sub-menu').length;
	// 	//console.log(currentlimobile)
	// 	// if(jQuery(currentlimobile).length == 1)
	// 	// {
	// 	// 	jQuery("<span class='nav-link-toggle'><i class='fa fa-plus'></i></span>").insertBefore(jQuery(this).find('ul.sub-menu'))
	// 	// };
	// });
	var link = jQuery(".nav-link-toggle");
	jQuery(".mobile-menu").on('click', '.nav-link-toggle', function(e) {
		e.preventDefault();
		jQuery(this).toggleClass('open');
		jQuery(this).siblings('ul.sub-menu').slideToggle();
		return false;
		// if (jQuery(this).is('.open')) {
		// 	jQuery(this).removeClass('open');
		// 	jQuery(this).next('ul').slideUp();
		// }else{
		// 	jQuery('.mobile-menu .nav-menu ul > li').find('.nav-link-toggle').removeClass('open');
		// 	jQuery('.mobile-menu .nav-menu ul > li').find('ul').slideUp();
		// 	jQuery(this).addClass('open');
		// 	jQuery(this).next('ul').slideDown();
		// }
	});

});



jQuery(document).ready(function() {

	/*==============================================================*/
	// tooltip init
	/*==============================================================*/
	jQuery('[data-toggle="tooltip"]').tooltip()

	/*==============================================================*/
	// Background Image to parent div start
	/*==============================================================*/
	jQuery(".bg_img").each(function(i, elem) {
		var img = jQuery(elem);
		jQuery(this).hide();
		jQuery(this).parent().css({
			background: "url(" + img.attr("src") + ") no-repeat center center",
		});
	});
	/*==============================================================*/
	// Background Image to parent div end
	/*==============================================================*/


	/*==============================================================*/
	// Dropdown Slide Animation start
	/*==============================================================*/
	jQuery('.dropdown').on('show.bs.dropdown', function(e){
		jQuery(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
	});
	jQuery('.dropdown').on('hide.bs.dropdown', function(e){
		jQuery(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
	});

	jQuery('.dropdown-menu').click(function(e) {
		e.stopPropagation();
	});
	/*==============================================================*/
	// Dropdown Slide Animation end
	/*==============================================================*/

	/*==============================================================*/
	// Image to svg convert start
	/*==============================================================*/
	jQuery('img.svg').each(function() {
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');
		jQuery(this).parent().attr('data-src',imgURL);
		jQuery.get(imgURL, function(data) {
			var $svg = jQuery(data).find('svg');
			if (typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			if (typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass + ' replaced-svg');
			}
			$svg = $svg.removeAttr('xmlns:a');
			if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
				$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
			}
			$img.replaceWith($svg);
		}, 'xml');
	});
	/*==============================================================*/
	// Image to svg convert end
	/*==============================================================*/

	/*==============================================================*/
	// data color,bgcolor & border start
	/*==============================================================*/
	jQuery("[data-color]").each(function() {
		jQuery(this).css('color', jQuery(this).attr('data-color'));
	});
	jQuery("[data-bgcolor]").each(function() {
		jQuery(this).css('background-color', jQuery(this).attr('data-bgcolor'));
	});
	jQuery("[data-border]").each(function() {
		jQuery(this).css('border', jQuery(this).attr('data-border'));
	});

	// jQuery("[data-hover-bg]").on('mouseover',function() {
	// 	jQuery(this).css('background', jQuery(this).attr('data-hover-bg'));
	// });


	if(jQuery(window).width() > 1024){
		jQuery(".work-list li").hover(
			function() {
				jQuery(this).css('background', jQuery(this).attr('data-hover-bg'));
			}, function() {
				jQuery(this).removeAttr('style');
			}
		);
	}
	/*==============================================================*/
	// data color,bgcolor & border end
	/*==============================================================*/


	/*==============================================================*/
	// menu
	/*==============================================================*/
	jQuery('.menu-icon > a, .menu-overlay').on('click',function(){
		jQuery('.menu-overlay, .mobile-menu').toggleClass('menu-show');
	});

	jQuery('.nav-login > a').on('click',function(){
		jQuery('.dropdown-menu').slideToggle(300);
	});

	jQuery('.dropdown-overlay').on('click',function(){
		jQuery('.nav-login .dropdown-menu').slideToggle(300);
	});

	jQuery('.quote-service-select ul li').on('click',function(){
		jQuery(this).toggleClass('select');
	});

	jQuery(".quote-service-select.single-select ul li").on("click",function(){
		jQuery(".quote-service-select.single-select ul li").removeClass("select");
		jQuery(this).addClass("select");
	});
	jQuery(".package-select ul li").on("click",function(){
		jQuery(".package-select ul li").removeClass("select");
		jQuery(this).addClass("select");
	});
	jQuery(".delivery-time-select ul li").on("click",function(){
		jQuery(".delivery-time-select ul li").removeClass("select");
		jQuery(this).addClass("select");
	});

	jQuery(".payment-selection ul li").on("click",function(){
		jQuery(".payment-selection ul li").removeClass("select");
		jQuery(this).addClass("select");
	});

	jQuery(".payment-selection ul li.credit-card").on("click",function(){
		jQuery('#credit-card-box').slideDown();
	});
	
	jQuery(".payment-selection ul li.paypal").on("click",function(){
		jQuery('#credit-card-box').slideUp();
	});

	jQuery('.nav-login > a, .dropdown-overlay').on('click',function(){
		jQuery('body').toggleClass('menu-dropdown-show');
	});

	jQuery('.package-select .cta-link').on('click',function(){
		jQuery('.package-select').toggleClass('features-show');
	});

	// jQuery('.nav-menu li.drop_down').hover(function() {
	// 	jQuery(this).toggleClass('drop_down_show');
	// 	jQuery('body').toggleClass('menu-dropdown-show');
	// });

	jQuery('.work-nav .filter-btn').on('click',function(){
		jQuery(this).toggleClass('open');
		jQuery('.work-nav-list').slideToggle();
	});

	jQuery('.blog-search .search a').on('click', function(event){
		jQuery('.blog-search .search').toggleClass('open');
	});

	/*==============================================================*/
	// banner slider
	/*==============================================================*/
	jQuery('.hero-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 700,
		autoplaySpeed: 4000,
		cssEase: 'linear',
		adaptiveHeight: true,
		dots: true,
		vertical: false,
		swipe: true,
		arrows: false,
		autoplay: true,
		fade: true,
		pauseOnHover: false,
	});

	jQuery('.work-slider').on('init', function(event, slick) {
		jQuery(this).append('<div class="slide-count-wrap"><span class="current"></span> / <span class="total"></span></div>');
		jQuery('.current').text(slick.currentSlide + 1);
		jQuery('.total').text(slick.slideCount);
	})
	jQuery('.work-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 700,
		autoplaySpeed: 4000,
		adaptiveHeight: true,
		cssEase: 'linear',
		vertical: false,
		swipe: true,
		// arrows: true,
		autoplay: true,
		fade: true,
		pauseOnHover: false,
		prevArrow: jQuery('.slider_prev'),
		nextArrow: jQuery('.slider_next'),
	})
	jQuery('.work-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
		jQuery('.current').text(nextSlide + 1);
	});

	jQuery('.subscribe-form .form-control').each(function(index, el) {
		jQuery(this).focus(function(){
			jQuery(this).parents('.subscribe-wrap').addClass("is-focus");
		})
		jQuery(this).focusout(function(){
			jQuery(this).parents('.subscribe-wrap').removeClass("is-focus");
		})
	});

	var headerHeight = jQuery('.navbar-wrap').height()
	jQuery( "#services-menu a[href^='#']").on( 'click', function ( e ) {
		e.preventDefault();
		var hash5 = this.hash;
		if(jQuery(window).width() < 1640){
			jQuery( 'html, body' ).animate( {
					scrollTop: jQuery( hash5 ).offset().top - headerHeight - 54 + 2
				}, 700, function () {
			});
		}
		else{
			jQuery( 'html, body' ).animate( {
					scrollTop: jQuery( hash5 ).offset().top - headerHeight + 54 + 2
				}, 700, function () {
			});
		}
	});

	var winWid2 = jQuery(this).outerWidth();
	jQuery(window).on('scroll', function(event) {
		event.preventDefault();
		var winTop = jQuery(this).scrollTop();
		var winWid2 = jQuery(this).outerWidth();
		jQuery('.services-block').each(function(index, el) {
			var servTop = jQuery(this).offset().top - headerHeight + 2 ;
			var servBot =  jQuery(this).outerHeight() + servTop;
			if(winTop >= servTop && winTop <= servBot && winWid2 > 991){
				var servId = jQuery(this).attr('id');
				jQuery('#services-menu a[href="#'+servId+'"]').parents('li').addClass('active');
				jQuery('#services-menu a[href="#'+servId+'"]').parents('li').siblings('li').removeClass('active');
			}

		});
		if(jQuery(window).width() < 1640){
			jQuery('.services-block').each(function(index, el) {
				var servTop = jQuery(this).offset().top - 155 + 2 ;
				var servBot =  jQuery(this).outerHeight() + servTop;
				if(winTop >= servTop && winTop <= servBot && winWid2 > 991){
					var servId = jQuery(this).attr('id');
					jQuery('#services-menu a[href="#'+servId+'"]').parents('li').addClass('active');
					jQuery('#services-menu a[href="#'+servId+'"]').parents('li').siblings('li').removeClass('active');
				}

			});
		}
	});

	jQuery(window).on('scroll', function(event) {
		event.preventDefault();
		if (jQuery('body').hasClass('page-template-services') === true) {
			if(jQuery(window).scrollTop() >= jQuery('.services-block:last-child').offset().top){
				jQuery('.services-menu').addClass('hide');
			}
			else{
				jQuery('.services-menu').removeClass('hide');
			}
		}
	});


	// FilePond.setOptions({
	// 	server: js_object.folder_uri +'/filepond/'
	// });
	// FilePond.registerPlugin(FilePondPluginFileValidateType);
	// FilePond.create(document.querySelector('.my-pond'), {
	// 	acceptedFileTypes: ['image/png','image/jpeg','application/pdf','image/vnd.adobe.photoshop','application/postscript','application/illustrator','image/tiff','image/x-tiff','image/webp','image/svg+xml'],
	// 	fileValidateTypeLabelExpectedTypes: '',
	// 	labelFileTypeNotAllowed: 'File must be AI,PSD,PNG,JPG,TIFF,WEBP,SVG,PDF or EPS'
	// });

	jQuery('.faq-nav a, .goto-section').on('click',function (e) {
		e.preventDefault();
		var target = jQuery(this).data('id');
		jQuery('html, body').stop().animate({
			'scrollTop': jQuery("#"+target).offset().top - 110
		}, 1600, 'swing', function () {
		});
	});
	jQuery(".faq-nav a").on("click",function(){
		jQuery(".faq-nav a").removeClass("active");
		jQuery(this).addClass("active");
	});

	// step on scroll & click active
    jQuery( ".step-nav a[href^='#']").on( 'click', function ( e ) {
		e.preventDefault();
		var hash = this.hash;
		jQuery( 'html, body' ).animate( {
				scrollTop: jQuery( hash ).offset().top - 200
			}, 700, function () {
		});
	});
	var winWid = jQuery(this).outerWidth();
	jQuery(window).on('scroll', function(event) {
		event.preventDefault();
		var winTop = jQuery(this).scrollTop();
		var winWid = jQuery(this).outerWidth();
		jQuery('.step-box').each(function(index, el) {
			var servTop = jQuery(this).offset().top - 200;
			var servBot =  jQuery(this).outerHeight() + servTop;
			if(winTop >= servTop && winTop <= servBot && winWid > 991){
				var servId = jQuery(this).attr('id');
				jQuery('.step-nav a[href="#'+servId+'"]').parents('li').addClass('active');
				jQuery('.step-nav a[href="#'+servId+'"]').parents('li').siblings('li').removeClass('active');
			}

		});
	});
	function removActive(){
		if (winWid <= 991) {
			jQuery('.step-nav li').removeClass('active');
		}
	}
	removActive();
	jQuery(window).resize(function(event) {
		var winWid1 = jQuery(this).outerWidth();
		if (winWid1 <= 991) {
			jQuery('.step-nav li').removeClass('active');
		}
	});


	// package-table
	// DIRTY Responsive pricing table JS
	jQuery( ".package-table ul" ).on( "click", "li", function() {
		var pos = jQuery(this).index()+2;
		jQuery(".package-table tr").find('td:not(:eq(0))').hide();
		jQuery('.package-table td:nth-child('+pos+')').css('display','table-cell');
		jQuery(".package-table tr").find('th:not(:eq(0))').hide();
		jQuery('.package-table li').removeClass('active');
		jQuery(this).addClass('active');
	});
	var mediaQuery = window.matchMedia('(min-width: 767px)');
	mediaQuery.addListener(doSomething);
	function doSomething(mediaQuery) {
		if (mediaQuery.matches) {
			jQuery('.package-table .sep').attr('colspan',3);
		} else {
			jQuery('.package-table .sep').attr('colspan',2);
		}
	}
	doSomething(mediaQuery);
});