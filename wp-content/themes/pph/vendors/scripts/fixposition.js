//Fixed object

function fixPosition(parrentObject, object, offsetTop, offsetBottom) {
	var $object = jQuery(object),
	$parrentObject = jQuery(parrentObject),
	objOffsetY = $object.position().top,
	objHeight,
	objWidth,
	pObjOffsetY,
	pObjHeight,
	scrollHeight,
	startFixed,
	endFixed;

	function computedVars() {
		objHeight = $object.outerHeight();
		objWidth = $object.outerWidth();
		pObjOffsetY = $parrentObject.offset().top;
		pObjHeight = $parrentObject.outerHeight();
		scrollHeight = jQuery(window).scrollTop() + offsetTop;
		startFixed = pObjOffsetY + objOffsetY;
		endFixed = pObjOffsetY + pObjHeight - objHeight - offsetBottom;
	}

	function fixPositionInit() {
		if (window.matchMedia('(min-width: 1025px)').matches) {
			computedVars();

			if(scrollHeight > startFixed && scrollHeight < endFixed){
				$object.attr('style', '');
				$object.css({
					'position' : 'relative',
					'top' : scrollHeight - pObjOffsetY - objOffsetY + 'px'
				});
			} else if(scrollHeight > endFixed){
				$object.attr('style', '');
				$object.css({
					'position' : 'relative',
					'top' : pObjHeight - objOffsetY - objHeight - offsetBottom + 'px'
				});
			}
		} else {
			$object.attr('style', '');
		}
	}

	jQuery(window).on('load',function() {
		fixPositionInit();
	});

	jQuery(window).on('resize', function(){
		fixPositionInit();
	});

	jQuery(window).scroll(function() {
		if (window.matchMedia('(min-width: 1025px)').matches) {
			computedVars();

			if(scrollHeight > startFixed && scrollHeight < endFixed){
				$object.attr('style', '');
				$object.css({
					'position' : 'fixed',
					'top' : offsetTop + 'px',
					'width' : objWidth + 'px'
				});
			} else if(scrollHeight >= endFixed){
				$object.attr('style', '');
				$object.css({
					'position' : 'relative',
					'top' : pObjHeight - objOffsetY - objHeight - offsetBottom + 'px'
				});
			} else if(scrollHeight <= startFixed) {
				$object.attr('style', '');
				$object.css({
					'position' : 'relative',
					'top' : 0 + 'px'
				});
			}
		} else {
			$object.attr('style', '');
		}
	});
}


jQuery(document).ready(function() {

	//Fixed Position faq nav
	if (jQuery('body').hasClass('order-page') === true) {
		fixPosition('.order-column','.summary-box', 140 , 300);
		fixPosition('.order-column','.step-nav .navbar', 100 , 0);
	}

	if (jQuery('body').hasClass('faq-page') === true) {
		fixPosition('.faq-wrap-content','.faq-nav', 140 , 0);
	}

	if (jQuery('body').hasClass('blog-detail-body') === true) {
		fixPosition('.blog-detail-container','.share-box', 140 , 0);
	}

});