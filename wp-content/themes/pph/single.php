<?php
/**
 * The template for displaying all single posts
 *
 */

get_header(); global $post;
?>
	<div class="blog-detail-desc bg-light-blue">
		<div class="blog-detail-container">
			<div class="left-share">
				<div class="share-box">
					<span>share</span>
					<div class="social-icon">
						<ul>
							<li>
								<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
									<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/facebook.svg" alt="facebook-share">
								</a>
							</li>

							<li>
								<a href="<?php echo add_query_arg( array( 'url'=> get_permalink(), 'text'=> urlencode( get_the_title() ) ), 'http://twitter.com/share' ); ?>" target="_blank">
									<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/twitter.svg" alt="">
								</a>
							</li>

							<li>
								<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>" target="_blank">
									<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/linkedin.svg" alt="">
								</a>
							</li>

							<li>
								<a href="<?php echo add_query_arg( array( 'url' => get_the_permalink(), 'media' => get_the_post_thumbnail_url(), 'description' => urlencode( get_the_title() ) ), 'http://pinterest.com/pin/create/button/' ); ?>" target="_blank">
									<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/pinterest.svg" alt="">
								</a></li>

							<li>
								<a href="https://www.instagram.com/" target="_blank">
									<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/instagram.svg" alt="">
								</a>
							</li>
						</ul>
						<?php /*echo do_shortcode('[DISPLAY_ULTIMATE_PLUS]');*/ ?>
					</div>
				</div>
			</div>
			<div class="blog-detail-title">
				<h3><?php the_title(); ?></h3>
				<div class="article-details">
					<?php $post_categories = wp_get_post_categories( $post->ID ); $count_cat = count($post_categories); $i = 1;
						foreach($post_categories as $cat_id){
							echo '<a href="'.get_category_link($cat_id).'">'.get_cat_name($cat_id).'</a>';
							echo $count_cat == $i ? '' : ', ';
							$i++;
						} ?>
					<span class="separator">|</span>
					<span><?php the_time('F j, Y'); ?></span>
				</div>
			</div>
			<div class="detail-banner-img">
				<?php $image = get_field('detail_page_image'); ?>
				<img src="<?php echo get_field('detail_page_image')['url']; ?>" alt="<?php echo get_field('detail_page_image')['alt']; ?>">
			</div>
			<div class="desc">
				<?php
				while(have_posts()):the_post();
					the_content();
				endwhile; ?>
			</div>
			<div class="blog-footer-share">
				<span>SHARE THIS ARTICLE ON</span>
				<div class="social-icon">
					<ul>
						<li>
							<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
								<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/facebook.svg" alt="facebook-share">
							</a>
						</li>

						<li>
							<a href="<?php echo add_query_arg( array( 'url'=> get_permalink(), 'text'=> urlencode( get_the_title() ) ), 'http://twitter.com/share' ); ?>" target="_blank">
								<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/twitter.svg" alt="">
							</a>
						</li>

						<li>
							<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>" target="_blank">
								<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/linkedin.svg" alt="">
							</a>
						</li>

						<li>
							<a href="<?php echo add_query_arg( array( 'url' => get_the_permalink(), 'media' => get_the_post_thumbnail_url(), 'description' => urlencode( get_the_title() ) ), 'http://pinterest.com/pin/create/button/' ); ?>" target="_blank">
								<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/pinterest.svg" alt="">
							</a></li>

						<li>
							<a href="https://www.instagram.com/" target="_blank">
								<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/instagram.svg" alt="">
							</a>
						</li>
					</ul>
					<?php /*echo do_shortcode('[DISPLAY_ULTIMATE_PLUS]');*/ ?>
				</div>
			</div>
			<div class="blog-detail-pagination">
				<div class="row"><?php
					$previous_post_id = get_adjacent_post(false,'',false) != '' ? get_adjacent_post(false,'',false)->ID : ''; ?>
					<div class="col-md-6 col-sm-12 prev pagination-link">
					<?php if ($previous_post_id != ''): ?>

							<a href="<?php echo get_permalink($previous_post_id); ?>">
								<div class="cta-btn">
									<span class="cta-link cta-style2"><span>PREV</span></span>
								</div>
								<div class="pagination-text"><?php echo get_the_title($previous_post_id); ?></div>
							</a>
						<?php
					endif; ?>
					</div>
						<?php
					$next_post_id = get_adjacent_post(false,'',true) != '' ? get_adjacent_post(false,'',true)->ID : ''; ?>
					<div class="col-md-6 col-sm-12 next pagination-link">
					<?php if ($next_post_id != ''): ?>

							<a href="<?php echo get_permalink($next_post_id); ?>">
								<div class="cta-btn">
									<span class="cta-link cta-style2"><span>NEXT</span></span>
								</div>
								<div class="pagination-text"><?php echo get_the_title($next_post_id); ?></div>
							</a>
						<?php
					endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="comment-wrap bg-light-blue">
		<?php comments_template(); ?>
	</div>

	<div class="blog-list-wrap">
		<div class="container">
			<h3 class="related-title">Related Articles</h3>
			<div class="blog-list">
				<?php echo do_shortcode('[rpdp_related_posts]'); ?>
			</div>
		</div>
	</div>
<?php 
if (!is_user_logged_in()){
	echo get_template_part('template-parts/newsletter','form'); 
} ?>
<?php get_footer();
