<?php
get_header();
?>

	<div class="work-banner dot-pattern">
		<div class="container">
			<h1><?php printf( __( 'Search for: %s', 'pph' ), get_search_query() ); ?></h1>
		</div>
	</div>

	<div class="blog-list-wrap bg-light-blue">
		<div class="container">
			<div class="blog-categories-search">
				<div class="left">
					<div class="dropdown">
						<button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							FILTER BY CATEGORY
						</button>
						<div class="dropdown-menu">
							<ul><?php 
							$categories = get_categories(array('hide_empty'=>false));
							foreach($categories as $category){ 
								echo '<li><a href="'.get_category_link($category->term_id).'">'.$category->name.'</a></li>'; 
							}?>
							</ul>
						</div>
					</div>
				</div>
				<div class="right">
					<div class="blog-search">
						<div class="search clearfix">
							<a href="javascript:;"><i class="fa fa-search"></i></a>
							<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		                        <input type="text" class="search-fild" value="" name="s" autofocus placeholder="Search">
		                        <input type="hidden" name="post_type" value="post" />  
			                </form>
						</div>
					</div>
				</div>
			</div><?php 
			if (have_posts()): ?>
				<div class="blog-list">
					<ul><?php global $post; $p = 1 ; $q = 1; $flag = false; $reset = false;
						while(have_posts()):the_post(); 
							if ($p == 4 && $q == 1) { 
								$flag = true;
							} ?>
							<li <?php echo $flag ? 'class="large"' : '' ; ?>>
								<div class="thumb-image" style="background-image: url(<?php echo ( $flag ? get_field('detail_page_image')['url'] : get_the_post_thumbnail_url()); ?>);"></div>
								<div class="thumb-text">
									<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
									<div class="article-details">
										<?php $post_categories = wp_get_post_categories( $post->ID );
										foreach($post_categories as $cat_id){
											echo '<a href="'.get_category_link($cat_id).'">'.get_cat_name($cat_id).'</a>';
										}?>
										<span class="separator">|</span>
										<span><?php the_time('F j, Y'); ?></span>
									</div>
								</div>
							</li><?php 
							$flag = false;
							if ($p == 4) { $q++; }
							if ($q == 4){
								$flag = true;
								$reset = true;
							} 
							if ($q == 1) { $p++; }
							if ($p == 4 && $q == 5 && $reset) { $p = 1; $q = 1; $reset = false; }
						endwhile; ?>
					</ul>
				</div>
				<div class="pagination-wrap">
					<?php do_action('pph_pagination'); ?>
				</div><?php 
			else:
				echo '<h3 style="text-align:center;" class="no-post">No Posts in this category</h3>';
			endif; ?>
		</div>
	</div>

<?php 
if (!is_user_logged_in()){
	echo get_template_part('template-parts/newsletter','form'); 
} ?>
<?php get_footer();
