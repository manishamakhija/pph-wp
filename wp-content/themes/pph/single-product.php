<?php 
get_header(); ?>

<div class="services-detail-block">
	<div class="container">
		<h1 class="service-title"><?php the_title(); ?></h1>
		<div class="service-banner-price">
			<span>Starting at <strong><?php the_field('start_price'); ?></strong></span>
		</div><?php 
		if (get_field('features')): ?>
			<ul class="bullet-list"><?php 
				while(has_sub_field('features')): ?>
					<li><?php the_sub_field('features_name'); ?></li><?php 
				endwhile; ?>
			</ul><?php 
		endif; ?>
		<div class="cta-btn text-center">
			<a href="<?php echo get_permalink(36) ?>" class="cta-link">ORDER NOW</a>
		</div>
	</div>
</div>

<div class="service-intro bg-light-blue">
	<div class="container">
		<?php while(have_posts()):the_post();
			the_content(); 
		endwhile; ?>
	</div>
</div>

<div class="reasons-list-wrap">
	<div class="container">
		<div class="title">
			<h2><?php the_field('reasons_title'); ?></h2>
		</div><?php 
		if (get_field('reasons')): ?>
			<ul class="reasons-list"><?php 
				while(has_sub_field('reasons')): ?>
					<li>
						<div class="reasons-list-box">
							<?php $icon = get_sub_field('icon'); ?>
							<div class="icon"><img src="<?php echo $icon['url']; ?>" class="svg" alt="<?php echo $icon['alt']; ?>"></div>
							<?php the_sub_field('text'); ?>
						</div>
					</li><?php 
				endwhile; ?>
			</ul><?php 
		endif; ?>
	</div>
</div>

<div class="home-work-section">
	<div class="work-slider"><?php 
		$posts = get_field('featured_projects');
		if ($posts): 
			foreach($posts as $post){ 
				setup_postdata($post); ?>
				<div class="work-slide" data-bgcolor="#<?php the_field('background_color'); ?>">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-md-7 col-sm-12">
								<div class="work-slider-img">
									<?php $image = get_field('home_image'); ?>
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
								</div>
							</div>
							<div class="col-md-5 col-sm-12">
								<div class="work-slider-text">
									<h5>FEATURED PROJECT</h5>
									<h3><?php the_title(); ?></h3>
									<?php the_field('home_excerpt'); 
									if ($website_link = get_field('website_link')): ?>
										<div class="cta-btn">
											<a href="<?php echo $website_link; ?>" class="cta-link cta-outline cta-white" target="_blank">VISIT WEBSITE</a>
										</div><?php 
									endif; ?>
								</div>
							</div>
						</div>
						<div class="slide-count-wrap">
							<span class="current"></span> <span class="separator"></span>
							<span class="total"></span>
						</div>
					</div>
				</div><?php 
			} wp_reset_postdata();
		endif; ?>
	</div>
	<div class="slider_arrow">
		<div class="slider_prev"><span>PREV</span></div>
		<div class="slider_next"><span>NEXT</span></div>
	</div>
</div>

<div class="package-wrap bg-light-blue">
	<div class="container">
		<div class="title">
			<h2><?php the_title(); ?> PACKAGES</h2>
			<div class="title-sub-text">
				<?php the_field('package_excerpt'); ?>
			</div>
		</div>

		<div class="package-table">
			<ul>
				<li class="standard">
					<button>Basic Package </button>
				</li>
				<li class="premium active">
					<button>Premium Package</button>
				</li>
			</ul>
			<table class="table">
				<thead>
					<tr>
						<th class="hide"></th>
						<th class="standard default">Basic Package</th>
						<th class="premium">Premium Package</th>
					</tr>
				</thead>
				<tbody>
						<tr>
							<td class="price-title">Package price</td>
							<td class="price"><span class="txt-top">$</span><span class="txt-l"><?php while (has_sub_field('packages_details')): if (get_row_layout() == 'basic_package_price')  the_sub_field('package_price'); endwhile; ?></span></td>
							<td class="default price"><span class="txt-top">$</span><span class="txt-l"><?php while (has_sub_field('packages_details')):  if (get_row_layout() == 'premium_package_price') the_sub_field('package_price'); endwhile; ?></span></td>
						</tr>
						<tr>
							<td colspan="3" class="sep">Features</td>
						</tr><?php 
						while (has_sub_field('packages_details')):
							if (get_row_layout() == 'package_features'): ?>
								<tr>
									<td class="text"><?php the_sub_field('feature'); ?></td>
									<td><?php echo (get_sub_field('standard') ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>'); ?></td>
									<td class="default"><?php echo (get_sub_field('premium') ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>'); ?></td>
								</tr><?php 
							endif; 
						endwhile; ?>
					<tr>
						<td class="tfooter"></td>
						<td class="tfooter bl-0">
							<div class="cta-btn"><a href="<?php echo get_permalink(36) ?>" class="cta-link cta-sm"><span>ORDER NOW</span></a></div>
						</td>
						<td class="default tfooter bl-0">
							<div class="cta-btn"><a href="<?php echo get_permalink(36) ?>" class="cta-link cta-sm"><span>ORDER NOW</span></a></div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="how-we-work-wrap">
	<div class="container">
		<div class="title">
			<h2><?php the_title(); ?> CONVERSION STEPS</h2>
			<div class="title-sub-text">
				<?php the_field('steps_excerpt'); ?>
			</div>
		</div>
		<div class="work-process service-detail-process">
			<ul><?php $counter = 1;
				while(has_sub_field('steps')): ?>
					<li>
						<div class="icon">
							<?php $icon = get_sub_field('icon'); ?>
							<img src="<?php echo $icon['url']; ?>" class="svg" alt="<?php echo $icon['alt']; ?>">
						</div>
						<div class="name">
							<?php the_sub_field('title'); ?>
						</div>
						<div class="line"></div>
						<div class="circle-dot"></div>
					</li><?php
						if ($counter == 3) { ?>
							</ul>
							<div class="process-connecter">
								<div class="right-corner"></div>
								<div class="line"></div>
								<div class="left-corner"></div>
							</div>
							<ul class="second-row"> <?php
						} 
						$counter++;
				endwhile;	?>
			</ul>
		</div>
	</div>
</div>

<div class="service-quote-wrap bg-light-blue">
	<div class="container">
		<div class="title">
			<h2>GET A QUICK QUOTE</h2>
			<div class="title-sub-text">
				<?php the_field('quick_quote_subtitle'); ?>
			</div>
		</div>
		<form method="POST" enctype="multipart/form-data" class="service-quote multiple_uploads_form" name="service-quote" id="service-quote">
			<div class="form-wrap small-wrap">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<input type="text" placeholder="Your Name*" name="name" class="form-control required">
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<input type="text" placeholder="Email Address*" name="email" class="form-control email required">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="form-group">
							<textarea placeholder="Message" class="form-control" name="message"></textarea>
						</div>
					</div>
				</div>
				<div class="form-note">We keep all information confidential and automatically agree to NDA.</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="form-group">
							<input type="file" class="my-pond" name="filepond[]" multiple />
						</div>
					</div>
				</div>
				<div class="form-note">Multiple files supported ( We take AI, PSD, PNG, JPG, TIFF, WEBP, SVG, PDF, SKETCH, EPS. )</div>
				<div class="form-group">
					<div class="cta-btn text-center">
						<button class="cta-link btn-block" type="submit">SEND</button>
						<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/ajax-loader.gif" class="ajax-loader" alt="loader" />
						<input type="hidden" value="<?php echo ucwords( strtolower ( get_the_title() ) ); ?>" name="form_type">
						<input type="hidden" value="<?php echo ucwords( strtolower ( get_the_title() ) ); ?>" name="service">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>


<?php get_footer(); ?>