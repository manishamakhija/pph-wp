<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>
<div class="footer-wrap">
	<footer>
		<div class="container">
			<ul class="footer-column-list">
				<li>
					<h5>QUICK LINKS</h5>
					<div class="f-quick-links">
						<?php  wp_nav_menu(array('menu'=>'Quick Links 1','container'=>''));
						wp_nav_menu(array('menu'=>'Quick Links 2','container'=>'')); ?>
					</div>
				</li>
				<li>
					<h5>GET IN TOUCH</h5>
					<div class="f-quick-links get-in-touch-links">
						<?php wp_nav_menu(array('menu'=>'Get in Touch','container'=>'')); ?>
					</div>
					<div class="social-touch">
						<ul><?php
							if ($email = get_field('email', 'option')): ?>
								<li><a href="mailto:<?php echo $email; ?>"> <i class="fa fa-envelope"></i> <?php echo $email; ?></a></li><?php
							endif;
							if ($skype = get_field('skype', 'option')): ?>
								<li><a href="skype:<?php echo $skype; ?>?chat"><i class="fa fa-skype"></i> <?php echo $skype; ?></a></li><?php
							endif; ?>

						</ul>
					</div>
				</li>
				<li>
					<h5>FOLLOW US</h5>
					<div class="social-icon">
						<ul>
							<li><a href="<?php echo get_field('facebook', 'option'); ?>" target="_blank"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/facebook.svg" alt=""></a></li>
							<li><a href="<?php echo get_field('twitter', 'option'); ?>" target="_blank"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/twitter.svg" alt=""></a></li>
							<li><a href="<?php echo get_field('linkedin', 'option'); ?>" target="_blank"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/linkedin.svg" alt=""></a></li>
							<li><a href="<?php echo get_field('pinterest', 'option'); ?>" target="_blank"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/pinterest.svg" alt=""></a></li>
							<li><a href="<?php echo get_field('instagram', 'option'); ?>" target="_blank"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/instagram.svg" alt=""></a></li>
						</ul>
					</div>
					<div class="we-accept">
						<h5>WE ACCEPT</h5>
						<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/card-icons@2x.png" alt="">
					</div>
				</li>
			</ul>
		</div>
	</footer>
	<div class="copy-right-wrap">
		<p>Copyrights © <?php echo date('Y'); ?> Pixel Perfect HTML.
		Site by <a href="https://www.krishaweb.com/" target="_blank">KrishaWeb Inc.</a></p>
	</div>
</div>

</main>
<?php wp_footer(); ?>
	<!--Start of Zendesk Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="https://v2.zopim.com/?37DYOIiIhQW5BQof79dx4mdbnFE5iY52";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
	<!--End of Zendesk Chat Script-->
</body>
</html>