<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 16px; line-height: 19px; color: #273270; font-weight: 700; text-align: left; padding-bottom: 20px;"><?php echo $email_heading ?></td>
</tr>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 13px;"><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></td>
</tr>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php printf( esc_html__( 'Someone has requested a new password for the following account on %s:', 'woocommerce' ), esc_html( wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES ) ) ); ?></td>
</tr>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php printf( esc_html__( 'Username: %s', 'woocommerce' ), esc_html( $user_login ) ); ?></td>
</tr>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php esc_html_e( 'If you didn\'t make this request, just ignore this email. If you\'d like to proceed:', 'woocommerce' ); ?></td>
</tr>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 17px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 10px;">
		<a class="link" href="<?php echo esc_url( add_query_arg( array( 'key' => $reset_key, 'id' => $user_id ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) ); ?>"><?php esc_html_e( 'Click here to reset your password', 'woocommerce' ); ?>
		</a>
	</td>
</tr>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 17px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php esc_html_e( 'Thanks for reading.', 'woocommerce' ); ?></td>
</tr>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left;">Peter Collins</td>
</tr>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 700; text-align: left;">Pixel Perfect HTML</td>
</tr>
<?php do_action( 'woocommerce_email_footer', $email );