<?php
/**
 * Customer note email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-note.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email );

$project_name = get_post_meta( $order->get_order_number(),'project_name',true );
foreach ($order->get_items() as $item_key => $item ):
     $item_name    = $item->get_name();
     // $product_id   = $item->get_product_id();
endforeach; ?>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 16px; line-height: 19px; color: #273270; font-weight: 700; text-align: left; padding-bottom: 20px;"><?php echo $email_heading ?></td>
</tr>

<?php /* translators: %s: Customer first name */ ?>
<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 13px;"><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></td>
</tr>

<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php esc_html_e( 'The following note has been added to your order:', 'woocommerce' ); ?></td>
</tr>

<blockquote><?php echo wpautop( wptexturize( make_clickable( $customer_note ) ) ); ?></blockquote>

<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php esc_html_e( 'As a reminder, here are your order details:', 'woocommerce' ); ?></td>
</tr>

<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php printf( esc_html__( 'Project Name: %s', 'woocommerce' ), esc_html( $project_name ) ); ?></td>
</tr>

<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php printf( esc_html__( 'Service: %s', 'woocommerce' ), esc_html( $item_name ) ); ?></td>
</tr>

<tr>
	<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;"><?php printf( esc_html__( 'Order ID: %s', 'woocommerce' ), esc_html( $order->get_order_number() ) ); ?></td>
</tr>

<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
// do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
// do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
// do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

?>

<?php

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
