<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$file_path = get_template_directory_uri().'/email-templates/images'; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<body>
	<table cellpadding="0" cellspacing="0" width="600" border="0" align="center">
		<tr>
			<td align="center" style="background-color: #f6f5fb;">
				<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0">
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px;"><a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php echo $file_path; ?>/logo.png" alt="logo" width="229" height="71" style="display: block; border:none;"></a></td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 0 40px 30px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: left 8px bottom -3px;">
							<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
								<tr>
									<td align="center" valign="top" style="padding: 30px; background-color: #fff; border: 1px solid #EDEDED; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
										<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">