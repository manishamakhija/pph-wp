<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates/Emails
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$file_path = get_template_directory_uri().'/email-templates/images'; 
?>


</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px 0px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: right 10px top;">
							<?php echo do_shortcode('[pph-mail-latest-posts]'); ?>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px;">
							<?php echo do_shortcode('[pph-mail-social-network]'); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
</body>
</html>