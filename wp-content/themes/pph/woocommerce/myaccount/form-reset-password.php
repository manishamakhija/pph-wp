<?php
/**
 * Lost password reset form.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 */

defined( 'ABSPATH' ) || exit;

get_header();
?>
<div class="login-page-wrap dot-pattern blue w-40">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/login-lego.png" alt="" class="d-md-block d-none">
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="login-box">
					<div class="form-wrap login-form">
						<!-- title start -->
						<div class="title text-left">
							<h2>Reset Password</h2>
						</div>
						<?php do_action( 'woocommerce_before_reset_password_form' ); ?>
						<form method="post" class="woocommerce-ResetPassword lost_reset_password">

							<!-- <p> -->
								<?php/* echo apply_filters( 'woocommerce_reset_password_message', esc_html__( 'Enter a new password below.', 'woocommerce' ) ); */ ?></p><?php // @codingStandardsIgnoreLine ?>

							<div class="form-group">
								<div class="input-icon">
							<!-- <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first"> -->
									<input type="password" class="form-control woocommerce-Input woocommerce-Input--text input-text" name="password_1" id="password_1" placeholder="New password" autocomplete="new-password" />
									<span class="icon-input"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/lock.svg" alt="" class="svg"></span>
								</div>
							</div>
							<!-- </p> -->
							<!-- <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last"> -->
								<div class="form-group">
									<div class="input-icon">
										<input type="password" class="form-control woocommerce-Input woocommerce-Input--text input-text" name="password_2" id="password_2" placeholder="Re-enter new password" autocomplete="new-password" />
										<span class="icon-input"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/lock.svg" alt="" class="svg"></span>
									</div>
								</div>
							<!-- </p> -->

							<input type="hidden" name="reset_key" value="<?php echo esc_attr( $args['key'] ); ?>" />
							<input type="hidden" name="reset_login" value="<?php echo esc_attr( $args['login'] ); ?>" />

							<div class="clear"></div>

							<?php do_action( 'woocommerce_resetpassword_form' ); ?>

							<div class="row align-items-center">
								<div class="col">
									<div class="submit-btn cta-btn">
										<input type="hidden" name="wc_reset_password" value="true" />
										<button type="submit" class="cta-link btn-block woocommerce-Button" value="<?php esc_attr_e( 'Save', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset', 'woocommerce' ); ?></button>
									</div>
								</div>
								<div class="col"></div>
							</div>
<!-- 							</p>
 -->
							<?php wp_nonce_field( 'reset_password', 'woocommerce-reset-password-nonce' ); ?>

						</form>
						</div>
					<div class="cta-btn text-center">
						<a href="<?php echo get_site_url(); ?>" class="cta-link cta-style2"><span>BACK TO HOME</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
do_action( 'woocommerce_after_reset_password_form' );

