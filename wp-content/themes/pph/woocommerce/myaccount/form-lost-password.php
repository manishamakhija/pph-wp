<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;

get_header();
?>

<div class="login-page-wrap dot-pattern blue w-40">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<img src="<?php echo get_template_directory_uri(); ?>/vendors/images/login-lego.png" alt="" class="d-md-block d-none">
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="login-box">
					<div class="form-wrap login-form">
						<!-- title start -->
						<div class="title text-left">
							<h2>Forgot Password</h2>
						</div>
						<!-- title end -->
						<form method="post" class="woocommerce-ResetPassword lost_reset_password">

							<div class="form-group">
								<div class="input-icon">
									<input class="form-control woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" autocomplete="username" placeholder="Registered Email Address"/>
									<span class="icon-input"><img src="<?php echo get_template_directory_uri(); ?>/vendors/images/user.svg" alt="" class="svg"></span>
								</div>
							</div>
							<?php do_action( 'woocommerce_lostpassword_form' ); 
							do_action( 'woocommerce_before_lost_password_form' ); ?>

							<div class="row align-items-center">
								<div class="col">
									<div class="submit-btn cta-btn">
										<input type="hidden" name="wc_reset_password" value="true" />
										<button type="submit" class="cta-link btn-block woocommerce-Button" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset', 'woocommerce' ); ?></button>
									</div>
								</div>
								<div class="col"></div>
							</div>

							<?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>

						</form>
					</div>
					<div class="cta-btn text-center">
						<a href="<?php echo get_site_url(); ?>" class="cta-link cta-style2"><span>BACK TO HOME</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
do_action( 'woocommerce_after_lost_password_form' );
