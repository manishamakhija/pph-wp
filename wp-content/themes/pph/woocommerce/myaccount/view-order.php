<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_DIR ?>/dashboard/dev-styles/trumbowyg.css">
<p><?php
	/* translators: 1: order number 2: order date 3: order status */
	// printf(
	// 	__( 'Order #%1$s was placed on %2$s and is currently %3$s.', 'woocommerce' ),
	// 	'<mark class="order-number">' . $order->get_order_number() . '</mark>',
	// 	'<mark class="order-date">' . wc_format_datetime( $order->get_date_created() ) . '</mark>',
	// 	'<mark class="order-status">' . wc_get_order_status_name( $order->get_status() ) . '</mark>'
	// );
?></p>

<?php /*if ( $notes = $order->get_customer_order_notes() ) : ?>
	<h2><?php _e( 'Order updates', 'woocommerce' ); ?></h2>
	<ol class="woocommerce-OrderUpdates commentlist notes">
		<?php foreach ( $notes as $note ) : ?>
		<li class="woocommerce-OrderUpdate comment note">
			<div class="woocommerce-OrderUpdate-inner comment_container">
				<div class="woocommerce-OrderUpdate-text comment-text">
					<p class="woocommerce-OrderUpdate-meta meta"><?php echo date_i18n( __( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?></p>
					<div class="woocommerce-OrderUpdate-description description">
						<?php echo wpautop( wptexturize( $note->comment_content ) ); ?>
					</div>
	  				<div class="clear"></div>
	  			</div>
				<div class="clear"></div>
			</div>
		</li>
		<?php endforeach; ?>
	</ol>
<?php endif; */?>

<?php //do_action( 'woocommerce_view_order', $order_id ); ?>
<?php foreach ($order->get_items() as $item_key => $item ):
        $item_id = $item->get_id(); 
        $item_name    = $item->get_name();
     	// $product_id   = $item->get_product_id();
    endforeach; 
    $inner_page_count = wc_get_order_item_meta($item_id,'inner_page_count', true );
    $inner_page_count = $inner_page_count == "" ? 0 : $inner_page_count; 
    global $wpdb;
    $payment_transaction_id = $wpdb->get_var('SELECT comment_content FROM wp_comments WHERE comment_karma = 1 AND comment_post_ID = '.$order_id); ?>
<div class="dash-wrap-header">
	<div class="d-sm-flex justify-content-between align-items-center">
		<h1>ORDER #<?php echo $order->get_order_number(); ?></h1>
	</div>
</div>
<div class="dash-project-detail-wrap">
	<div class="left">
		<div class="dash-box mb-5">
			<div class="dash-project-info-detail">
				<div class="dash-project-info-detail-top">
					<div class="top-left">
						<h1><?php echo get_post_meta( $order_id, 'project_name' ,true ); ?></h1>
						<div class="dash-package-info">
							<?php echo ucwords( strtolower( $item_name ) ); ?> <span>(Homepage + <?php echo sprintf( _n('%s Innerpage', '%s Innerpages', $inner_page_count ), $inner_page_count ); ?>)</span>
						</div>
					</div>
					<div class="top-right">$<?php echo get_post_meta($order_id, 'total_order_amount', true ); ?></div>
				</div>
				<div class="dash-project-info-list">
					<ul>
						<li>
							<strong>EMAIL</strong>
							<a href="mailto:<?php echo $order->get_billing_email(); ?>"><?php echo $order->get_billing_email(); ?></a>
						</li>
						<li>
							<strong>PHONE NUMBER</strong>
							<a href="tel:<?php echo $order->get_billing_phone(); ?>"><?php echo $order->get_billing_phone(); ?></a>
						</li><?php 
						if ($skype = get_post_meta( $order_id, 'skype_name', true )): ?>
							<li>
								<strong>SKYPE</strong>
								<a href="skype:<?php echo $skype; ?>?chat"><?php echo $skype; ?></a>
							</li><?php 
						endif; ?>
						<li>
							<strong>CUSTOMER NAME</strong>
							<?php echo $order->get_billing_first_name(); ?>
						</li>
						<li>
							<strong>ORDER DATE</strong>
							<?php echo wc_format_datetime( $order->get_date_created() ); ?>
						</li>
						<li>
							<strong>DEADLINE</strong>
							<?php echo get_post_meta( $order_id, 'project_deadline', true ); ?>
						</li>
						<li>
							<strong>PAYMENT METHOD</strong>
							<?php echo $order->get_payment_method(); ?><br> <?php echo $payment_transaction_id; ?>
						</li>
						<li>
							<strong>PACKAGE</strong>
							<?php echo get_post_meta( $order_id, 'package', true ); ?>
						</li>
						<li>
							<strong>DELIVERY</strong>
							<?php echo get_post_meta( $order_id, 'delivery', true ); ?>
						</li>
					</ul>
				</div>
			</div>
			<div class="project-files-list">
				<?php $files_uploaded = get_post_meta( $order_id, 'files_included', true ); 
						$count = !empty( $files_uploaded ) ? count( $files_uploaded ) : 0 ; ?>
				<div class="project-files-list-title">PROJECT FILES ( <?php echo sprintf( _n('%s File', '%s Files', $count ), $count ); ?> )</div><?php 
				if ( $count != 0 ) { ?>
					<ul><?php 
						foreach($files_uploaded as $key => $value){ 
							$filepath = get_template_directory_uri().'/filepond/uploads/'.$value['new_name'];
	    					echo '<a href = "'. $filepath .'" download target="_blank"><li>'.$value['original'].'</li></a>';
						} ?>
					</ul><?php
				} ?>
			</div>
			<!-- <div class="dash-box-header">
				<div class="cta-btn">
					<a href="#" class="cta-link cta-style2 p-0"><span>DOWNLOAD ALL FILES</span></a>
				</div>
			</div> -->
		</div>
		<div class="dash-box mb-3">
			<div class="dash-box-header">
				Chat with Project Manager
			</div>
			<div class="manager-chat">
				<div class="chat-box">
					<ul class="chat-discussion"><?php 
						$order_notes = get_customer_order_notes($order_id);
						$current_user = get_user_by( 'id', get_current_user_id() );
						$current_user_mail = $current_user->user_email; 
						if ( !empty( $order_notes ) ): ?>
							<?php 
								foreach( $order_notes as $key => $comment_obj ){ ?>
									<li class="<?php echo ($current_user_mail == $comment_obj->comment_author_email ? 'client' : 'pm'); ?>">
										<div class="manager-name-date">
											<span class="m-name"><?php echo  $comment_obj->comment_author; ?></span>
											<span class="date"><?php echo date('F j, Y', strtotime( $comment_obj->comment_date ) ); ?></span>
										</div>
										<div class="text">
											<?php echo $comment_obj->comment_content; ?>
										</div>
									</li><?php 
								}  ?>
							<?php 
						endif; ?>
					</ul>
					<div class="send-msg-to-pm form-wrap">
						<div class="alert-msg"></div>
						<textarea class="user-msg-to-pm form-control" id="froala-editor"></textarea>
						<input type="hidden" value="<?php echo $order_id ?>" name="current_order_id">
						<div class="chat-buttons text-right cta-btn">
							<input type="button" name="chat_cancel" class="chat-cancel" onclick="clearEditorText()" value="Cancel">
							<input type="button" name="chat_send" class="chat-send cta-link" value="Send">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="right">
		<!-- <div class="manage-meeting">
			<a href="#">
				Manager Schedule Meeting
			</a>
		</div> -->
		<div class="cta-btn">
			<a href="<?php echo TEMPLATE_DIR ?>/dashboard/order-invoice.php?order_id=<?php echo urlencode($order_id); ?>" target="_blank" class="cta-link cta-sm text-center btn-block"><span>Get Invoice</span></a>
		</div>

		<div class="dash-box project-progress-step mb-3">
			<div class="dash-box-header">
				Project Progress
			</div>
			<div class="dash-step-list">
				<ul><?php $order_status = $order->get_status();
					$project_statuses = array('processing','order-development','order-qa','completed','cancelled'); 
					$order_status_no = array_search( $order_status, $project_statuses ); ?>
					<li <?php echo ( $order_status_no == 0 ? 'class="active"' : ( $order_status_no > 0 ? 'class="done"' : '' ) ); ?> >
						<span>STEP 01</span>
						Submit Your Order
					</li>
					<!-- <li class="done">
						<span>STEP 02</span>
						Project Acceptance
					</li> -->
					<li <?php echo ( $order_status_no == 1 ? 'class="active"' : ( $order_status_no > 1 ? 'class="done"' : '' ) ); ?>>
						<span>STEP 02</span>
						Project Slicing & Coding
					</li>
					<li <?php echo ( $order_status_no == 2 ? 'class="active"' : ( $order_status_no > 2 ? 'class="done"' : '' ) ); ?>>
						<span>STEP 03</span>
						Quality Assurance
					</li>
					<li <?php echo ( $order_status_no == 3 ? 'class="active"' : ( $order_status_no > 3 ? 'class="done"' : '' ) ); ?>>
						<span>STEP 04</span>
						You Get Delivery
					</li><?php // cancelled order
					if ( $order_status_no == 4 ){ ?>
						<li class="active">
							Order Cancelled
						</li><?php 
					} ?>
				</ul>
			</div>
		</div>

		<?php if ( $order_status == 'completed' && get_field('client_feedback',$order_id) == '' ){ ?>
			<div class="dash-box mb-3 client-feedback-wrap">
				<div class="dash-box-header">
					Feedback
				</div>
				<div class="dash-feedback">
					<form>
						<div class="alert-msg-fb"></div>
						<div class="dash-form-wrap">
							<div class="form-group">
								<textarea class="form-control client-feedback-input" placeholder="Give us your feedback"></textarea>
							</div>
							<div class="cta-btn text-center">
								<button type="button" class="cta-link cta-sm btn-cfb"><span>Send</span></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		<?php } ?>
		<!-- <div class="dash-box mb-3">
			<div class="dash-box-header">
				Invite Co-Worker
			</div>
			<div class="co-worker-list">
				<ul>
					<li>
						<a href="#" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
							<img src="../vendors/images/avatar.png" alt="">
							<div class="remove fa fa-times"></div>
						</a>
					</li>
					<li>
						<a href="#" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
							<img src="../vendors/images/avatar.png" alt="">
							<div class="remove fa fa-times"></div>
						</a>
					</li>
					<li>
						<a href="#" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
							<img src="../vendors/images/avatar.png" alt="">
							<div class="remove fa fa-times"></div>
						</a>
					</li>
					<li>
						<a href="#" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
							<img src="../vendors/images/avatar.png" alt="">
							<div class="remove fa fa-times"></div>
						</a>
					</li>
					<li>
						<a href="#" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
							<img src="../vendors/images/avatar.png" alt="">
							<div class="remove fa fa-times"></div>
						</a>
					</li>
				</ul>
			</div>
			<div class="dash-feedback">
				<form>
					<div class="dash-form-wrap">
						<div class="form-group">
							<label>CO-WORKER NAME</label>
							<input type="text" class="form-control" placeholder="Co-worker Name">
						</div>
						<div class="form-group">
							<label>CO-WORKER EMAIL</label>
							<input type="text" class="form-control" placeholder="Co-worker Email Address">
						</div>
						<div class="cta-btn text-center">
							<button type="Submit" class="cta-link cta-sm"><span>INVITE</span></button>
						</div>
					</div>
				</form>
			</div>
		</div> -->
	</div>
</div>
<script type="text/javascript" src="<?php echo TEMPLATE_DIR ?>/dashboard/dev-scripts/trumbowyg.min.js"></script>
<script type="text/javascript">
	jQuery('#froala-editor').trumbowyg({
		btns: [
			['viewHTML'],
			['undo', 'redo'],
			// ['formatting'],
			['strong', 'em', 'del'],
			['superscript', 'subscript'],
			['link'],
			['insertImage'],
			['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
			['unorderedList', 'orderedList']
		],
		svgPath: '<?php echo TEMPLATE_DIR ?>/dashboard/images/icons.svg',
	});
</script>