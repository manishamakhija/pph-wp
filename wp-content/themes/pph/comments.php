<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage pph
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<div class="container">
<!-- <div id="comments" class="comments-area title"> -->

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
		<div class="title">
			<h2>
				<?php 
				$comments_number = get_comments_number();
				
				if ( '1' === $comments_number ) {
					/* translators: %s: post title */
					printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'pph' ), get_the_title() );
				} else {
					printf(
						/* translators: 1: number of comments, 2: post title */
						_nx(
							'%1$s Reply to &ldquo;%2$s&rdquo;',
							'%1$s Comments',
							$comments_number,
							//'comments title',
							'pph'
						),
						number_format_i18n( $comments_number ),
						get_the_title()
					);
				}
				?>
			</h2>
		</div>

		<ol class="commentlist">
			<?php
				wp_list_comments('callback=pph_comment');
			?>
		</ol>

		<?php 

	endif; // Check for have_comments().

	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

		<p class="no-comments"><?php _e( 'Comments are closed.', 'pph' ); ?></p>
	<?php
	endif; 

	// $args = array(
	//   'title_reply'       => __( 'Your Comment Here' ),
	//   'title_reply_to'    => __( 'Leave a Reply to %s' ),
	// ); ?>
	<?php if ('open' == $post->comment_status) : ?>


	<div class="title">
		<h2><?php comment_form_title( 'Your Comment Here', 'Leave a Reply to %s' ); ?></h2>
	</div>

	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>

		<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p>

	<?php else : ?>
	<div class="form-wrap">
		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<textarea class="form-control" placeholder="Write Comment" name="comment" id="comment" tabindex="1"></textarea>
					</div>
				</div>
			</div>


		<?php if ( $user_ID ) : ?>

			<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>

		<?php else : ?>

			<div class="row clearfix">
				<div class="col-md-6 col-sm-12">
					<div class="form-group">
						<input type="text" class="form-control" name="author" id="author" placeholder="Name" value="<?php echo $comment_author; ?>" size="22" tabindex="2">
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Email" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="3">
					</div>
				</div>
			</div>
			
		<?php endif; ?>
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="cta-btn text-center">
						<button name="submit" type="submit" id="submit" class="cta-link">POST COMMENT</button>
					</div>
				</div>
			</div>


			<?php comment_id_fields(); ?>
			<?php do_action('comment_form', $post->ID); ?>
	 	</form>
	 </div>
	<?php endif; ?>

<?php endif; ?>
	</div>
<!-- </div> -->

<!-- </div>#comments -->
<!-- </div> container -->