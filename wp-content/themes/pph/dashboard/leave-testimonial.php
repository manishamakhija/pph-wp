<?php 
/*
** Template Name: Dashboard Leave Testimonial
*/

if (!is_user_logged_in()){
	wp_redirect(get_permalink(24)); // login page
	exit;
}
get_template_part('dashboard/header'); ?>

<div class="dash-wrap-header">
	<div class="d-sm-flex justify-content-between align-items-center">
		<h1><?php the_title(); ?></h1>
	</div>
</div>

<div class="dash-box change-password-box">
	<?php echo do_shortcode('[contact-form-7 id="249389" title="Leave a Testimonial"]'); ?>
</div>

<?php get_template_part('dashboard/footer'); 