<?php 
/*
** Template Name: Dashboard Change Password
*/

if (!is_user_logged_in()){
	wp_redirect(get_permalink(24)); // login page
	exit;
}
get_template_part('dashboard/header'); ?>
	<div class="dash-wrap-header">
		<div class="d-sm-flex justify-content-between align-items-center">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="reset-password-msg"></div>
	<div class="dash-box change-password-box">
		<form method="POST" name="change_password" id="change-password">
			<div class="dash-form-wrap">
				<div class="row">
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<label>OLD PASSWORD</label>
							<input type="password" class="form-control" name="old_password" placeholder="Old Password">
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<label>NEW PASSWORD</label>
							<input type="password" class="form-control" name="new_password" id="new_password" placeholder="Your New Password">
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<label>CONFIRM NEW PASSWORD</label>
							<input type="password" class="form-control" name="confirm_password" placeholder="Your Confirm New Password">
						</div>
					</div>
				</div>
				<div class="cta-btn text-md-right">
					<button class="cta-link cta-sm password-update-btn" type="submit" name="submit"><span>UPDATE</span></button>
				</div>
			</div>
		</form>
	</div>
<?php get_template_part('dashboard/footer'); 