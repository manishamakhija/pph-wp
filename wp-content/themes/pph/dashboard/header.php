<?php 
define('TEMPLATE_DIR', get_template_directory_uri());
define('PPH_SITE_URL', get_site_url()); ?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="theme-color" content="#EA5351">
	<?php wp_head(); ?>
</head>
<body <?php body_class('pph-dashboard'); ?>>
	<div class="dash-board">
		<?php get_template_part('dashboard/sidebar'); ?>
		<?php get_template_part('dashboard/topbar'); ?>
		<div class="dash-wrap">
			<div class="dash-wrap-content">
				
