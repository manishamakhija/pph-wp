<?php 

/*
** Added class in body for dashboard
*/
// add_filter( 'body_class', function( $classes ) {
//     return array_merge( $classes, array( 'pph-dashboard' ) );
// } );

function is_dashboard_page(){

    if ( is_wc_endpoint_url( get_option('woocommerce_myaccount_view_order_endpoint',true) ) || is_page_template( array( 'dashboard/project-list.php', 'dashboard/edit-profile.php', 'dashboard/leave-testimonial.php', 'dashboard/change-password.php' ) ) ){
        return true;
    }
}

/*
** Enqueue Dashboard Styles & Scripts
*/
function pph_enqueue_dashboard_scripts(){
	$classes = get_body_class();
	if ( is_dashboard_page() ) {

	    wp_enqueue_style( 'pph-dashboard', get_theme_file_uri( '/vendors/styles/dashboard-style.min.css' ), array(), '1.0');

	    wp_enqueue_script( 'pph-dashboard', get_theme_file_uri( '/vendors/scripts/dashboard-script.min.js' ), array(), '1.0', true );

        wp_localize_script('pph-dashboard', 'obj_dash',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}
}

add_action('wp_enqueue_scripts','pph_enqueue_dashboard_scripts');

/*
** customer send message to Project Manager
*/
function user_send_note_to_pm(){
    if ( isset( $_POST['message'] ) && isset( $_POST['orderId'] ) ){
        $note =  $_POST['message'];
        $user                 = get_user_by( 'id', get_current_user_id() );
        $comment_author = get_user_meta($user->ID,'first_name',true);
        if (!$comment_author) {
            $comment_author = $user->display_name;
        }
        $order_id = sanitize_text_field( $_POST['orderId'] );
        // $comment_author       = $commentor_name;
        $comment_author_email = $user->user_email;
        $commentdata = 
                array(
                    'comment_post_ID'      => $order_id,
                    'comment_author'       => $comment_author,
                    'comment_author_email' => $comment_author_email,
                    'comment_author_url'   => '',
                    'comment_content'      => $note,
                    'comment_agent'        => 'WooCommerce',
                    'comment_type'         => 'order_note',
                    'comment_parent'       => 0,
                    'comment_approved'     => 1,
                );
           
        $comment_id = wp_insert_comment( $commentdata );
        add_comment_meta( $comment_id, 'is_customer_note', 1 );
        $comment_date = date('F j, Y');
        $msg = array('author_name' => $comment_author,'date' => $comment_date);
        wp_send_json($msg);
    }

}

add_action( 'wp_ajax_user_send_note_to_pm', 'user_send_note_to_pm' );
add_action( 'wp_ajax_nopriv_user_send_note_to_pm', 'user_send_note_to_pm' );

/*
** Add project development & QA status in orders
*/
function pph_register_new_order_status() {

    register_post_status( 'wc-order-development', array(
        'label'                     => _x( 'Project Slicing & Coding', 'Order status', 'pixel-perfect-html' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true
        // 'label_count'               => _n_noop( 'Project Slicing & Coding <span class="count">(%s)</span>', 'Project Slicing & Coding<span class="count">(%s)</span>', 'phresh-water-orders' )
    ) );

    register_post_status( 'wc-order-qa', array(
        'label'                     => _x( 'Quality Assurance', 'Order status', 'pixel-perfect-html' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true
        // 'label_count'               => _n_noop( 'Project Slicing & Coding <span class="count">(%s)</span>', 'Project Slicing & Coding<span class="count">(%s)</span>', 'phresh-water-orders' )
    ) );
}
add_action( 'init', 'pph_register_new_order_status' );

/*
** Display new order status on admin side
*/
function pw_orders_new_wc_order_status( $order_status ) {

  $order_status['wc-order-development'] = _x( 'Project Slicing & Coding', 'Order status', 'pixel-perfect-html' ) ;
  $order_status['wc-order-qa'] = _x( 'Quality Assurance', 'Order status', 'pixel-perfect-html' ) ;

  return $order_status;
}
add_filter( 'wc_order_statuses', 'pw_orders_new_wc_order_status' );

/*
** Save Client feedback
*/
function pph_save_client_feedback(){
    if ( isset( $_POST['feedBack'] ) &&  isset( $_POST['orderId'] ) ){
        $feedback = sanitize_textarea_field( $_POST['feedBack'] );
        $order_id = sanitize_text_field( $_POST['orderId'] );
        $update_result = update_post_meta($order_id,'client_feedback',$feedback);
        if ( $update_result ){
            $order = wc_get_order( $order_id );
            $project_name = get_post_meta($order_id,'project_name',true );
            $client_name = $order->get_billing_first_name();
            $mail_content = $client_name.' has given feedback for the project '.$project_name.'. The Feedback is as follows:'."\r\n". $feedback;
            // $to = 'info@pixelperfecthtml.com,peter@pixelperfecthtml.com';
            $to = 'natashaherbert21@gmail.com';
            $subject = 'Your client '.$client_name.' has given feedback for project '.$project_name;
            wp_mail($to, $subject, $mail_content);
            $msg = array('result' => 1,'message' => 'success');
        }else{
            $msg = array('result' => 0,'message' => 'Error');
        }
        wp_send_json($msg);
    }
}
add_action( 'wp_ajax_pph_save_client_feedback', 'pph_save_client_feedback' );
add_action( 'wp_ajax_nopriv_pph_save_client_feedback', 'pph_save_client_feedback' );

/*
** Add client project List in testimonial form
*/
function pph_add_project_list_to_testimonial_form( $tag, $unused ){
    if ( $tag['name'] != 'testi-projects' ){
        return $tag;
    }

    $current_user_id = get_current_user_id();

    // Getting current customer orders
    $customer_orders = wc_get_orders( array(
        'meta_key' => '_customer_user',
        'meta_value' => $current_user_id,
        'numberposts' => -1
    ) ); 
    foreach($customer_orders as $order ){
        $project_name = get_post_meta( $order->get_order_number(), 'project_name', true );
        $tag['raw_values'][] = $project_name;
        $tag['values'][] = $project_name;  
        $tag['labels'][] = $project_name;  
    }
    return $tag;
}
add_filter( 'wpcf7_form_tag', 'pph_add_project_list_to_testimonial_form', 10, 2); 

/*
** Save User Avatar Cropped Image
*/
function pph_crop_user_avatar(){
    // var_dump($_POST);
    $current_user_id = get_current_user_id();
    $croped_image = $_POST['image'];
    list($type, $croped_image) = explode(';', $croped_image);
    list($garbage,$extension) = explode('/', $type);
    list(, $croped_image)      = explode(',', $croped_image);
    $croped_image = base64_decode($croped_image);
    $image_name = $current_user_id.'.'.$extension;
    update_user_meta($current_user_id, 'user_avatar',$image_name);
    // upload cropped image to server 
    $file_path = get_template_directory().'/dashboard/user-avatars/'.$image_name;
    file_put_contents($file_path, $croped_image);
}
add_action( 'wp_ajax_pph_crop_user_avatar', 'pph_crop_user_avatar' );
add_action( 'wp_ajax_nopriv_pph_crop_user_avatar', 'pph_crop_user_avatar' );

/*
** Return user display picture url for edit profile page
*/
function get_avatar_image_url(){
    $current_user = get_current_user_id();
    $image_name = get_user_meta($current_user, 'user_avatar', true);
    $image_path = get_template_directory_uri().'/dashboard/user-avatars/';
    if ($image_name){
        $image_url = $image_path.$image_name;
    }else{
        $image_url = $image_path.'avatar.png';
    }
    return $image_url;
}

/*
** Save User Profile Details
*/
function pph_save_profile_details(){
    if ( isset( $_POST['clientData'] ) ){
        $current_user_id = get_current_user_id();
        $unserialize_clientdata = array();
        parse_str($_POST['clientData'],$unserialize_clientdata);
        update_user_meta( $current_user_id, 'first_name', $unserialize_clientdata['name'] );
        $user_update_status = wp_update_user(array('ID' => $current_user_id, 'user_email'=> $unserialize_clientdata['email'],'user_url'=>$unserialize_clientdata['company_url'] ));

        if ( is_wp_error( $user_update_status ) ) {

            $msg = array('result'=> 0,'message'=> $user_update_status->get_error_message() );

        }else{

            update_user_meta( $current_user_id, 'billing_company', $unserialize_clientdata['company'] );
            // update_user_meta( $current_user_id, 'company_url', $unserialize_clientdata['company_url'] );
            update_user_meta( $current_user_id, 'billing_country', $_POST['countryCode'] );
            update_user_meta( $current_user_id, 'billing_phone', $unserialize_clientdata['phone_no'] );
            update_user_meta( $current_user_id, 'skype', $unserialize_clientdata['skype_name'] );
            update_user_meta( $current_user_id, 'country_text', $_POST['countryText'] );
            update_user_meta( $current_user_id, 'country_ISD_code', $unserialize_clientdata['country_name'] );

            $msg = array('result'=> 1, 'message' => 'Your details have saved successfully.' );
        }
        wp_send_json($msg);
    }
}
add_action( 'wp_ajax_pph_save_profile_details', 'pph_save_profile_details' );
add_action( 'wp_ajax_nopriv_pph_save_profile_details', 'pph_save_profile_details' );

/*
** display Notes from pm and client
*/
function get_customer_order_notes( $order_id ) {
    $notes = array();
    $args  = array(
        'post_id' => $order_id,
        'approve' => 'approve',
        'type'    => '',
        'order'   => 'ASC'
    );

    remove_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

    $comments = get_comments( $args );

    foreach ( $comments as $comment ) {
        if ( ! get_comment_meta( $comment->comment_ID, 'is_customer_note', true ) ) {
            continue;
        }
        $comment->comment_content = make_clickable( $comment->comment_content );
        $notes[]                  = $comment;
    }

    add_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

    return $notes;
}

/*
** Change Password of client
*/
function pph_reset_password(){
    if ( isset( $_POST['passData'] ) ){
        $password_data = array();
        $current_user_id = get_current_user_id();
        parse_str( $_POST['passData'], $password_data );
        $current_password = $password_data['old_password'];
        $new_password = $password_data['new_password'];
        $confirm_password = $password_data['confirm_password']; 
        if ( $current_password == '' || $new_password == '' ){
            $msg = array('result' => 0, 'message' => 'Enter the mandatory fields.');
            wp_send_json($msg);
            exit;
        }

        if ( $new_password !== $confirm_password ){
            $msg = array('result' => 0, 'message' => 'Please confirm the new password.');
            wp_send_json($msg);
            exit;   
        }
        require_once ABSPATH . 'wp-includes/class-phpass.php';
        $wp_hasher = new PasswordHash( 8, true );
        $user_info = get_userdata($current_user_id); 
        $user_pass = $user_info->user_pass;

        if($wp_hasher->CheckPassword($current_password, $user_pass)) {
            wp_set_password($new_password, $current_user_id);
            $current_user = wp_signon(array('user_login' => $user_info->user_login, 'user_password' => $new_password));

            $msg = array('result' => 1, 'message' => 'Password has been reset successfully.');
            // wp_send_json($msg);
        }else{
            $msg = array('result' => 0, 'message' => 'Your current password is not correct.');
        }
        wp_send_json($msg);
    }

}
add_action('wp_ajax_pph_reset_password', 'pph_reset_password');
add_action('wp_ajax_nopriv_pph_reset_password', 'pph_reset_password');