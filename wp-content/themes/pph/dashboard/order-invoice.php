<?php 
require '../../../../wp-config.php'; 
if ( !isset($_SERVER['HTTP_REFERER'] ) && $_SERVER['HTTP_REFERER'] == "" ){
  wp_redirect( get_site_url() );
}

$order_id = isset( $_GET['order_id'] ) ? urldecode($_GET['order_id']) : ''; 
$order = wc_get_order( $order_id ); 
foreach ($order->get_items() as $item_key => $item ):
    $item_id = $item->get_id();
 	$product_id   = $item->get_product_id();
endforeach; 
$home_page = wc_get_order_item_meta($item_id,'home_page',true);
$inner_page_count = wc_get_order_item_meta($item_id,'inner_page_count', true );
$inner_page_count = $inner_page_count == "" ? 0 : $inner_page_count;
$single_inner_page = wc_get_order_item_meta($item_id,'single_inner_page',true);
$inner_page = wc_get_order_item_meta($item_id,'inner_page',true); 
$show_in_portfolio_discount = wc_get_order_item_meta($item_id,'show_in_portfolio_discount',true);
$coupon_discount_amount = wc_get_order_item_meta( $item_id, 'discount_amount', true ); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
	<style>
		@charset "UTF-8";
		html {-webkit-text-size-adjust: none; /* Prevent font scaling in landscape */width: 100%;height: 100%;-webkit-font-smoothing: antialiased;-moz-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}
		input[type="submit"] {-webkit-appearance: none; -webkit-border-radius: 0; border-radius: 0;	}
		*, *:after, *:before {box-sizing: border-box; margin: 0; padding: 0;}
		body {margin: 0; padding: 0; font-family: 'Montserrat', sans-serif; -webkit-font-smoothing: antialiased; font-weight: 400; width: 100%; min-height: 100%; color: #000000; background: #fff; font-size: 16px; line-height: 22px;}
		a {outline: none; text-decoration: none; color: #000000;}
		a:hover, a:focus{outline: none; text-decoration: none;}
		strong{font-weight: 600;}
		input, textarea, select { outline: none; resize: none; font-family: 'Montserrat', sans-serif; }
		a, input, button{ outline:none !important; }
		button::-moz-focus-inner { border: 0; }
		h1, h2, h3, h4, h5, h6 { margin: 0; padding: 0; font-weight: normal; color: #000000; }
		img { border: 0; vertical-align: top; max-width: 100%; height: auto; }
		ul, ol { margin: 0; padding: 0; list-style: none; }
		p { margin: 0 0 15px 0; padding: 0; }
		.main-table{
			border-top: 6px solid #3C4C96;
			background: #f3f4f9;
			margin: 0 auto;
		}
		.order-table{
			overflow: hidden;
		}
		.order-table thead{

		}
		.order-table th{
			background: #e2e5f0;
			font-weight: 600;
			font-size: 14px;
			line-height: 18px;
			letter-spacing: 0.030em;
			color: #3c4c96;
			white-space: nowrap;
			text-transform: uppercase;
			border-bottom: 1px solid rgba(191, 202, 255, 0.6);
		}
		.order-table th, .order-table td{
			padding: 12px 15px;
			text-align: left;
		}
		.order-table td{
			border-bottom: 1px solid rgba(191, 202, 255, 0.6);
			border-top: 0;
			font-size: 14px;
			font-weight: 500;
			outline: none;
			color: #3c4c96;
			vertical-align: middle;
		}
		.order-table tbody tr:last-child td, .order-table tbody tr:last-child th{
			border-bottom: 0;
		}
</style>
<?php if ($order_id != ''){ ?>
	<script>
		window.onload = function(){
	    	window.print();
		}
	</script><?php 
} ?>

</head>

<body>
	<table cellpadding="0" cellspacing="0" class="main-table" width="1000">
		<tr>
			<td style="background: #e3eaff; padding: 30px">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td valign="center" width="30%">
							<img src="../vendors/images/pdf-logo.png" alt="" width="250" height="80">
						</td>
						<td valign="center" width="25%">
							<div style="color: #3C4C96; padding-bottom: 20px;">
								<strong style="display: block; padding-bottom: 3px; font-size: 14px;">ADDRESS</strong>
								10432 Balls Ford Rd<br> Suite # 300, Manassas,<br> VA 20109.
							</div>
							<div style="color: #3C4C96">
								<strong style="display: block; padding-bottom: 3px; font-size: 14px;">PHONE NUMBER</strong>
								<a href="tel:+310-606-2426" style="color: #3C4C96">+310-606-2426</a>
							</div>
						</td>
						<td valign="center" width="25%">
							<div style="color: #3C4C96; padding-bottom: 63px;">
								<strong style="display: block; padding-bottom: 3px; font-size: 14px;">EMAIL</strong>
								<a href="mailto:Info@pixelperfecthtml.com" style="color: #3C4C96">Info@pixelperfecthtml.com</a>
							</div>
							<div style="color: #3C4C96">
								<strong style="display: block; padding-bottom: 3px; font-size: 14px;">WEBSITE</strong>
								<a href="https://pixelperfecthtml.com/" style="color: #3C4C96">www.pixelperfecthtml.com</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="padding: 30px;">
				<table cellspacing="0" cellpadding="0" width="100%" style="background: #ffffff; padding: 30px 50px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">
					<tr>
						<td width="50%">
							<div style="color: #3C4C96; font-weight: 600; font-size: 35px; padding-bottom: 30px;">Invoice</div>
							<div style="color: #3C4C96">
								<p style="margin-bottom: 5px;">Date: <?php echo $order->get_date_created()->format ('F j, Y'); ?></p>
								<p style="margin-bottom: 5px;">Invoice No: #<?php echo $order_id; ?></p>
								<p style="margin-bottom: 0;">Order Id: #<?php echo $order_id; ?></p>
							</div>
						</td>
						<td width="50%">
							<!-- <div style="color: #3C4C96; font-weight: 600; font-size: 20px; padding-bottom: 10px;">Bill To</div> -->
							<div style="color: #3C4C96; padding-top: 53px;">

								<p style="margin-bottom: 5px;">
									<strong style="margin-right: 5px;">Name:</strong> 
									<?php echo $order->get_billing_first_name(); ?>
								</p>

								<p style="margin-bottom: 5px;">
									<strong style="margin-right: 5px;">Email:</strong> 
									<a href="mailto:<?php echo $order->get_billing_email(); ?>" style="color: #3C4C96"><?php echo $order->get_billing_email(); ?>
									</a>
								</p>

								<p style="margin-bottom: 5px;">
									<strong style="margin-right: 5px;">Phone:</strong> 
									<a href="tel:<?php echo $order->get_billing_phone(); ?>" style="color: #3C4C96">
										<?php echo $order->get_billing_phone(); ?>
									</a>
								</p>

							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="padding: 0px 30px 0px;">
				<div style="background: #e2e5f0; text-align: center; padding: 15px 15px; font-weight: 600; font-size: 18px; line-height: 18px; letter-spacing: 0.030em; color: #3c4c96; white-space: nowrap; text-transform: uppercase; -webkit-border-radius: 5px 5px 0px 0px; -moz-border-radius: 5px 5px 0px 0px; border-radius: 5px 5px 0px 0px; border-bottom: 1px solid rgba(191, 202, 255, 0.6);"><?php echo get_the_title($product_id); ?></div>
			</td>
		</tr>
		<tr>
			<td style="padding: 0px 30px 30px;">
				<table cellspacing="0" cellpadding="0" width="100%" style="background: #ffffff; -webkit-border-radius: 0px 0px 5px 5px; -moz-border-radius: 0px 0px 5px 5px; border-radius: 0px 0px 5px 5px;" class="order-table">
					<thead>
						<tr>
							<th>NO</th>
							<th>PAGE</th>
							<th>RATE</th>
							<th>AMOUNT</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Home Page</td>
							<td>$<?php echo $home_page; ?></td>
							<td>$<?php echo $home_page; ?></td>
						</tr><?php

						if ( $inner_page_count != 0 ): ?>

							<tr>
								<td>2</td>
								<td>Inner Page</td>
								<td><?php echo $inner_page_count.' x $'. $single_inner_page; ?></td>
								<td>$<?php echo $inner_page; ?></td>
							</tr><?php 

						endif; ?>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td style="padding: 0px 30px 30px;" align="right">
				<table cellspacing="0" cellpadding="0" width="40%" style="background: #ffffff; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;" class="order-table">
					<tbody>
						<tr>
							<th width="50%">SUB TOTAL</th>
							<td width="50%"><strong>$<?php echo ($home_page + $inner_page); ?></strong></td>
						</tr>
						<tr>
							<th width="50%">DISCOUNT</th>
							<td width="50%"><strong>$<?php echo ( $show_in_portfolio_discount + $coupon_discount_amount); ?></strong></td>
						</tr>
						<tr>
							<th width="50%">GRAND TOTAL</th>
							<td width="50%"><strong>$<?php echo get_post_meta($order_id, 'total_order_amount', true );?></strong></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td style="padding: 0px 30px 30px;">
				<table cellspacing="0" cellpadding="0" width="100%" style="background: #ffffff; overflow: hidden; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">
					<tr>
						<td style="background: #e2e5f0; padding: 15px; font-weight: 600; font-size: 16px; line-height: 18px; letter-spacing: 0.030em; color: #3c4c96; white-space: nowrap; text-transform: uppercase;">BANK INFORMATION</td>
					</tr>
					<tr>
						<td style="padding: 15px;">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td width="50%;">
										<div style="color: #3C4C96; padding-bottom: 20px;">
											<strong style="display: block; padding-bottom: 3px; font-size: 14px;">BANK NAME</strong>
											Silicon Valley Bank
										</div>
									</td>
									<td width="50%;">
										<div style="color: #3C4C96; padding-bottom: 20px;">
											<strong style="display: block; padding-bottom: 3px; font-size: 14px;">ACCOUNT NUMBER</strong>
											3302491240
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="padding: 15px;">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td width="50%;">
										<div style="color: #3C4C96; padding-bottom: 20px;">
											<strong style="display: block; padding-bottom: 3px; font-size: 14px;">DIRECT DEPOSIT ACH R/T NUMBER</strong>
											121140399
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="padding: 30px 30px 30px;" align="right">
				<table cellspacing="0" cellpadding="0" width="300">
					<tr>
						<td style="text-align: center;">
							<img src="<?php echo get_template_directory_uri() ?>/vendors/images/sign.png" alt="" width="100">
						</td>
					</tr>
					<tr>
						<td style="text-align: center; font-size: 20px; color: #444; font-weight: 500; padding-top: 5px;">
							Account Manager
						</td>
					</tr>
					<tr>
						<td style="text-align: center; font-size: 16px; color: #3c4c96; font-weight: 600; padding-top: 5px">
							Maria Smith
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="background: #e3eaff; padding: 30px">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td style="color: #3C4C96; text-transform: uppercase; font-size: 20px; font-weight: 700;">
						THANK YOU FOR YOUR BUSINESS
						</td>
					</tr>
					<!-- <tr>
						<td style="font-size: 12px; color: #000000;">
							<strong style="color: #3C4C96; font-size: 14px;">TERMS :</strong>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</td>
					</tr> -->
				</table>
			</td>
		</tr>
	</table>
</body>
</html>


