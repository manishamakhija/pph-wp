		<div class="dash-topbar">
			<div class="left">
				<div class="dash-logo">
					<div class="icon"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/lego-icon.svg" alt="logo"></div>
				</div>
				<div class="dash-sidebar-toggle">
					<a href="javascript:;"><i class="fa fa-bars"></i></a>
				</div>
			</div>
			<div class="right">
				<ul>
					<li>
						<div class="cta-btn">
							<a href="<?php echo PPH_SITE_URL; ?>" class="cta-link cta-style2"><span>Back To Website</span></a>
						</div>
					</li>
					<!-- <li class="notification-dropdown dropdown">
						<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
							<span>
								<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/chat-icon2.svg" alt="" class="svg">
								<span class="badge">99</span>
							</span>
						</a>
						<div class="dropdown-menu">
							<ul class="list-group list-group-flush">
								<li class="list-group-item">
									<a href="#">
										<strong>eDoc24x7</strong> 35 min ago
										<p>Lorem ipsum text consector consector consector</p>
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<strong>eDoc24x7</strong> 35 min ago
										<p>Lorem ipsum text consector consector consector</p>
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<strong>eDoc24x7</strong> 35 min ago
										<p>Lorem ipsum text consector consector consector</p>
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<strong>eDoc24x7</strong> 35 min ago
										<p>Lorem ipsum text consector consector consector</p>
									</a>
								</li>
								<li class="list-group-item logout">
									<a href="#">
										<strong>Read All Messages</strong>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="notification-dropdown dropdown">
						<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
							<span>
								<img src="../vendors/images/notification-icon.svg" alt="" class="svg">
								<span class="badge">10</span>
							</span>
						</a>
						<div class="dropdown-menu">
							<ul class="list-group list-group-flush">
								<li class="list-group-item">
									<a href="#">
										<strong>Application error</strong> 35 min ago
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<strong>Sketch to Woocomerce</strong> 35 min ago
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<strong>Sketch to Woocomerce</strong> 35 min ago
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<strong>Sketch to Woocomerce</strong> 35 min ago
									</a>
								</li>
								<li class="list-group-item logout">
									<a href="#">
										<strong>Read All Notification</strong>
									</a>
								</li>
							</ul>
						</div>
					</li> -->
					<li class="user-dropdown dropdown">
						<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
							<img src="<?php echo TEMPLATE_DIR ?>/vendors/images/user-icon.svg" alt="" class="svg">
						</a>
						<div class="dropdown-menu">
							<a href="<?php echo get_permalink(249436); ?>">Edit Profile</a>
							<!-- <a href="#">My Inbox</a> -->
							<a href="<?php echo get_permalink(249447); ?>">Change Password</a>
							<!-- <a href="#">Chats</a> -->
							<a href="<?php echo wp_logout_url( PPH_SITE_URL ) ?>" class="logout">Logout</a>
						</div>
					</li>
				</ul>
			</div>
		</div>