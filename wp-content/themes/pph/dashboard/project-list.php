<?php 
/*
** Template Name: Dashboard Project List
*/

if (!is_user_logged_in()){
	wp_redirect(get_permalink(24)); // login page
	exit;
}
get_template_part('dashboard/header'); ?>

<?php if (isset($_SESSION['login_success']) && time() - $_SESSION['login_success'] < 15){ ?>
		<div class="pph-login-success-msg alert alert-success">Success! You are successfully logged in.</div>	
<?php }else{
		unset($_SESSION['login_success']);
} ?>

	<div class="dash-wrap-header">
		<div class="d-flex justify-content-between align-items-center">
			<h1><?php the_title(); ?></h1>
			<div class="cta-btn">
				<a href="<?php echo get_permalink(36); ?>" class="cta-link cta-xs"><span>ORDER NOW</span></a>
			</div>
		</div>
	</div>

	<div class="project-filter">
		<label>Status</label>
		<select id="dropdown1" class="selectpicker">
			<option value="">All</option>
			<option value="Processing" data-content="<span class='text-warning'>Processing</span>">Processing</option>
			<option value="Project Slicing & Coding" data-content="<span class='text-warning'>Project Slicing & Coding</span>">Project Slicing & Coding</option>
			<option value="Quality Assurance" data-content="<span class='text-warning'>Quality Assurance</span>">Quality Assurance</option>
			<option value="Cancelled" data-content="<span class='text-danger'>Cancelled</span>">Cancelled</option>
			<option value="Completed" data-content="<span class='text-success'>Completed</span>">Completed</option>
		</select>
	</div>

	<div class="project-table">
		<table id="my-order-list" class="table" cellspacing="0">
			<thead>
				<tr>
					<th>ORDER ID</th>
					<th>PROJECT NAME</th>
					<th>IMPLEMENT TO</th>
					<th>ORDER DATE</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody><?php 
				$order_statuses = array('wc-on-hold', 'wc-processing', 'wc-completed','wc-order-development','wc-order-qa','wc-cancelled');
				$customer_user_id = get_current_user_id(); // current user ID

				// Getting current customer orders
				$customer_orders = wc_get_orders( array(
				    'meta_key' => '_customer_user',
				    'meta_value' => $customer_user_id,
				    'post_status' => $order_statuses,
				    'numberposts' => -1
				) ); 
				foreach($customer_orders as $order ){

			    	$order_id = method_exists( $order, 'get_id' ) ? $order->get_id() : $order->id; 
			    	$project_name = get_post_meta( $order_id, 'project_name' ,true ); 
			    	foreach ($order->get_items() as $item_key => $item ): 
			    		$item_name    = $item->get_name(); 
			    	endforeach; ?>
					<tr>
						<td class="p-order-id"><a href="<?php echo $order->get_view_order_url(); ?>">#<?php echo $order_id; ?></a></td>
						<td><?php echo $project_name; ?></td>
						<td><?php echo $item_name; ?></td>
						<td><?php echo $order->get_date_created()->format ('j F, Y'); ?></td>
						<?php $current_order_status = $order->get_status(); 
						switch($current_order_status){
							case "completed": $status_color = 'success'; break;
							case "cancelled": $status_color = 'danger'; break;
							default: $status_color = 'warning'; break;
						} ?>
						<td><span class='text-<?php echo $status_color; ?>'><?php echo wc_get_order_status_name($order->get_status()); ?></span></td>
					</tr><?php 
				} ?>
			</tbody>
		</table>
	</div>
	<script>
		jQuery(document).ready(function($) {
			var table =  $('#my-order-list').DataTable({
				responsive: true,
				autoWidth: false,
				dom: 'rftip',
				pageLength: 10,
				columnDefs: [{
					targets: "no-sort",
					orderable: false,
				}],
				"language": {
					search: "",
					searchPlaceholder: "Search Here..."
				},
			});
			$('#dropdown1').on('change', function () {
				table.columns(4).search( this.value ).draw();
			} );
		});
	</script>

<?php get_template_part('dashboard/footer'); 