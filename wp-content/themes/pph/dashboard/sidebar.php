<div class="dash-sidebar">
	<div class="dash-sidebar-toggle">
		<a href="javascript:;"><i class="fa fa-times"></i></a>
	</div>
	<div class="dash-logo">
		<div class="icon"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/lego-icon.svg" alt="logo"></div>
		<div class="brand-name">PIXEL PERFECT HTML</div>
	</div>
	<div class="dash-menu">
		<ul>
			<li <?php echo (is_page(249230) || is_wc_endpoint_url( 'view-order' )  ? 'class="active"' : ''); ?>>
				<a href="<?php echo get_permalink(249230); ?>">
					<div class="icon"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/project.svg" alt="" class="svg"></div> Projects
				</a>
			</li>
			<!-- <li>
				<a href="maintenance.php">
					<div class="icon"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/maintenance-icon.svg" alt="" class="svg"></div> Maintenance
				</a>
			</li> -->
			<li>
				<a href="<?php echo get_permalink(36); ?>">
					<div class="icon"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/place-order-icon.svg" alt="" class="svg"></div> Place Order
				</a>
			</li>
			<li>
				<a href="<?php echo get_permalink(37); ?>">
					<div class="icon"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/quick-quote-icon.svg" alt="" class="svg"></div> Quick Quote
				</a>
			</li>
			<li <?php echo (is_page(249387) ? 'class="active"' : ''); ?>>
				<a href="<?php echo get_permalink(249387); ?>">
					<div class="icon"><img src="<?php echo TEMPLATE_DIR ?>/vendors/images/leave-a-testimonial-icon.svg" alt="" class="svg"></div> Leave a Testimonial
				</a>
			</li>
			<!-- <li>
				<a href="#">
					<div class="icon"><img src="../vendors/images/chat-icon.svg" alt="" class="svg"></div> Chat
				</a>
			</li> -->
		</ul>
		<div class="cta-btn">
			<a href="<?php echo PPH_SITE_URL; ?>" class="cta-link cta-xs"><span>Back To Website</span></a>
		</div>
	</div>
</div>