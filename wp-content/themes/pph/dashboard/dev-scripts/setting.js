
function clearEditorText(){
	jQuery('#froala-editor').trumbowyg('empty');
}

// client send message
jQuery(document).on('click','input[name="chat_send"]', function(){
	var userMsg = jQuery('.user-msg-to-pm').val();
	if (userMsg == ''){

		jQuery('.send-msg-to-pm .alert-msg').addClass('alert alert-danger').text('Type Something...');
		setTimeout(function(){
			jQuery('.send-msg-to-pm .alert-msg').removeClass('alert alert-danger').text('');
		},3000);

	}else{
		var senddata = {
			action: "user_send_note_to_pm",
			message: userMsg,
			orderId: jQuery('input[name="current_order_id"]').val()
		}

		jQuery.post(obj_dash.ajax_url, senddata,
	        function ( response ) {
		   		var latestMsg = '<li class="client"><div class="manager-name-date"><span class="m-name">'+ response.author_name +'</span><span class="date">'+ response.date +'</span></div><div class="text">'+ userMsg +'</div></li>';
		   		jQuery('ul.chat-discussion').append(latestMsg);
		   		clearEditorText();
			}
		)
				
	}
})

// client send feedback
jQuery(document).on('click','.btn-cfb', function(e){
	e.preventDefault();
	var clientFeedback = jQuery('.client-feedback-input').val();
	if (clientFeedback == ''){

		jQuery('.alert-msg-fb').addClass('alert alert-danger').html('Please share your feedback...');

	}else{

		var feedbackData = {
			action: "pph_save_client_feedback",
			feedBack: clientFeedback,
			orderId: jQuery('input[name="current_order_id"]').val()
		}

		jQuery.post( obj_dash.ajax_url, feedbackData,
			function (response){
				// console.log(response.result);
				if (response.result == 1){
					jQuery('.dash-feedback').addClass('alert alert-success mt-3 mr-3 ml-3').html('Thank you for your feedback.');
				}
			}
		)
	}
})


// // change password
// jQuery(document).on('click','.password-update-btn',function(){

// })

jQuery(document).ready(function(){
	// jQuery('#change-password').validate();
	// hide login success message in projects list page
	setTimeout(function(){
		jQuery('.pph-login-success-msg').hide();
	},7000);

	jQuery("#change-password").validate({
		errorElement: "span",
		errorClass: "wpcf7-not-valid-tip",
		rules : {
				old_password: {
					required: true
				},
                new_password : {
                	required: true,
                    minlength : 5
                },
                confirm_password : {
                    equalTo : "#new_password"
                }
        },
		submitHandler:function() {
			var pass_details = {
				action: "pph_reset_password",
				passData: jQuery('#change-password').serialize()
			}

			jQuery.post( obj_dash.ajax_url, pass_details,
				function (response){
					jQuery('.reset-password-msg').html(response.message);
					if (response.result == 1){
						jQuery('.reset-password-msg').addClass('alert alert-success');
						jQuery("#change-password").trigger("reset");

					}else{
						jQuery('.reset-password-msg').addClass('alert alert-danger').removeClass('alert-success');
					}

					setTimeout(function(){
						jQuery('.reset-password-msg').removeClass('alert alert-success alert-danger').text('');
					},10000);
					
				}
			)
			
		}
	});
});


document.addEventListener( 'wpcf7mailsent', function( event ) {

	if ( '249389' == event.detail.contactFormId ) {	 // testimonial form

    	jQuery('.screen-reader-response').addClass('alert alert-success');

	}

	setTimeout(function(){

		jQuery('.screen-reader-response').removeClass('alert alert-success');
		
	},10000);

}, false );