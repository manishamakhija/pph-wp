<?php 
$file_path = get_template_directory_uri().'/email-templates/images';
ob_start();
?>
<html>
<body>
	<table cellpadding="0" cellspacing="0" width="600" border="0" align="center">
		<tr>
			<!-- <td align="center" style="background-color: #f6f5fb; background-image: url('images/bg-dots.png');"> -->
			<td align="center" style="background-color: #f6f5fb;">
				<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border:0">
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px;"><a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php echo $file_path; ?>/logo.png" alt="logo" width="229" height="71" style="display: block; border:none;"></a></td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 0 40px 30px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: left 8px bottom -3px;">
							<table width="100%" width="600"  cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
								<tr>
									<td align="center" valign="top" style="padding: 30px; background-color: #fff; border: 1px solid #EDEDED; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
										<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 700; text-align: left; padding-bottom: 13px;">Hey <?php echo $name; ?></td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;">Greetings from Pixel Perfect HTML!</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;">We have received your quote request for <?php echo $_POST['service']; ?>. We glad to hear from you. Our operations team is analyzing the design assets you have provided. We will get back to you with the conversion quotation with in next 24 hours. </td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 20px;">If your requirement is super urgent you can call us on <a href="tel:3106062426" style="text-decoration: none; color: #626262; font-weight: 700;">+310-606-2426</a> or get in touch with us through Skype handler: <a href="skype:PixelPerfectHTML?chat" style="text-decoration: none; color: #626262; font-weight: 700;" target="_blank">‘PixelPerfectHTML’</a>.</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 17px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 10px;">Thanks for getting in touch.</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 17px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 20px;">Cheers!</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left;">Peter Collins</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 700; text-align: left;">Pixel Perfect HTML</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px 0px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: right 10px top;">
							<?php echo do_shortcode('[pph-mail-latest-posts]'); ?>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px;">
							<?php echo do_shortcode('[pph-mail-social-network]'); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
</body>
</html>
<?php
$to = $email;
$subject = 'Thanks for contacting Pixel Perfect HTML';
$user_mail_body = ob_get_clean();
$headers[] = 'From: PixelPerfectHTML <info@pixelperfecthtml.com>';
$headers[] = 'Content-Type: text/html';
$headers[] = 'charset=UTF-8';

$res = wp_mail( $to, $subject, $user_mail_body, $headers );
ob_start();
?>
<html>
<body style="margin: 0; padding: 0; background-color: #f6f5fb;">
    <table cellpadding="0" cellspacing="0" width="600" border="0" align="center">
        <tr>
            <!-- <td align="center" style="background-color: #f6f5fb; background-image: url('images/bg-dots.png');"> -->
            <td align="center" style="background-color: #f6f5fb;">
                <table width="100%" cellpadding="0" cellspacing="0" width="600" align="center" border="0" style="border: 0">
                    <tr>
                        <td align="center" valign="top" style="padding: 30px 40px;"><a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php echo $file_path; ?>/logo.png" alt="logo" width="229" height="71" style="display: block; border:none;"></a></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding: 0 40px 30px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: left 8px bottom -3px;">
                            <table width="100%" width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
                                <tr>
                                    <td align="center" valign="top" style="padding: 30px; background-color: #fff; border: 1px solid #EDEDED; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
                                        <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
                                            <tr>
                                                <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 16px; line-height: 19px; color: #273270; font-weight: 700; text-align: left; padding-bottom: 20px;">Congratulations</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 30px;">You have a new Quote Inquiry from your website.</td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="border-radius: 5px; border:1px solid #EDEDED;">
                                                    <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border-collapse: collapse;">
                                                    	<tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED; border-top-left-radius: 5px;">Service</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED; border-top-right-radius: 5px; text-transform: uppercase"><?php echo $_POST['service']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED; border-top-left-radius: 5px;">Name</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED; border-top-right-radius: 5px;"><?php echo $name; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Email Address</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Files attached</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $link; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Message</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $message; ?> </td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Client IP Address</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo get_client_ip(); ?> </td>
                                                        </tr>
                                                      
                                                    </table>
                                                    <tr>
                                                     <td align="center" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: center; padding-top: 30px;">This inquiry came from <?php echo $_POST['form_type']; ?>.</td>
                                                    </tr>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            </td>
                    </tr>
              
                </table>
            </td>
        </tr>

    </table>
</body>
</html>
<?php
// $to_admin = 'info@pixelperfecthtml.com';
$to_admin = 'natashaherbert21@gmail.com';
$admin_subject = 'New Quote Request from '.$_POST['name'].' for '.$_POST['service'];
$admin_body = ob_get_clean();
$admin_headers[] = 'From: '.$name.' <'.$email.'>';
$admin_headers[] = 'Content-Type: text/html';
$admin_headers[] = 'charset=UTF-8';
// $admin_headers[] = 'Cc: test@pph.com';
// $admin_headers[] = 'Cc: t2@pph.com';

$admin_mail = wp_mail( $to_admin, $admin_subject, $admin_body, $admin_headers );
// if ($admin_mail && $res):
// 	$url = get_site_url().'/thank-you-quote';
	
// else:
// 	$url = get_site_url();

// endif;
// wp_redirect($url);