<?php 
$file_path = get_template_directory_uri().'/email-templates/images';
if ( !empty( $filenames ) ){
	foreach($filenames as $key => $value){
	    $filepath = get_template_directory_uri().'/filepond/uploads/'.$value['new_name'];
	    $link .= '<a href = "'. $filepath .'" download target="_blank">'.$value['original'].'</a>'."<br>";
	}
}
// function get_estimate_delivery_date($no_of_business_days,$order_created_date){
// 	$non_business_days = array('6','7');
// 	$i = 0;
// 	// $order_created_date = new DateTime('24 May, 2019');
// 	$date = new DateTime($order_created_date);
//     while ($i < $no_of_business_days) {
//         $date->modify("+1 day");
//      	if (!in_array( (int)$date->format('N'), $non_business_days) ) {
//             $i++;
//         }
//     }
//     return $date->format('j F, Y');

// }
ob_start();
?>
<html>
<body>
	<table cellpadding="0" cellspacing="0" width="600" border="0" align="center">
		<tr>
			<td align="center" style="background-color: #f6f5fb;">
				<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0">
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px;"><a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php echo $file_path; ?>/logo.png" alt="logo" width="229" height="71" style="display: block; border:none;"></a></td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 0 40px 30px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: left 8px bottom -3px;">
							<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
								<tr>
									<td align="center" valign="top" style="padding: 30px; background-color: #fff; border: 1px solid #EDEDED; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
										<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 16px; line-height: 19px; color: #273270; font-weight: 700; text-align: left; padding-bottom: 20px;">Congratulations <?php echo $name; ?>,</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 700; text-align: left; padding-bottom: 13px;">We have received your <?php echo ucwords( strtolower (get_the_title($product_id) ) ); ?> conversion order</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 30px;">Following is the order details </td>
											</tr>
											<tr>
												<td align="center" valign="top" style="border-radius: 5px; border:1px solid #EDEDED;">
													<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border-collapse: collapse;">

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED; border-top-left-radius: 5px;">Order ID</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED; border-top-right-radius: 5px;"><?php echo $order_id; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Service</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo ucwords( strtolower ( get_the_title($product_id ) ) ); ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Project</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $project_name; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Package selected</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $package_name; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Home page</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;">1 ( $<?php echo $home_page; ?>) </td>
														</tr><?php 

														if ($inner_page_count > 0){ ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">No. of inner pages</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php printf( '%s (%s*%s) $%s', $inner_page_count, $inner_page_count, $single_inner_page, $inner_page ); ?></td>
															</tr><?php 
														} 

														if ($project_note != ''): ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Comment</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $project_note; ?></td>
															</tr><?php 
														endif; ?>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Delivery type</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $delivery; ?></td>
														</tr><?php 

														if ( $show_in_portfolio_discount != '' ){ ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Portfolio inclusion discount</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;">$<?php echo $show_in_portfolio_discount; ?></td>
															</tr><?php 
														} 

														if ( $coupon_discount_amount != 0 ): ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Offer discount</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;">$<?php echo $coupon_discount_amount; ?></td>
															</tr><?php 
														endif; ?>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Total order value</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;">$<?php echo $total_cost; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Order date</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $order_created_date; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Expected Delivery Date</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $project_deadline; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Payment mode</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $payment_method; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Transaction ID</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $transaction_id; ?></td>
														</tr>

														<?php 

														if ( !empty( $filenames ) ): ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom-left-radius: 5px;">Design included</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom-right-radius: 5px;"><?php echo $link; ?></td>
															</tr> <?php 
														endif; ?>
													</table>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 20px;padding-top: 20px;">You can connect with our support officer at <a href="mailto:support@pixelperfecthtml.com" style="text-decoration: none; color: #626262; font-weight: 700;">support@pixelperfecthtml.com</a> or connect with us through Skype handler: <a href="skype:pixelperfecthtml?chat" style="text-decoration: none; color: #626262; font-weight: 700;" target="_blank">‘pixelperfecthtml’</a>.</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 17px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 10px;">Thanks for getting in touch.</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 17px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 20px;">Cheers!</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left;">Peter Collins</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 700; text-align: left;">Pixel Perfect HTML</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							</td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px;">
							<?php echo do_shortcode('[pph-mail-social-network]'); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>

<?php
$to = $email;
$subject = 'Thanks for placing order with us';
$user_mail_body = ob_get_clean();
$headers[] = 'From: Peter Collins <order@pixelperfecthtml.com>';
// $headers[] = 'Reply-To: Peter Collins <order@pixelperfecthtml.com>';
$headers[] = 'Content-Type: text/html';
$headers[] = 'charset=UTF-8';

$res = wp_mail( $to, $subject, $user_mail_body, $headers );
ob_start();
?>
<html>
<body>
	<table cellpadding="0" cellspacing="0" width="600" border="0" align="center">
		<tr>
            <td align="center" style="background-color: #f6f5fb;">
                <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0">
                    <tr>
                        <td align="center" valign="top" style="padding: 30px 40px;"><a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php echo $file_path; ?>/logo.png" alt="logo" width="229" height="71" style="display: block; border:none;"></a></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding: 0 40px 30px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: left 8px bottom -3px;">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
                                <tr>
                                    <td align="center" valign="top" style="padding: 30px; background-color: #fff; border: 1px solid #EDEDED; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
                                        <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
                                            <tr>
                                                <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 16px; line-height: 19px; color: #273270; font-weight: 700; text-align: left; padding-bottom: 20px;">Congratulations</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 30px;">A new order has been placed from PixelPerfectHtml </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="border-radius: 5px; border:1px solid #EDEDED;">
                                                    <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border-collapse: collapse;">
                                                    	<tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED; border-top-left-radius: 5px;">Service</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED; border-top-right-radius: 5px;"><?php echo ucwords( strtolower ( get_the_title($product_id ) ) ); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED; border-top-left-radius: 5px;">Name</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED; border-top-right-radius: 5px;"><?php echo $name; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Email address</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Phone</td>
                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></td>
                                                        </tr><?php 
                                                        if ($skype != ''): ?>
	                                                        <tr>
	                                                            <td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Skype name</td>
	                                                            <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><a href="skype:<?php echo $skype; ?>?chat"><?php echo $skype; ?></a></td>
	                                                        </tr><?php 
	                                                    endif;  ?>
                                                        <tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED; border-top-left-radius: 5px;">Order ID</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED; border-top-right-radius: 5px;"><?php echo $order_id; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Project</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $project_name; ?></td>
														</tr>
														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Package selected</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $package_name; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Home page</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;">1 ( $<?php echo $home_page; ?>) </td>
														</tr><?php 

														if ($inner_page_count > 0){ ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">No. of inner pages</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php printf( '%s (%s*%s) $%s', $inner_page_count, $inner_page_count, $single_inner_page, $inner_page ); ?></td>
															</tr><?php 
														} 

														if ($project_note != ''): ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Comment</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $project_note; ?></td>
															</tr><?php 
														endif; ?>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Delivery type</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $delivery; ?></td>
														</tr><?php 

														if ( $show_in_portfolio_discount != '' ){ ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Portfolio inclusion discount</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;">$<?php echo $show_in_portfolio_discount; ?></td>
															</tr><?php 
														} 

														if ( $coupon_discount_amount != 0 ): ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Offer discount</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;">$<?php echo $coupon_discount_amount; ?></td>
															</tr><?php 
														endif; ?>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Total order value</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;">$<?php echo $total_cost; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Order date</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $order_created_date; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Expected Delivery Date</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $project_deadline; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Payment mode</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $payment_method; ?></td>
														</tr>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED;">Transaction ID</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED;"><?php echo $transaction_id; ?></td>
														</tr>

														<?php 

														if ( !empty( $filenames ) ): ?>
															<tr>
																<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom-left-radius: 5px;">Design included</td>
																<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom-right-radius: 5px;"><?php echo $link; ?></td>
															</tr> <?php 
														endif; ?>

														<tr>
															<td align="left" valign="top" width="160" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom-left-radius: 5px;">Client IP address</td>
															<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom-right-radius: 5px;"><?php echo get_client_ip(); ?></td>
														</tr>
                                                      
                                                    </table>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            </td>
                    </tr>
              </table>
            </td>
        </tr>
    </table>
</body>
</html>
<?php
// $to_admin = 'info@pixelperfecthtml.com,peter@pixelperfecthtml.com';
$to_admin = 'natashaherbert21@gmail.com';
$admin_subject = 'New Order from PixelPerfectHTML for '. get_the_title($product_id);
$admin_body = ob_get_clean();
$admin_headers[] = 'From: '.$name.' <'.$email.'>';
$admin_headers[] = 'Content-Type: text/html';
$admin_headers[] = 'charset=UTF-8';
// $admin_headers[] = 'Cc: test@pph.com';
// $admin_headers[] = 'Cc: t2@pph.com';

$admin_mail = wp_mail( $to_admin, $admin_subject, $admin_body, $admin_headers );
// if ($admin_mail && $res):
	
// 	$msg = array( "result" => 1, "message" => "done" );
// else:
// 	$msg = array( "result" => 0, "message" => "Something went wrong!!!" );

// endif;
// echo json_encode( $msg, true );
// wp_die();
