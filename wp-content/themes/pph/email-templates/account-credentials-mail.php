<?php 
$file_path = get_template_directory_uri().'/email-templates/images';
ob_start();
?>
<html>
<body>
	<table cellpadding="0" cellspacing="0" width="600" border="0" align="center">
		<tr>
			<td align="center" style="background-color: #f6f5fb;">
				<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0">
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px;"><a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php echo $file_path; ?>/logo.png" alt="logo" width="229" height="71" style="display: block; border:none;"></a></td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 0 40px 30px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: left 8px bottom -3px;">
							<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
								<tr>
									<td align="center" valign="top" style="padding: 30px; background-color: #fff; border: 1px solid #EDEDED; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
										<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 700; text-align: left; padding-bottom: 13px;">Hey <?php echo $name; ?></td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 19px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;">Greetings from Pixel Perfect HTML!</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left; padding-bottom: 20px;">We have received your quote request for <?php echo get_the_title($product_id); ?>. We glad to hear from you. Our operations team is analyzing the design assets you have provided. We will get back to you with the conversion quotation with in next 24 hours. </td>
											</tr>
											<tr>
												<td style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #313131; text-align:center; font-weight: 700;  padding-bottom: 10px; padding-top: 20px;">
													Username: <?php echo $email; ?> <br>
													Password: <?php echo $default_password; ?>
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #313131; font-weight: 700; text-align: center; padding-bottom: 20px; padding-top: 40px;">You can login to your account to connect with your project coordinator.</td>
											</tr>
											<tr>
												<td align="center" valign="top" style="padding-bottom: 30px;">
													<table cellpadding="0" cellspacing="0" align="center" border="0" style="border: 0;">
														<tr>
															<td align="center" valign="middle" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 18px; color: #ffffff; font-weight: 700; text-transform: uppercase; background-color: #EA5351; border-radius: 2px;"><a href="<?php echo get_permalink(24); ?>" style="color: #ffffff; text-decoration: none; display: inline-block; padding: 14px 24px;" target="_blank">SIGN IN TO DASHBOARD</a></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 20px;">If your requirement is super urgent you can call us on <a href="tel:3106062426" style="text-decoration: none; color: #626262; font-weight: 700;">+310-606-2426</a> or get in touch with us through Skype handler: <a href="skype:PixelPerfectHTML?chat" style="text-decoration: none; color: #626262; font-weight: 700;" target="_blank">‘PixelPerfectHTML’</a>.</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 17px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 10px;">Thanks for getting in touch.</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 17px; color: #626262; font-weight: 400; text-align: left; padding-bottom: 20px;">Cheers!</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 400; text-align: left;">Peter Collins</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; line-height: 24px; color: #0F101F; font-weight: 700; text-align: left;">Pixel Perfect HTML</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px 0px; background-image: url('<?php echo $file_path; ?>/bg-polygon.png'); background-repeat: no-repeat; background-position: right 10px top;">
							<?php echo do_shortcode('[pph-mail-latest-posts]'); ?>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" style="padding: 30px 40px;">
							<?php echo do_shortcode('[pph-mail-social-network]'); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
</body>
</html>
<?php
$to = $email;
$subject = 'Thanks for contacting Pixel Perfect HTML';
$user_mail_body = ob_get_clean();
$headers[] = 'From: PixelPerfectHTML <info@pixelperfecthtml.com>';
$headers[] = 'Content-Type: text/html';
$headers[] = 'charset=UTF-8';

$res = wp_mail( $to, $subject, $user_mail_body, $headers );