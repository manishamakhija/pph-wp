var autoprefixer, browserSync, concat, config, gulp, imagemin, minify, path, plumber, rename, sass, streamqueue, uglify, changed, reload;

gulp = require('gulp');
sass = require('gulp-sass');
plumber = require('gulp-plumber');
rename = require('gulp-rename');
autoprefixer = require('gulp-autoprefixer');
concat = require('gulp-concat');
uglify = require('gulp-uglify');
imagemin = require('gulp-imagemin');
minify = require('gulp-clean-css');
streamqueue = require('streamqueue');
browserSync = require('browser-sync').create();
changed = require('gulp-changed');
reload = browserSync.reload;
config = {
	nodeDir: './node_modules/'
};

var path = {
	styles: [
		config.nodeDir + 'bootstrap/dist/css/bootstrap.min.css', // bootstrap css
		config.nodeDir + 'slick-carousel/slick/slick.css', // slick css
		//'src/styles/fullpage.css', // full page services css
		config.nodeDir + 'filepond/dist/filepond.css', // file upload css
		'src/styles/tt-bluescreens.css',
		'src/styles/**/style.scss',
		'src/dev-css/dev.css'
	],
	scripts: [
		// config.nodeDir + 'jquery/dist/jquery.js',
		'src/scripts/browser-polyfill.min.js', // browser-polyfill js
		'src/scripts/filepond-polyfill.js', // filepond-polyfill js
		'src/scripts/popper.min.js', // popper js
		'src/scripts/TweenMax.min.js', // TweenMax js
		config.nodeDir + 'bootstrap/dist/js/bootstrap.min.js', // bootstrap js
		config.nodeDir + 'slick-carousel/slick/slick.js', // slick js
		//'src/scripts/fullpage.min.js', // full page services js
		config.nodeDir + 'filepond/dist/filepond.min.js', // file upload js
		config.nodeDir + 'filepond-plugin-file-encode/dist/filepond-plugin-file-encode.min.js', // file upload js
		config.nodeDir + 'jquery-filepond/filepond.jquery.js', // file upload js
		config.nodeDir + 'filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.min.js', // file upload js
		'src/scripts/setting.js', // all setting js
		'src/dev-js/filepond.js', // filepond register js
		'src/dev-js/dev.js'
	],
	fonts: [
		'src/fonts/**/*.*',
	],
	images: 'src/images/**/*.*',
	php: ['*.php'],
	dashboard_styles: [
		'src/styles/datatable/jquery.dataTables.css',
		'src/styles/datatable/dataTables.bootstrap4.css',
		'src/styles/datatable/responsive.bootstrap4.css',
		config.nodeDir + 'bootstrap-select/dist/css/bootstrap-select.min.css',
		config.nodeDir + 'croppie/croppie.css',
		'src/styles/**/dashboard-style.scss',
		'dashboard/dev-styles/style.css'
	],
	dashboard_scripts: [
		'src/scripts/datatable/jquery.dataTables.min.js',
		'src/scripts/datatable/dataTables.bootstrap4.min.js',
		'src/scripts/datatable/dataTables.responsive.min.js',
		'src/scripts/datatable/responsive.bootstrap4.js',
		config.nodeDir + 'bootstrap-select/dist/js/bootstrap-select.min.js',
		config.nodeDir + 'croppie/croppie.min.js',
		'src/scripts/dashboard-setting.js', // all dashboard setting js
		'dashboard/dev-scripts/setting.js'
	]	
};

gulp.task('styles', function() {
	var stream;
	stream = streamqueue({
		objectMode: true
	});
	stream.queue(gulp.src(path.styles));
	return stream.done().pipe(plumber())
	.pipe(sass())
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	})).pipe(concat('style.css')).pipe(gulp.dest('vendors/styles/')).pipe(minify({
		keepSpecialComments: 0
	})).pipe(rename({
		suffix: '.min'
	})).pipe(plumber.stop()).pipe(gulp.dest('vendors/styles/')).pipe(browserSync.reload({
		stream: true
	}));
});

gulp.task('dashboard_styles', function() {
	var stream;
	stream = streamqueue({
		objectMode: true
	});
	stream.queue(gulp.src(path.dashboard_styles));
	return stream.done().pipe(plumber())
	.pipe(sass())
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	})).pipe(concat('dashboard-style.css')).pipe(gulp.dest('vendors/styles/')).pipe(minify({
		keepSpecialComments: 0
	})).pipe(rename({
		suffix: '.min'
	})).pipe(plumber.stop()).pipe(gulp.dest('vendors/styles/')).pipe(browserSync.reload({
		stream: true
	}));
});

gulp.task('dashboard_scripts', function() {
	var stream;
	stream = streamqueue({
		objectMode: true
	});
	stream.queue(gulp.src(path.dashboard_scripts));
	return stream.done().pipe(plumber()).pipe(concat('dashboard-script.js')).pipe(gulp.dest('vendors/scripts/')).pipe(rename({
		suffix: '.min'
	})).pipe(uglify()).pipe(plumber.stop()).pipe(gulp.dest('vendors/scripts/')).pipe(browserSync.reload({
		stream: true
	}));
});


gulp.task('scripts', function() {
	var stream;
	stream = streamqueue({
		objectMode: true
	});
	stream.queue(gulp.src(path.scripts));
	return stream.done().pipe(plumber()).pipe(concat('script.js')).pipe(gulp.dest('vendors/scripts/')).pipe(rename({
		suffix: '.min'
	})).pipe(uglify()).pipe(plumber.stop()).pipe(gulp.dest('vendors/scripts/')).pipe(browserSync.reload({
		stream: true
	}));
});

gulp.task('images', function() {
	var stream;
	stream = streamqueue({
		objectMode: true
	});
	stream.queue(gulp.src(path.images));
	return stream.done().pipe(changed('vendors/images/')).pipe(imagemin({
		optimizationLevel: 3,
		progressive: true,
		interlaced: true
	})).pipe(gulp.dest('vendors/images/'));
});

gulp.task('php', function() {
	var stream;
	stream = streamqueue({
		objectMode: true
	});
	stream.queue(gulp.src(path.php));
	return stream.done().pipe(browserSync.reload({
		stream: true
	}));
});

gulp.task('fonts', function() {
	var stream;
	stream = streamqueue({
		objectMode: true
	});
	stream.queue(gulp.src(path.fonts));
	return stream.done().pipe(gulp.dest('vendors/fonts/'));
});

// gulp.task('connect-sync', function() {
// 	browserSync.init({
// 		server: 'localhost/pph',
// 		open: false,
// 		reloadDelay: 10,
// 		watchOptions: {
// 			debounceDelay: 10
// 		}
// 	});

// 	gulp.watch("src/styles/**/*.*", gulp.parallel('styles'))
//     	.on('change', browserSync.reload);

// 	gulp.watch("src/scripts/**/*.js", gulp.parallel('scripts'))
//     	.on('change', browserSync.reload);

//     gulp.watch(path.php, gulp.parallel('php'))
//     	.on('change', browserSync.reload);
// });

gulp.task('connect-sync', ['styles', 'fonts', 'scripts', 'dashboard_styles', 'dashboard_scripts'], function() {
	browserSync.init({
		proxy: 'localhost/pph',
		port: 3000,
		open: true,
		reloadDelay: 50,
		watchOptions: {
			debounceDelay: 50
		}
	});
	gulp.watch(['src/styles/**/**'], ['styles']);
	gulp.watch(['src/scripts/**/**'], ['scripts']);
	gulp.watch(path.php, ['php']);
	return gulp.watch(['*', 'vendors/**/**'], function(file) {
		if (file.type === "changed") {
			return browserSync.reload(file.path);
		}
	});
});

gulp.task('watch', function(){
	gulp.watch("src/styles/**/*.*", ['styles']);
	gulp.watch("src/styles/**/*.*", ['dashboard_styles']);
	gulp.watch("src/fonts/**/*", ['fonts']);
	gulp.watch("src/scripts/**/*.js", ['scripts']);
	gulp.watch("src/scripts/**/*.js", ['dashboard_scripts']);
	gulp.watch("dashboard/dev-scripts/setting.js", ['dashboard_scripts']);
});

gulp.task('default', ['styles', 'fonts', 'scripts', 'dashboard_styles', 'dashboard_scripts', 'connect-sync'], function(){
	gulp.watch("src/styles/**/*.*", ['styles']);
	gulp.watch("src/styles/**/*.*", ['dashboard_styles']);
	gulp.watch("src/fonts/**/*", ['fonts']);
	gulp.watch("src/scripts/**/*.js", ['scripts']);
	gulp.watch("src/scripts/**/*.js", ['dashboard_scripts']);
	gulp.watch("dashboard/dev-scripts/setting.js", ['dashboard_scripts']);
	gulp.watch("dashboard/dev-styles/style.css", ['dashboard_styles']);
});