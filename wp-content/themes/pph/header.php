<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="theme-color" content="#EA5351">
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
	
	<!-- Google Analytics Code -->

	<!-- <script type="text/javascript">//<![CDATA[
	        // var Translator = new Translate([]);
	        //]]></script> --><!-- Page-hiding snippet (recommended)  -->
	<style>.async-hide { opacity: 0 !important} </style>
	<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
	h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
	(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
	})(window,document.documentElement,'async-hide','dataLayer',4000,
	{'GTM-MZCK72F':true});
	</script>

	<!-- Modified Analytics tracking code with Optimize plugin -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69741783-1', 'auto');
	  ga('require', 'GTM-MZCK72F');
	  ga('send', 'pageview');

	</script>

	<!-- Google Tag Manager -->

	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W7DHFNX');</script>

	<!-- End Google Tag Manager -->

	<!-- Google Site Verification -->

	<meta name="google-site-verification" content="9cYg6NXSEgyuo4kg6EoghoTLDcOyOLtgJeKPGbAvtxU" />
	<meta name="google-site-verification" content="ZXeA0gujJ7Rmp5W7KY_t0uPXSJ9f1cmad_WJ7CBhLls" />
	<meta name="msvalidate.01" content="36E07A6038A755E63C26C752370A99A2" />
	<meta name="wot-verification" content="eb4f68ca4f64f57c3a98"/>

	<!-- Hello Bar -->

	<script src="//my.hellobar.com/b81399f89383ce3b6849833fa064880825d1e52d.js" async="async"></script>
	<script type="text/javascript">    window._mfq = window._mfq || [];    (function() {        var mf = document.createElement("script");        mf.type = "text/javascript"; mf.async = true;        mf.src = "//cdn.mouseflow.com/projects/b96e6d67-84ee-4a4b-80cb-72fd4aa034d8.js";        document.getElementsByTagName("head")[0].appendChild(mf);    })();</script>

	<!-- End Hello Bar -->

</head>
<?php 
	define('TEMPLATE_DIR', get_template_directory_uri());
	define('PPH_SITE_URL', get_site_url());
	$single_post_class = is_singular('post') ? 'blog-detail-body' : '';
?>
<body <?php body_class( $single_post_class ); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7DHFNX"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<main>
		<div class="pre-loader"></div>
		<div class="navbar-wrap">
			<header>
				<div class="container">
					<div class="logo">
						<a href="<?php echo PPH_SITE_URL ?>">
							<?php $logo_id = get_theme_mod('custom_logo');
							$logo_src = wp_get_attachment_image_src($logo_id,'full'); ?>
							<div class="icon"><img src="<?php echo $logo_src[0]; ?>" alt="logo"></div>
							<div class="brand-name">PIXEL PERFECT HTML</div>
						</a>
					</div>
					<div class="navbar-right">
						<div class="nav-menu"><?php
							wp_nav_menu(array('menu'=>'Header Menu','container'=>'`')); ?>
						</div>
						<div class="nav-cta">
							<div class="cta-btn">
								<a href="<?php echo get_permalink(36); ?>" class="cta-link cta-sm"><span>ORDER NOW</span></a>
							</div>
						</div>
						<div class="menu-icon">
							<a href="javascript:;"><span>Menu</span></a>
						</div>
						<div class="nav-login dropdown <?php echo (is_user_logged_in() ? 'user_login_success' : '' ); ?>">
							<a href="javascript:;" class="dropdown-toggle"><i class="fa fa-user-o"></i></a>
							<div class="dropdown-menu dropdown-menu-right"><?php 
								if (!is_user_logged_in()): ?>
									<div class="form-wrap login-form">
										<div class="msg-status"></div>
										<form method="post" name="login_form" action="" id="login_form_dt" class="login_form">
											<?php wp_nonce_field(); ?>
											<div class="form-group">
												<input type="text" class="form-control" name="login_username" placeholder="Email">
											</div>
											<div class="form-group forgot-password">
												<input type="Password" class="form-control" name="login_password" placeholder="Password">
												<a href="<?php echo wp_lostpassword_url(); ?>">Forgot?</a>
											</div>
											<div class="form-group">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input" name="remember_me" id="remember_me_dt">
													<label class="custom-control-label" for="remember_me_dt">Remember me</label>
												</div>
											</div>
											<div class="submit-btn cta-btn">
												<button type="submit" class="cta-link btn-block login_submit">LOGIN</button>
											</div>
										</form>
									</div><?php 
									else: ?>
										<div class="user-dropdown">
											<ul>
												<li><a href="<?php echo get_permalink(249230); ?>">Dashboard</a></li>
												<li><a href="<?php echo get_permalink(249436); ?>">Edit Profile</a></li>
												<li><a href="<?php echo get_permalink(249447); ?>">Change Password</a></li>
												<li><a href="<?php echo wp_logout_url( PPH_SITE_URL ) ?>" class="logout">Logout</a></li>
											</ul>
										</div>
										<?php 
								endif; ?>
							</div>
						</div>  
					</div>
				</div>
			</header>
			<div class="mobile-menu">
				<div class="menu-icon">
					<a href="javascript:;"><span>Close</span></a>
				</div>
				<div class="nav-menu"><?php
					wp_nav_menu(array('menu'=>'Header Menu','container'=>'')); ?>
				</div>
				
				
				<div class="nav-login">
					<a href="#mobile-login" data-toggle="collapse"><i class="fa fa-user-o"></i> <?php echo (is_user_logged_in() ? 'Profile' : 'Login'); ?></a>
					<div class="collapse" id="mobile-login"><?php 
						if (!is_user_logged_in()): ?>
							<div class="form-wrap login-form">
								<div class="msg-status"></div>
								<form method="post" name="login_form" action="" id="login_form_mob" class="login_form">
									<?php wp_nonce_field(); ?>
									<div class="form-group">
										<input type="text" class="form-control" name="login_username" placeholder="Email">
									</div>
									<div class="form-group forgot-password">
										<input type="Password" class="form-control" name="login_password" placeholder="Password">
										<a href="<?php echo wp_lostpassword_url(); ?>">Forgot?</a>
									</div>
									<div class="form-group">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="remember_me" id="remember_me_mob">
											<label class="custom-control-label" for="remember_me_mob">Remember me</label>
										</div>
									</div>
									<div class="submit-btn cta-btn">
										<button type="submit" class="cta-link btn-block login_submit">LOGIN</button>
									</div>
								</form>
							</div><?php 
						else: ?>
							<div class="user-dropdown">
								<ul>
									<li><a href="<?php echo get_permalink(249230); ?>">Dashboard</a></li>
									<li><a href="<?php echo get_permalink(249436); ?>">Edit Profile</a></li>
									<li><a href="<?php echo get_permalink(249447); ?>">Change Password</a></li>
									<li><a href="<?php echo wp_logout_url( PPH_SITE_URL ) ?>" class="logout">Logout</a></li>
								</ul>
							</div><?php 
						endif; ?>
					</div>
				</div> 
			</div>
			<div class="menu-overlay"></div>
		</div>
		<div class="dropdown-overlay"></div>
