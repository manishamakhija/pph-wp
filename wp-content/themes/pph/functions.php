<?php

if ( ! function_exists( 'pph_setup' ) ) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function pph_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     */
    load_theme_textdomain( 'pph', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 1568, 9999 );

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus(
      array(
        'menu-1' => __( 'Primary', 'pph' ),
        'footer' => __( 'Footer Menu', 'pph' ),
        'social' => __( 'Social Links Menu', 'pph' ),
      )
    );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support(
      'html5',
      array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
      )
    );

    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support(
      'custom-logo'
    );

    // Add theme support for selective refresh for widgets.
    add_theme_support( 'customize-selective-refresh-widgets' );

    // Add support for Block Styles.
    add_theme_support( 'wp-block-styles' );

    // Add support for full and wide align images.
    add_theme_support( 'align-wide' );

    // Add support for editor styles.
    add_theme_support( 'editor-styles' );

    // Enqueue editor styles.
    add_editor_style( 'style-editor.css' );

    // Add custom editor font sizes.
    add_theme_support(
      'editor-font-sizes',
      array(
        array(
          'name'      => __( 'Small', 'pph' ),
          'shortName' => __( 'S', 'pph' ),
          'size'      => 19.5,
          'slug'      => 'small',
        ),
        array(
          'name'      => __( 'Normal', 'pph' ),
          'shortName' => __( 'M', 'pph' ),
          'size'      => 22,
          'slug'      => 'normal',
        ),
        array(
          'name'      => __( 'Large', 'pph' ),
          'shortName' => __( 'L', 'pph' ),
          'size'      => 36.5,
          'slug'      => 'large',
        ),
        array(
          'name'      => __( 'Huge', 'pph' ),
          'shortName' => __( 'XL', 'pph' ),
          'size'      => 49.5,
          'slug'      => 'huge',
        ),
      )
    );

    // Editor color palette.
    // add_theme_support(
    //  'editor-color-palette',
    //  array(
    //    array(
    //      'name'  => __( 'Primary', 'pph' ),
    //      'slug'  => 'primary',
    //      'color' => pph_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 33 ),
    //    ),
    //    array(
    //      'name'  => __( 'Secondary', 'pph' ),
    //      'slug'  => 'secondary',
    //      'color' => pph_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 23 ),
    //    ),
    //    array(
    //      'name'  => __( 'Dark Gray', 'pph' ),
    //      'slug'  => 'dark-gray',
    //      'color' => '#111',
    //    ),
    //    array(
    //      'name'  => __( 'Light Gray', 'pph' ),
    //      'slug'  => 'light-gray',
    //      'color' => '#767676',
    //    ),
    //    array(
    //      'name'  => __( 'White', 'pph' ),
    //      'slug'  => 'white',
    //      'color' => '#FFF',
    //    ),
    //  )
    // );

    // Add support for responsive embedded content.
    add_theme_support( 'responsive-embeds' );
  }
endif;
add_action( 'after_setup_theme', 'pph_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pph_widgets_init() {

  register_sidebar(
    array(
      'name'          => __( 'Footer', 'pph' ),
      'id'            => 'sidebar-1',
      'description'   => __( 'Add widgets here to appear in your footer.', 'pph' ),
      'before_widget' => '<section id="%1$s" class="widget %2$s">',
      'after_widget'  => '</section>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
    )
  );

}
add_action( 'widgets_init', 'pph_widgets_init' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function pph_content_width() {
  // This variable is intended to be overruled from themes.
  // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
  // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
  $GLOBALS['content_width'] = apply_filters( 'pph_content_width', 640 );
}
add_action( 'after_setup_theme', 'pph_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function pph_scripts() {
  wp_dequeue_style( 'notify-subscribers' );
  wp_dequeue_style( 'notify-subscribers-block' );
  wp_dequeue_style( 'rpdp-frontend-styles' );
  wp_dequeue_script( 'rpdp-frontend-scripts' );

  // dequeue woo assets
  wp_dequeue_script( 'wc-cart-fragments' );
  wp_dequeue_style('woocommerce-layout'); 
  wp_dequeue_style('woocommerce-smallscreen'); 
  wp_dequeue_style('wc-block-style');
  if ( ! is_page(36) && !is_wc_endpoint_url( 'lost-password' ) ){ // order page
    wp_dequeue_style( 'woocommerce-general' );
  }
  // if (is_page(36) || is_wc_endpoint_url( 'lost-password' )){
  //   wp_enqueue_style( 'woocommerce-general' ); 
  // }
  
  wp_enqueue_style( 'pph-style', get_template_directory_uri() . '/vendors/styles/style.min.css', array(), wp_get_theme()->get( 'Version' ) );
  // wp_enqueue_style( 'pph-style-temp', get_template_directory_uri() . '/src/dev-css/dev.css', array(), wp_get_theme()->get( 'Version' ) );
  wp_enqueue_style( 'pph-typekit', 'https://use.typekit.net/dhk8fok.css', array(), '1.0');
  wp_enqueue_script( 'pph-stripe-js', 'https://js.stripe.com/v3/' , array(), '1.0', true );
  wp_enqueue_script( 'pph-script', get_theme_file_uri( '/vendors/scripts/script.min.js' ), array(), '1.0', true );
  
  wp_enqueue_script( 'pph-validate', get_theme_file_uri( '/src/dev-js/jquery.validate.min.js' ), array(), '1.0', true );
  if ( is_page(36) ){ //order page
      wp_enqueue_script( 'pph-vue', get_theme_file_uri( '/src/dev-js/vue.min.js' ), array(), '1.0', true );
      wp_enqueue_script( 'pph-axios', get_theme_file_uri( '/src/dev-js/axios.min.js' ), array(), '1.0', true );
      wp_enqueue_script( 'pph-order', get_theme_file_uri( '/src/dev-js/order.js' ) , array(), '1.0', true );
      wp_enqueue_script( 'pph-order-init', get_theme_file_uri( '/src/dev-js/order-init.js' ), array(), '1.0', true );
  }
  // wp_enqueue_script( 'pph-dev-script', get_theme_file_uri( '/src/dev-js/dev.js' ), array(), '1.0', true );
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
  if ( is_singular('post') || is_page(457) || is_page(36) ) { // FAQ, Order Page, blog detail
    wp_enqueue_script( 'pph-fix-position', get_theme_file_uri( '/vendors/scripts/fixposition.js' ), array(), '1.0', true );
  }
  if ( !is_singular('post') ){
    wp_dequeue_style('rpdp-frontend-media-styles');
  }
  // wp_localize_script('pph-dev-script', 'js_obj',
  //           array('ajax_url' => admin_url( 'admin-ajax.php' ),
  //             'template_url' => get_template_directory_uri(),
  //             'site_url' => get_site_url() ) );
  wp_localize_script('pph-order', 'obj',
            array( 'template_url' => get_template_directory_uri(),
                'site_url' => get_site_url(),
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'dynamic_data' => pph_fetch_service_prices(97) ) );
  wp_localize_script('pph-script', 'js_object',
            array('folder_uri' => get_template_directory_uri(),
                  'site_url' => get_site_url(),
                  'ajax_url' => admin_url( 'admin-ajax.php' ),
                  'template_url' => get_template_directory_uri() ) );
}
add_action( 'wp_enqueue_scripts', 'pph_scripts' );


/**
 * Enqueue Admin side scripts and styles.
 */
function load_pph_admin_style() {
    wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/src/dev-css/admin-style.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_pph_admin_style' );


/**
 * Theme settings global page
 */
if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'PPH General Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
}

/**
 * Add SVG capabilities 
 */
if (!function_exists('pph_mime_types')){

    function pph_mime_types($mimes) {  
      $mimes['svg'] = 'image/svg+xml'; 
      return $mimes; 
  }
}
add_filter('upload_mimes', 'pph_mime_types');


/**
 * Pagination
 *
 * @param      array   $args   The arguments
 * @param      string  $class  The class
 */
function pph_pagination( $args = [], $class = 'pagination' ) {
  
  global $wp_query;

  $links = paginate_links( array(
    'prev_next'          => false,
    'type'               => 'array',
  ) );

  if ( $links ) :
    echo '<div class="pagination-block">';
    // get_previous_posts_link will return a string or void if no link is set.
    echo get_previous_posts_link( __( '<span>PREV</span>', 'pph' ) );
    echo '<ul>';
    foreach($links as $link){
      $active = preg_match_all('/<span aria-current.*?\>(.*?)<\/span>/si', $link, $matches ) ? ' active' : '';
      $class = $active != '' ? 'class = "'. $active .'"' : '';
      echo '<li ' . $class .'>'. $link .'</li>';

    }
    echo '</ul>';

    // get_next_posts_link will return a string or void if no link is set.
    echo get_next_posts_link( __( '<span>NEXT</span>', 'pph' ) );
    echo '</div>';
  endif;
}
add_action( 'pph_pagination', 'pph_pagination' );

/**
 * Next post link filter.
 *
 * @return     string
 */
function pph_posts_next_link_attributes() {
    return 'class="next"';
}
add_filter( 'next_posts_link_attributes', 'pph_posts_next_link_attributes' );

/**
 * Previous post link filter.
 *
 * @return     string
 */

function pph_posts_previous_link_attributes() {
    return 'class="prev"';
}

add_filter( 'previous_posts_link_attributes', 'pph_posts_previous_link_attributes' );


/*Comment Template*/
// add_filter( 'comment_form_fields', 'comment_form_fields' );

// function comment_form_fields( $fields ) {
//     unset($fields['author']);
//     unset($fields['email']);
//     unset($fields['url']);
//     unset( $fields['comment'] );
//     unset($fields['cookies']);

//     $commenter = wp_get_current_commenter(); 
//  $req = get_option( 'require_name_email' );
//  $aria_req = ( $req ? " aria-required='true'" : '' );
//  // $consent = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';

//      $fields[] = '<div class="form-wrap">';

//      $fields[] = '<div class="row clearfix"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">' ;
//      $fields['comment'] = '<div class="form-group">' . __( 'Comment', 'noun' ) .
//                '</label><textarea id="comment" name="comment" class="form-control">' .
//                '</textarea></p>';
//    $fields[] = '</div></div>'; 
      
//      $fields[] = '<div class="row clearfix"><div class="form-group">' ;
//        $fields['author'] = '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . '</label> ' .
//                '<input id="author" name="author" class="form-control" type="text" value="' . esc_attr(  $commenter['comment_author'] ) .'" size="30"' . $aria_req . '/></p>';
//    $fields[] = '</div></div>';
        
    
//    $fields[] = '<div class="col-lg-6"> <div class="form-group">' ;     
//      $fields['email']  = '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . '</label> ' .
//                '<input id="email" name="email" type="email" class="form-control" value="' . esc_attr(  $commenter['comment_author_email'] ) .'" size="30"' . $aria_req . '/></p>';
//    $fields[] = '</div></div>';     

              

//    $fields[] = '<div class="col-lg-12"> <div class="form-group hide-cookies-consent">' ;
//      $fields['cookies'] = '<p class="comment-form-cookies-consent"><input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes" checked="checked" />' .'<label for="wp-comment-cookies-consent">' . __( ' Save my name, email, and website in this browser for the next time I comment.' ) . '</label></p>';
//    $fields[] = '</div></div>'; 

//    $fields[] = '<div class="col-lg-12 form-submit">';
//    $fields['submit_button'] = '<div class="form-group">
//                <input name="%1$s" type="submit" id="%2$s" class="%3$s btn submit-cta" value="SUBMIT COMMENT" />';
//    $fields[] = '</div></div>';

//    $fields[] = '</div>';
//    if ( ! get_option( 'show_comments_cookies_opt_in' ) )  unset($fields['cookies']);
//  return $fields;
// }


/** Comment Template */
function pph_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
      <div id="comment-<?php comment_ID(); ?>" class="comment-box">
          <div class="comment-author vcard clearfix">
              <div class="comment-author-right">
                <div class="comment-author-info clearfix">
                  <?php printf(__('<cite class="fn">%s</cite>'), get_comment_author()) ?>
                </div>
                <div class="comment-desc">
                  <?php if ($comment->comment_approved == '0') : ?>
                  <em><?php _e('Your comment is awaiting moderation.') ?></em>
                  <?php endif; ?>
                  <p><?php comment_text() ?></p>
              </div>
              <?php if($args['max_depth']!=$depth) { ?>
                      <div class="cta-btn">
                          <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth'],'reply_text'=>'<span>Reply</span>'))) ?>
                      </div>
                  <?php } ?>
            </div>
        </div>
      </div>
<?php }

add_filter('comment_reply_link', 'replace_reply_link_class');
function replace_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='comment-reply-link cta-link cta-style2", $class);
    return $class;
}

/**
 * Add classes in body tag
 */
add_filter( 'body_class','pph_body_classes' );
function pph_body_classes( $classes ) {
 
  if (is_page('faq')):
      $classes[] = 'faq-page';
    elseif (is_page('order')):
      $classes[] = 'order-page';
    elseif (is_page('login') || is_wc_endpoint_url( 'lost-password' )):
      $classes[] = 'login-body';  
    endif;
    return $classes;
     
}

// general login
add_action( 'wp_ajax_login_user_form', 'login_user_form' );
add_action( 'wp_ajax_nopriv_login_user_form', 'login_user_form' );
function login_user_form(){
  if (isset($_POST['formData'])){ 
    $unserialize_logindata = array();
    parse_str($_POST['formData'],$unserialize_logindata);
    $user_username = $unserialize_logindata['login_username'];
    $user_password = $unserialize_logindata['login_password'];
    if (  !wp_verify_nonce($unserialize_logindata['_wpnonce'])  ) {
       $msg = array('result' => '0','message' => 'Unauthorized access');
    } 
    else {
      if( $user_username == '' || $user_password == '' ){
              $msg = array('result' => '0','message' => 'Please enter e-mail and password.');
      }else {
            $user_name = array();
            if ( strpos($user_username, '@') ){
              $user = get_user_by('email', $user_username);
              $user_name['user_login'] = $user->user_login;
              $user_name['user_password'] = $user_password;
            }else{
              $user_name['user_login'] = $user_username;
              $user_name['user_password'] = $user_password;
            }

            if (isset($unserialize_logindata['remember_me'])) {
                $user_name['remember'] = true; 
            }else {
                $user_name['remember'] = false;
            }

            // if ( wp_get_referer() ){

            //     $url =  wp_get_referer();

            // }else{

            //     $url = get_permalink(249230);
            // }

            $user_verify = wp_signon($user_name, false); 
            if (is_wp_error($user_verify) ) {
                $msg = array('result' => '0','message' => $user_verify->get_error_message());
            } else {
              $_SESSION['login_success'] = time();
                $msg = array('result' => '1', 'url' => get_permalink(249230));
            }
        }
    }
}else{
      $msg = array('result'=>'0','message'=>'Unauthorized access');
    }

    echo json_encode($msg);
    exit;
}

/*
** Fetch Client IP
*/
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

// saving all contact forms
add_action( 'wpcf7_before_send_mail', 'process_contact_form_data' );
function process_contact_form_data( $contact_form ){

  if (!isset($contact_form->posted_data) && class_exists('WPCF7_Submission')) {
          $submission = WPCF7_Submission::get_instance();
          if ($submission) {
              $formdata = $submission->get_posted_data();
              if ( $formdata['_wpcf7'] == '476' || $formdata['_wpcf7'] == '310' || $formdata['_wpcf7'] == '413' ){
                  if ($formdata['_wpcf7'] == '476'){    // faq
                    $form_type = 'FAQ';
                    $user_info = 
                    'Name: '. $formdata['text-517'] ."\r\n".
                    'Email: <a href = "'. $formdata['email-661'] .'">' . $formdata['email-661'] . '</a>'."\r\n".
                    'Phone: '. $formdata['tel-571'] ."\r\n".
                    'Message: '. $formdata['textarea-731'] ."\r\n".
                    'Client IP: '. get_client_ip();
                    $post_title = $formdata['text-517'] . '-' . date('F j, Y H:i:s');
                  }
                  elseif ($formdata['_wpcf7'] == '310'){  // agency form
                    $form_type = 'Agency';
                    $user_info = 
                          'Name: '. $formdata['your-name'] ."\r\n".
                          'Agency Name: '. $formdata['agency-name'] ."\r\n".
                          'Email: <a href = "'. $formdata['email-131'] .'">' . $formdata['email-131'] . '</a>'."\r\n".
                          'Phone: '. $formdata['tel-201'] ."\r\n".
                          'Message: '. $formdata['message-521']."\r\n".
                          'Client IP: '. get_client_ip();
                        $post_title = $formdata['your-name'] . '-' . date('F j, Y H:i:s');
                  }
                  elseif ($formdata['_wpcf7'] == '413'){  // Contact us
                    $form_type = 'Contact us';
                    $user_info = 
                          'Name: '. $formdata['your-name'] ."\r\n".
                          'Email: <a href = "'. $formdata['email-919'] .'">' . $formdata['email-919'] . '</a>'."\r\n".
                          'Phone: '. $formdata['tel-478'] ."\r\n".
                          'Message: '. $formdata['textarea-640'] ."\r\n".
                          'Client IP: '. get_client_ip();
                        $post_title = $formdata['your-name'] . '-' . date('F j, Y H:i:s');
                        // add_query_arg( array('enquire_name' => $formdata['your-name'] ), get_home_url('/thank-you') );
                  }
                  $arg = array('post_title' => $post_title,'post_type' => 'feedback','post_status'=>'publish','post_content' => $user_info);
                    $form_id = wp_insert_post($arg);
                    update_post_meta($form_id,'form_type',$form_type);

              } elseif ( $formdata['_wpcf7'] == '249389' ){  // leave a testimonial(dashboard)
                  $testimonial_title = $formdata['testi-name'];
                  $testimonial_project = $formdata['testi-projects'];
                  $content = $formdata['testi-subject'] ."<br>". $formdata['review-231'];
                  $args = array('post_title' => $testimonial_title, 'post_type' => 'testimonial','post_content' => $content);
                  $testi_id = wp_insert_post($args);
                  update_post_meta( $testi_id,'testimonial_for_project',$testimonial_project );
                  update_post_meta( $testi_id,'one_liner_review',$formdata['testi-subject'] );
                  $user_obj = get_user_by('email', $formdata['your-email']);
                  $user_id = $user_obj->ID;
                  $client_avatar = get_user_meta( $user_id,'user_avatar',true );
                  if ( $client_avatar ){
                    $client_avatar_path = get_template_directory().'/dashboard/user-avatars/'.$client_avatar;
                    $client_avatar_attachment_id = upload_user_avatar_in_media($client_avatar_path,$testi_id);
                  }else{
                    // thumbnail id of default testimonial image
                    $client_avatar_attachment_id = 249472;
                  }
                  set_post_thumbnail( $testi_id, $client_avatar_attachment_id );
              }
          }
    }

}

/*
** Upload Client Avatar in WP Media 
*/
function upload_user_avatar_in_media( $file, $parent_post_id ){
    $filename = basename($file);
    $upload_file = wp_upload_bits($filename, null, file_get_contents($file));
    if (!$upload_file['error']) {
      $wp_filetype = wp_check_filetype($filename, null );
      $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_parent' => $parent_post_id,
        'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
        'post_content' => '',
        'post_status' => 'inherit'
      );
      $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $parent_post_id );
      if (!is_wp_error($attachment_id)) {
        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
        wp_update_attachment_metadata( $attachment_id,  $attachment_data );
        return $attachment_id;
      }
    }
}
// adding filter in admin feedback posttype list page
add_action( 'restrict_manage_posts', 'pph_admin_posts_filter_restrict_manage_posts' );

function pph_admin_posts_filter_restrict_manage_posts(){
    $type = 'feedback';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    if ('feedback' == $type){
        $values = array(
            'Contact us' => 'Contact us', 
            'FAQ' => 'FAQ',
            'Agency' => 'Agency',
            'Quick Quote' => 'Quick Quote',
            'Psd To Html5' => 'Psd To Html5',
            'Sketch To Html' => 'Sketch To Html',
            'Xd To Html' => 'Xd To Html',
            'Psd To Wordpress' => 'Psd To Wordpress',
            'Psd To Drupal' => 'Psd To Drupal',
            'Psd To Woocommerce' => 'Psd To Woocommerce',
            'Psd To Magento' => 'Psd To Magento',
            'Psd To Hubspot Templates' => 'Psd To Hubspot Templates',
            'Html Maintenance' => 'Html Maintenance',
            'Wordpress Support And Maintenance' => 'Wordpress Support And Maintenance'
        );
        ?>
        <select name="pph_form_type">
        <option value=""><?php _e('Filter By ', 'pph'); ?></option>
        <?php
            $current_v = isset($_GET['pph_form_type']) ? $_GET['pph_form_type'] : '';
            foreach ($values as $label => $value) {
                printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_v? ' selected="selected"':'',
                        $label
                    );
                }
        ?>
        </select>
        <?php
    }
}

add_filter( 'parse_query', 'pph_form_filter' );
function pph_form_filter( $query ){
    global $pagenow;
    $type = 'feedback';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }
    if ( 'feedback' == $type && is_admin() && $pagenow == 'edit.php' && isset($_GET['pph_form_type']) && $_GET['pph_form_type'] != '') {
        $query->query_vars['meta_key'] = 'form_type';
        $query->query_vars['meta_value'] = $_GET['pph_form_type'];
    }
}

// pph order page coupon discount
add_action( 'wp_ajax_pph_discount_coupon', 'pph_discount_coupon' );
add_action( 'wp_ajax_nopriv_pph_discount_coupon', 'pph_discount_coupon' );
function pph_discount_coupon(){
  if (isset($_GET['productId'])){
    if ($_GET['couponCode'] != ''){
      $_SESSION['customer_price'] = $_GET['totalCost'];
      $product_id = $_GET['productId'];
      $coupon_code = $_GET['couponCode'];
      WC()->cart->empty_cart();
      WC()->cart->add_to_cart( $product_id );
      WC()->cart->add_discount( $coupon_code );
      $notice = wc_print_notices(true);
      $discounted_price = WC()->cart->total;
      if ( WC()->cart->has_discount( $coupon_code ) ) {
        $discount = $_GET['totalCost'] - $discounted_price;
        $msg = array('result'=>'1','discount' => $discount,'message' => $notice);

      }else{
        $msg = array('result'=>'0','message' => $notice);
      }
    }else{
      $msg = array('result'=>'0','message' => 'Please Enter Coupon Code');
    }
  }else{
    $msg = array('result'=>'0','message' => 'Invalid Product');
  }
  echo wp_json_encode($msg);
  wp_die();
}

function register_session(){
    if( !session_id() ):
        session_start();
    endif;
}
add_action('init','register_session');
// add_action( 'woocommerce_before_calculate_totals', 'misha_recalc_price' );
 
// function misha_recalc_price( $cart_object ) {
//  foreach ( $cart_object->get_cart() as $hash => $value ) {
//    $value['data']->set_price( 10 );
//  }
// }

add_action( 'woocommerce_before_calculate_totals', 'add_custom_price', 20, 1);
function add_custom_price( $cart_obj ) {

    // This is necessary for WC 3.0+
    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;

    // Avoiding hook repetition (when using price calculations for example)
    if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
        return;
    // var_dump($_SESSION); exit;
    // Loop through cart items
    foreach ( $cart_obj->get_cart() as $cart_item ) {
      // if( !session_id()):
      //     session_start();
      if (isset($_SESSION['customer_price'])){
          $price = $_SESSION['customer_price'];
      }else{
          $price = 180;        
      }
      // endif;
      // $price = 200;
        $cart_item['data']->set_price( $price );
    }
 }

 
// remove order page coupon
add_action( 'wp_ajax_pph_remove_discount_coupon', 'pph_remove_discount_coupon' );
add_action( 'wp_ajax_nopriv_pph_remove_discount_coupon', 'pph_remove_discount_coupon' );
function pph_remove_discount_coupon(){
  WC()->cart->remove_coupons();
  WC()->cart->empty_cart();
  $notice = wc_print_notices(true);
  $msg = array( 'result' => 1, 'message' => $notice );
  echo wp_json_encode($msg);
  wp_die();
}


add_action( 'wp_ajax_pph_fetch_service_prices', 'pph_fetch_service_prices' );
add_action( 'wp_ajax_nopriv_pph_fetch_service_prices', 'pph_fetch_service_prices' );
function pph_fetch_service_prices($serviceid = null){
  // var_dump($_GET);
  if ( isset( $_GET['service'] ) ){
    $service_id = $_GET['service'];
  }else if ($serviceid != null){
    $service_id = $serviceid;
  }
    $price_structure = get_field('prices', $service_id );

    // package features
    $basic_features = array();
    $premium_features = array();
    while (has_sub_field('packages_details', $service_id)):
      if (get_row_layout() == 'package_features'):

         get_sub_field('standard') ? array_push($basic_features, get_sub_field('feature')) : '';
         get_sub_field('standard') !== get_sub_field('premium') ? array_push($premium_features, get_sub_field('feature')) : '';
      endif;
    endwhile;
    // $service_icon = get_field('service_icon', $_GET['service'])['url'];
    // var_dump($price_structure);
    // restructured price array
    foreach($price_structure as $key => $value){
      $data[$value['delivery_type_name']] = $value['delivery_time'];
    } 
    foreach($data as $key => $value ){
      foreach($value as $k => $v){
        $data1[$key][$v['package_name']] = $v['package_type'];
      }
    } 
    foreach($data1 as $key => $value){
      foreach($value as $k => $v){
        foreach($v[0] as $hash => $match){
          foreach($match as $t => $p){
            $data2[$key][$k][$p['page_name']] = array('rate'=>$p['page_price'],'days'=>$p['page_build_time']);
          }
        }
      }
    } 
    // var_dump($data2);
    // $content = wp_json_encode($data2);
    $msg = array( 'content' => $data2 , 'basic_features' => $basic_features, 'premium_features' => $premium_features );

    // on page load fetch data
    if ($serviceid != null){
      return $msg;
    }

    echo wp_json_encode($msg);
    wp_die();
}

// display item meta in order admin page
add_action( 'woocommerce_after_order_itemmeta', 'order_meta_customized_display',10, 3 );
 
function order_meta_customized_display( $item_id, $item, $product ){
 $all_meta_data = get_metadata( 'order_item', $item_id, "", "");
//  $useless = array(
//  "_qty","_tax_class","_variation_id","_product_id","_line_subtotal","_line_total","_line_subtotal_tax", "  
// _line_tax", "_line_tax_data","line_tax"
//  );// Add key values that you want to ignore
 
//  $customized_array= array();
//  foreach($all_meta_data as $data_meta_key => $value)
//  {
//  if(!in_array($data_meta_key,$useless)){
//  $newKey = ucwords(str_replace('_'," ",$data_meta_key ));//To remove underscrore and capitalize
//  $customized_array[$newKey]=ucwords(str_replace('_'," ",$value[0])); // Pushing each value to the new array
//  }
//  }
 ?>
 
 <?php /*if (!empty($customized_array)) {*/ ?>
 <div class="order_data_column" style="float: left; width: 50%; padding: 0 5px;">
 <p>
  <span style="display:inline-block; width:100px;">
    Home Page</span><span>: $<?php echo $all_meta_data['home_page'][0]; ?></span> X 1 <span> <?php echo $all_meta_data['home_page'][0]; ?></span>
 </p>

 <?php if (isset( $all_meta_data['inner_page_count']) ) : ?>
  <p>
    <span style="display:inline-block; width:100px;">Inner Page</span><span>: $<?php echo $all_meta_data['single_inner_page'][0]; ?></span> X <?php echo $all_meta_data['inner_page_count'][0]; ?> <span> <?php echo $all_meta_data['inner_page'][0]; ?></span>
  </p> <?php 
 endif; ?>

 <p>
  <span style="display:inline-block; width:100px;">Turnaround</span><span>: <?php echo $all_meta_data['turnaround'][0]; ?></span> Business days
 </p>

<?php if (isset( $all_meta_data['show_in_portfolio_discount']) ) : ?>
   <p>
    <span style="display:inline-block; width:100px;">Show in Portfolio Discount</span><span>: - $<?php echo $all_meta_data['show_in_portfolio_discount'][0]; ?></span> 
   </p>
   <?php 
endif;
 // foreach($customized_array as $data_meta_key => $value)
 // {
 // echo '<p><span style="display:inline-block; width:100px;">' . __( $data_meta_key ) . '</span><span>:&nbsp;' . $value . '</span></p>';
 // }
 ?>
 </div>
 <?php /*}*/ ?>
 
<?php }

// display order general meta fields
add_action( 'woocommerce_admin_order_data_after_order_details', 'pph_editable_order_meta_general' );
 
function pph_editable_order_meta_general( $order ){  ?>
 
    <br class="clear" />
    <h4> Package Details </h4>
    <?php 
    $order_id = $order->get_id();
    $project_name = get_post_meta( $order_id, 'project_name', true );
    $skype_name = get_post_meta( $order_id, 'skype_name', true );
    $package = get_post_meta( $order_id, 'package', true );
    $delivery = get_post_meta( $order_id, 'delivery', true );
    $files = get_post_meta( $order_id, 'files_included', true );
    ?>
    <div class="order-details">
      <p><strong>Project Name </strong><?php echo $project_name; ?></p>
      <p><strong>Skype Name </strong><?php echo $skype_name; ?></p>
      <p><strong>Package </strong><?php echo $package; ?></p>
      <p><strong>Delivery </strong><?php echo $delivery; ?></p>
      <?php
      if( !empty( $files ) ) :
        foreach ($files as $key => $value) {
          $filepath = get_template_directory_uri().'/filepond/uploads/'.$value['new_name'];
              echo '<a href = "'. $filepath .'">'.$value['original'].'</a>'."<br>";
        } 
      endif;
      ?>        
    </div>
<?php }
 
 function paypal_button( $item_name, $price ){
  $btn_html = '';
  ob_start(); ?>
  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="parth@krishaweb.com">
    <input type="hidden" name="lc" value="US">
    <input type="hidden" name="item_name" value="<?php echo $item_name; ?>">
    <input type="hidden" name="amount" value="<?php echo str_replace(',','', $price); ?>">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="button_subtype" value="services">
    <input type="hidden" name="no_note" value="0">
    <input type="hidden" name="bn" value="PP-BuyNowBF:order-now-btn.png:NonHostedGuest">
    <input type="image" src="<?php echo get_template_directory_uri() ?>/vendors/images/order-now-btn.png" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
  </form><?php 
  $btn_html = ob_get_clean();
  return $btn_html ; 
 }

// create shortcodes for displaying form details in thank you page
 add_shortcode('enquire_person','get_person_from_contact_form');
 function get_person_from_contact_form(){
  $name = isset($_GET['enquire_name']) ? stripslashes(strtoupper( $_GET['enquire_name'] )) : '';
  $enquire_name = '<span>'. $name .'</span>';
  return $enquire_name;
 }

$pph_ty_shortcode_list = array('agency_name','service_name','order_id');
foreach($pph_ty_shortcode_list as $shortcode){
  add_shortcode($shortcode, function( $attributes, $content, $shortcode ){
    $detail = isset($_GET[$shortcode]) ? stripslashes($_GET[$shortcode]) : '';
    return $detail;
  });
  
        
}

// function get_details_from_contact_form(){
//     $detail = isset($_GET[$shortcode]) ? $_GET[$shortcode] : '';
//     // $enquire_name = '<span>'. $name .'</span>';
//     return $detail;
//   }

// Add custom query vars
// function add_query_vars_filter( $vars ){
//    $vars[] = "enquire_name";
//    return $vars;
// }
// add_filter( 'query_vars', 'add_query_vars_filter' );

// add_filter( 'init', 'add_widget_type_query_var' );
// function add_widget_type_query_var()
// {
// global $wp;
// $wp->add_query_var( 'enquire_name' );
// }

// recent post shortcode for thankyou mail template

 function pph_mail_recent_posts_shortcode($atts){
 $q = new WP_Query(
   array( 'orderby' => 'date', 'posts_per_page' => '3')
 );

$list = '<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0"><tr>';

while($q->have_posts()) : $q->the_post();

 $list .= '<td align="center" valign="top" width="133" style="width: 133px;">
      <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
          <td><img src="' . get_the_post_thumbnail_url(get_the_ID(),'full') . '" width="133" height="121" style="display: block; border:none;" alt=""></td>
        </tr>
        <tr>
          <td align="left" valign="top" style="font-family: \'Montserrat\', Arial, sans-serif; font-size: 11px; line-height: 18px; color: #273270; font-weight: 400; text-align: left; padding-top: 10px;"><a href="'. get_the_permalink() .'" style="text-decoration: none; color: #273270;" target="_blank">' . get_the_title() . '</a></td>
        </tr>
      </table>
    </td>
    <td width="30" style="width: 30px;"></td>';

endwhile;

wp_reset_query();

return $list .= '</tr></table>';
}

add_shortcode('pph-mail-latest-posts', 'pph_mail_recent_posts_shortcode');


// social network shortcode for contact form 7
function social_network_shortcode($atts){
$list1 = '<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
      <tr>
        <td align="center" valign="top" style="border: 1px solid #EDEDED; background-color: #fff; padding: 30px; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
          <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
              <td align="center" valign="top" style="padding-bottom: 15px;">
                <table width="234" cellpadding="0" cellspacing="0" align="center" border="0">
                  <tr>';

            if(get_field('facebook', 'option')): 
              $list1 .= '<td align="center" valign="top" width="42" style="width: 42px;">
                      <a href="'.get_field('facebook', 'option').'" style="text-decoration: none; display: block;" target="_blank"><img src="http://54.200.79.82/projects/pph/wp-content/themes/pph/vendors/images/facebook.png" width="42" height="46" style="display: block; border:none;" alt=""></a>
                    </td>
                    <td width="22" style="width: 22px;"></td> ';
            endif ;
            if(get_field('twitter', 'option')): 
              $list1 .= '<td align="center" valign="top" width="42" style="width: 42px;">
                    <a href="' .get_field('twitter', 'option') .'" style="text-decoration: none; display: block;" target="_blank"><img src="http://54.200.79.82/projects/pph/wp-content/themes/pph/vendors/images/twitter.png" width="42" height="46" style="display: block; border:none;" alt=""></a>
                  </td>
                  <td width="22" style="width: 22px;"></td>';
            endif ;
            if(get_field('pinterest', 'option')): 
              $list1 .= '<td align="center" valign="top" width="42" style="width: 42px;">
                  <a href="'. get_field('pinterest', 'option') .'" style="text-decoration: none; display: block;" target="_blank"><img src="http://54.200.79.82/projects/pph/wp-content/themes/pph/vendors/images/pinterest.png" width="42" height="46" style="display: block; border:none;" alt=""></a>
                </td>
                <td width="22" style="width: 22px;"></td>';
            endif;
            if(get_field('linkedin', 'option')): 
              $list1 .= '<td align="center" valign="top" width="42" style="width: 42px;">
                  <a href="'. get_field('linkedin', 'option') .'" style="text-decoration: none; display: block;" target="_blank"><img src="http://54.200.79.82/projects/pph/wp-content/themes/pph/vendors/images/linkedin.png" width="42" height="46" style="display: block; border:none;" alt=""></a>
                </td>
                <td width="22" style="width: 22px;"></td>';
            endif;            
            if(get_field('instagram', 'option')):
              $list1 .= '<td align="center" valign="top" width="42" style="width: 42px;">
                    <a href="'. get_field('instagram', 'option') .'" style="text-decoration: none; display: block;" target="_blank"><img src="http://54.200.79.82/projects/pph/wp-content/themes/pph/vendors/images/instagram.png" width="42" height="46" style="display: block; border:none;" alt=""></a>
                  <td width="22" style="width: 22px;"></td>';
            endif;
          $list1 .='</tr>
            </table>
          </td>
        </tr>
        <tr>
          <td align="center" valign="top" style="font-family: \'Montserrat\', Arial, sans-serif; font-size: 11px; line-height: 17px; color: #626262; font-weight: 400; text-align: center;">Copyrights &copy; 2019 Pixel Perfect HTML.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>';
return $list1;
}
add_shortcode('pph-mail-social-network', 'social_network_shortcode');

function pph_client_ip_mail_shortcode(){
  $html = '';
  $html = '<tr>
              <td align="left" valign="top" width="160" style="font-family: \'Montserrat\', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; width: 160px; background-color: #FAFAFA; border-right:1px solid #EDEDED; border-bottom:1px solid #EDEDED; border-top-left-radius: 5px;">Client IP Address</td>
              <td align="left" valign="top" style="font-family: \'Montserrat\', Arial, sans-serif; font-size: 12px; line-height: 20px; color: #0F101F; text-align: left; padding: 11px 15px; border-bottom:1px solid #EDEDED; border-top-right-radius: 5px;">'. get_client_ip() .'</td>
          </tr>';
  return $html;
}

add_shortcode('pph-mail-client-ip', 'pph_client_ip_mail_shortcode');

function my_special_mail_tag( $output, $name, $html ) {
  if ( 'pph-mail-latest-posts' == $name || 'pph-mail-social-network' == $name )
    $output = do_shortcode( "[$name]" );
    return $output;
}
add_filter( 'wpcf7_special_mail_tags', 'my_special_mail_tag', 10, 3 );

// made shortcodes work in contact form 7 mail templates
// add_filter('wp_mail', 'insert_shortcode_in_subscription', 10, 1);
// function insert_shortcode_in_subscription( $mail_details ) {
//   $content = $mail_details['message'];
//   $mail_details['message'] = apply_filters( 'the_content', $content );
//   return $mail_details;
// }

function pph_remove_version_scripts_styles($src) {
  if (strpos($src, 'ver=')) {
    $src = remove_query_arg('ver', $src);
  }
  return $src;
}
add_filter('style_loader_src', 'pph_remove_version_scripts_styles', 9999);
add_filter('script_loader_src', 'pph_remove_version_scripts_styles', 9999);

/**
 * scripts buffer start.
 */
function pph_buffer_start() { 
    ob_start( "pph_callback" ); 
}
add_action( 'wp_loaded', 'pph_buffer_start' );

/**
 * scripts call back.
 */
function pph_callback( $buffer ) {
    return preg_replace( "%[ ]type=[\'\"]text\/(javascript|css)[\'\"]%", '', $buffer );
}

function wc_new_order_column( $columns ) {

    $columns['order_price'] = 'Order Value';

    return $columns;

}
add_filter( 'manage_edit-shop_order_columns', 'wc_new_order_column' );


function sv_wc_cogs_add_order_profit_column_content( $column ) {

    global $post;

    if ( 'order_price' === $column ) {

        $order    = wc_get_order( $post->ID );
        $currency = is_callable( array( $order, 'get_currency' ) ) ? $order->get_currency() : $order->order_currency;
        
        $cost = get_post_meta($post->ID,'total_order_amount',true);
        
        echo wc_price( $cost, array( 'currency' => $currency ) );
    }
}
add_action( 'manage_shop_order_posts_custom_column', 'sv_wc_cogs_add_order_profit_column_content' );

// change forgot password page error messages 
add_filter('woocommerce_add_error', 'change_email_error');
function change_email_error( $message ) {

    if ($message == 'Enter a username or email address.' ) {
        $message = 'Enter a email address.';
    }

    if ($message == 'Invalid username or email.' ) {
        $message = 'Invalid email.';
    }

    return $message;
}

//dashboard function file
require_once( get_template_directory() .'/dashboard/functions.php');