<?php get_header(); ?>
	<!-- hero banner start -->
	<div class="hero-banner">
		<div class="hero-slider">
			<!-- hero slider start -->
			<?php 
			while(has_sub_field('hero_section')): ?> 
				<div class="hero-slide">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="hero-text">
									<?php the_sub_field('hero_text'); 
									if ($promocode = get_sub_field('promo_code')): ?>
										<div class="promo-code">USE CODE: <span><?php echo $promocode; ?></span></div><?php 
									endif; ?>
									<div class="cta-btn">
										<?php the_sub_field('hero_cta'); ?>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<div class="hero-img"><?php 
								$hero_image = get_sub_field('hero_image'); ?>
									<img src="<?php echo $hero_image['url']; ?>" alt="">
								</div>
							</div>
						</div>
					</div>
				</div> <?php 
			endwhile; ?>
			<!-- hero slider end -->
		</div>
	</div>
	<!-- hero banner end -->

	<!-- home services start -->
	<div class="home-services-wrap bg-light-blue">
		<div class="container">
			<!-- title start -->
			<div class="title">
				<?php the_field('service_title_&_description'); ?>
			</div>
			<!-- title end -->
			<div class="home-services-list">
				<ul class="row">
					<!-- service start --><?php 
					$service_query = new WP_Query(array('post_type'=>'product','posts_per_page'=> 6,'order'=>'ASC', 'post__in' => array(97,110,111,113,115,114)));
					while($service_query->have_posts()):$service_query->the_post(); ?>
						<li class="col-lg-4 col-md-6 col-sm-6 col-12">
							<div class="h-service-icon">
								<img src="<?php echo get_field('home_logo')['url']; ?>" alt="<?php echo get_field('home_logo')['alt']; ?>">
							</div>
							<h3><?php the_title(); ?></h3>
							<div class="h-price-service">
								Starting at <span><?php the_field('start_price'); ?></span>
							</div>
							<div class="h-service-desc">
								<?php the_field('home_excerpt'); ?>
							</div>
							<div class="cta-btn">
								<a href="<?php the_permalink(); ?>" class="cta-link cta-style2"><span>VIEW MORE</span></a>
							</div>
						</li><?php 
					endwhile; wp_reset_postdata(); ?>
					<!-- service end -->
				</ul>
			</div>
		</div>
	</div>
	<!-- home services end -->

	<div class="why-pph-section bg-light-blue">
		<div class="container">
			<div class="title">
				<h2>WHY PIXEL PERFECT HTML?</h2>
			</div>
			<div class="why-pph-points">
				<ul class="row justify-content-center"><?php 
					while(has_sub_field('why_pph_&_how_we_works')):
						if (get_row_layout() == 'pros_of_pph'): ?>
							<li class="col-md-4 col-sm-6 col-6">
								<div class="icon">
									<img src="<?php echo get_sub_field('image')['url']; ?>" class="svg" alt="<?php echo get_sub_field('image')['alt']; ?>">
								</div>
								<h4><?php the_sub_field('title'); ?></h4>
							</li><?php 
						endif;
					endwhile; ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="how-we-work-wrap bg-light-blue">
		<div class="container">
			<div class="title">
				<h2>HOW WE WORK</h2>
			</div>
			<div class="work-process">
				<ul><?php $counter = 1;
					while(has_sub_field('why_pph_&_how_we_works')):
						if (get_row_layout() == 'how_we_works'): ?>
							<li>
								<div class="icon">
									<img src="<?php echo get_sub_field('image')['url']; ?>" class="svg" alt="<?php echo get_sub_field('image')['alt']; ?>">
								</div>
								<div class="name">
									<?php the_sub_field('title'); ?>
								</div>
								<div class="line"></div>
								<div class="circle-dot"></div>
							</li><?php
							if ($counter == 5) { ?>
								</ul>
								<div class="process-connecter">
									<div class="right-corner"></div>
									<div class="line"></div>
									<div class="left-corner"></div>
								</div>
								<ul class="second-row"> <?php
							} 
							$counter++;
						endif; 
					endwhile;	?>
				</ul>
			</div>
		</div>
	</div>

	<div class="home-work-section">
		<div class="work-slider"><?php 
			$portfolio_query = new WP_Query(array('post_type'=>'portfolio','meta_key'=>'featured_project','meta_value'=>1,'posts_per_page' => 4));
			while($portfolio_query->have_posts()):$portfolio_query->the_post(); ?>
				<div class="work-slide" data-bgcolor="#<?php the_field('background_color'); ?>">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-md-7 col-sm-12">
								<div class="work-slider-img">
									<?php $project_image = get_field('home_image'); ?>
									<img src="<?php echo $project_image['url']; ?>" alt="<?php echo $project_image['alt']; ?>">
								</div>
							</div>
							<div class="col-md-5 col-sm-12">
								<div class="work-slider-text">
									<h5>FEATURED PROJECT</h5>
									<h3><?php the_title(); ?></h3>
									<?php the_field('home_excerpt'); 
									if ($website_link = get_field('website_link')): ?>
										<div class="cta-btn">
											<a href="<?php echo $website_link; ?>" class="cta-link cta-outline cta-white" target="_blank">VISIT WEBSITE</a>
										</div><?php 
									endif; ?>
								</div>
							</div>
						</div>
						<div class="slide-count-wrap">
							<span class="current"></span> <span class="separator"></span>
							<span class="total"></span>
						</div>
					</div>
				</div><?php 
			endwhile; wp_reset_postdata(); ?>
		</div>
		<div class="slider_arrow">
			<div class="slider_prev"><span>PREV</span></div>
			<div class="slider_next"><span>NEXT</span></div>
		</div>
	</div>

	<div class="customer-says-wrap bg-light-blue">
		<div class="container">
			<div class="title">
				<h2>WHAT OUR CUSTOMER SAYS?</h2>
			</div><?php
			$testimonial_query = new WP_Query(array('post_type'=>'testimonial','meta_key'=>'featured','meta_value'=>1,'posts_per_page'=>1,'orderby'=> 'rand')); 
			while($testimonial_query->have_posts()):$testimonial_query->the_post(); ?>
				<div class="customer-says-box">
					<div class="customer-img">
						<div class="img">
							<?php the_post_thumbnail(); ?>
						</div>
					</div>
					<div class="customer-quote">
						<?php the_content(); ?>
						<h3><?php the_title(); ?></h3>
					</div>
				</div><?php 
			endwhile; wp_reset_postdata(); /*?>
			<div class="cta-btn text-center">
				<a href="<?php echo get_permalink(); ?>" class="cta-link cta-outline">VIEW MORE</a>
			</div> */ ?>
		</div>
	</div>

<?php 
if (!is_user_logged_in()){
	echo get_template_part('template-parts/newsletter','form'); 
} ?>

<?php echo get_template_part('template-parts/cta','banner'); ?>

<?php get_footer(); ?>