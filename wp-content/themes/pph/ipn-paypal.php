<?php 
// namespace Listener;
/*
** Template Name: IPN Paypal
*/
// get_header(); 
wp_head();
ini_set('log_errors', true);
ini_set('error_log', 'ipn_errors.log');
require('paypal/ipnlistener.php');

$listener = new IpnListener();
$listener->use_sandbox = true;
$listener->force_ssl_v3 = false;
try {
    $listener->requirePostMethod();
    $verified = $listener->processIpn();
} catch (Exception $e) {
    error_log($e->getMessage());
    exit(0);
}

if ($verified) {
    
	$order_id = $_POST['custom'];
	$transaction_id = $_POST['txn_id'];
    // change in order post type
    $order = wc_get_order( $order_id );
    $note = 'Transaction ID: '.$transaction_id;
    $note_comment_id = $order->add_order_note( $note );
    $wpdb->update( 'wp_comments', array('comment_karma' => 1), array('comment_ID' => $note_comment_id ) );
    $payment_method = get_post_meta($order_id, 'payment_method', true );
    $order->set_payment_method($payment_method);
    $total_cost = get_post_meta($order_id, 'total_order_amount', true );
    $order->set_total($total_cost);
    $order->update_status("processing"); 
    // get order data for emails
    $project_name = get_post_meta( $order_id, 'project_name', true );
    $skype = get_post_meta( $order_id, 'skype_name', true );
    $package_name = get_post_meta( $order_id, 'package', true );
    $delivery = get_post_meta( $order_id, 'delivery', true );
    $filenames = get_post_meta( $order_id, 'files_included', true );
    $project_deadline = get_post_meta( $order_id, 'project_deadline', true );
    // $coupon_discount_amount = get_post_meta( $order_id, 'coupon_discount_amount', true );
    // $user = $order->get_user(); // Get the WP_User object
    // $name = $user->data->display_name;
    // $payment_method = $order->get_payment_method(); // Get the payment method ID
    $order_created_date  = $order->get_date_created()->format ('j F, Y'); // Get date created (WC_DateTime object)
    $phone = $order->get_billing_phone(); // Customer billing phone
    $email = $order->get_billing_email(); // Customer billing email
    $name = $order->get_billing_first_name(); // Customer billing email
    $project_note = $order->get_customer_note();
    // $coupon_discount_amount = $order->get_discount_total();
    foreach ($order->get_items() as $item_key => $item ):
        $item_id = $item->get_id();
         // $item_name    = $item->get_name();
         $product_id   = $item->get_product_id();
    endforeach;
    $home_page = wc_get_order_item_meta($item_id,'home_page',true);
    $inner_page_count = wc_get_order_item_meta($item_id,'inner_page_count', true );
    $inner_page_count = $inner_page_count == "" ? 0 : $inner_page_count;
    $single_inner_page = wc_get_order_item_meta($item_id,'single_inner_page',true);
    $inner_page = wc_get_order_item_meta($item_id,'inner_page',true);
    $turnaround = wc_get_order_item_meta($item_id,'turnaround',true); 
    $show_in_portfolio_discount = wc_get_order_item_meta($item_id,'show_in_portfolio_discount',true);
    $coupon_discount_amount = wc_get_order_item_meta( $item_id, 'discount_amount', true );
    // var_dump($coupon_discount_amount );
    include ( 'email-templates/order-mail.php');
}

// Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
// header("HTTP/1.1 200 OK");


get_footer(); ?>