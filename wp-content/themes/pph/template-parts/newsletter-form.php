<?php 
	global $post;
	$blue_background = array('services'); 
?>
<div class="subscribe-wrap <?php echo (in_array($post->post_name,$blue_background) || is_single() ? 'bg-light-blue' : ''); ?>">
	<div class="container">
		<h3>SUBSCRIBE TO OUR NEWSLETTER </h3>
		<div class="subscribe-text"><?php dynamic_sidebar('footer'); ?></div>
		<div class="subscribe-form form-wrap">
			<?php echo do_shortcode('[notify-subscribers]'); ?>
		</div>
		<div class="bg-svg">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			viewBox="0 0 1171 281" style="enable-background:new 0 0 1171 281;" xml:space="preserve">
				<polygon class="obj_1" fill="#2154CF" points="265.9,2.3 256.6,7.7 256.6,18.4 265.9,23.8 275.2,18.4 275.2,7.7 "/>
				<polygon class="obj_2" fill="#2154CF" points="185.1,133 180.6,135.7 180.6,140.9 185.1,143.5 189.7,140.9 189.7,135.7 "/>
				<polygon class="obj_3" fill="#163587" points="1008.6,74 1001.7,78 1001.7,85.9 1008.6,89.9 1015.5,85.9 1015.5,78 "/>
				<polygon class="obj_4" fill="#163587" points="1162.8,8 1156.9,11.4 1156.9,18.1 1162.8,21.4 1168.6,18.1 1168.6,11.4 "/>
				<polygon class="obj_5" fill="#2154CF" points="612.2,214.7 608.7,216.7 608.7,220.7 612.2,222.7 615.7,220.7 615.7,216.7 "/>
				<polygon class="obj_6" fill="#0F2765" points="667.9,258.3 659.3,263.2 659.3,273.1 667.9,278.1 676.5,273.1 676.5,263.2 "/>
				<polygon class="obj_7" fill="#2DC7FF" points="61,113.8 58,115.6 58,119.1 61,120.8 64.1,119.1 64.1,115.6 "/>
				<polygon class="obj_8" fill="#0F2765" points="5.7,9 2,11.2 2,15.5 5.7,17.6 9.4,15.5 9.4,11.2 "/>
				<polygon class="obj_9" fill="#2DC7FF" points="917.9,0.3 908.6,5.7 908.6,16.4 917.9,21.8 927.2,16.4 927.2,5.7 "/>
			</svg>
		</div>
	</div>
</div>