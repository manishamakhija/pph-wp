<?php 
/**
 * Plugin Name: Related Posts for Distinct Posts
 * Plugin URI: https://codecanyon.net/item/related-posts-for-distinct-posts/21357850
 * Description: lets you provide options for selecting different posts as related posts for each posts. 
 * Version: 1.0
 * Author: KrishaWeb PVT LTD
 * Author URI: http://www.krishaweb.com 
 * Text Domain: related-posts-for-distinct-posts
 * Domain path: /languages
 * License: GPL2
 */
if ( ! defined( 'ABSPATH' ) ){
	exit;
} 

define( 'RELATED_POSTS_FOR_DISTINCT_POSTS_REQUIRED_WP_VERSION', '4.3' );
define( 'RELATED_POSTS_FOR_DISTINCT_POSTS', __FILE__ );
define( 'RELATED_POSTS_FOR_DISTINCT_POSTS_BASENAME', plugin_basename( RELATED_POSTS_FOR_DISTINCT_POSTS ) );
define( 'RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_DIR', plugin_dir_path( RELATED_POSTS_FOR_DISTINCT_POSTS ) );
define( 'RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL', plugin_dir_url( RELATED_POSTS_FOR_DISTINCT_POSTS ) );
function related_posts_for_distinct_posts_activate() {
	if (!get_option('rpdp_enabled_posttypes') ) {
		update_option( 'rpdp_enabled_posttypes', array('post') );
	}
	if ( !get_option( 'rpdp_related_posttypes' ) ){
		$related_pts['post'] = array('post');	 
		update_option( 'rpdp_related_posttypes', $related_pts );
	}
	if ( !get_option('rpdp_general_options') ){
		$option_fields = array('title_of_section'=> 'Related Posts','no_of_posts'=>'3','thumbnail_display'=>'show','thumbnail_size'=>'thumbnail','excerpt_display'=>'show','content_limit'=>'10','title_display'=>'show','title_length'=> '5','post_date_display'=>'show','comment_count_display'=>'show','author_display'=>'show','link_btn_display'=>'show','link_btn_label'=>'Read More');
		update_option('rpdp_general_options',$option_fields);
	}
}
register_activation_hook( __FILE__, 'related_posts_for_distinct_posts_activate' );

function related_posts_for_distinct_posts_deactivate() {}
register_deactivation_hook( __FILE__, 'related_posts_for_distinct_posts_deactivate' );

function related_posts_for_distinct_posts_uninstall(){
	delete_option('rpdp_enabled_posttypes'); // delete DB entry in options table
	delete_option('rpdp_related_posttypes');
	delete_option('rpdp_general_options');
	$dir_path = WP_CONTENT_DIR.'/default-images/';
 	$files = glob($dir_path . '*', GLOB_MARK);
    foreach ($files as $file) {
        unlink($file); 
    }
    rmdir($dir_path);
}
register_uninstall_hook(__FILE__, 'related_posts_for_distinct_posts_uninstall');

// Admin side settings
// adding plugin's page as submenu to appearance page
function related_posts_for_distinct_posts_menu() {
add_submenu_page( 'themes.php', 'Related Posts for Distinct Posts', 'Related Posts for Distinct Posts','activate_plugins', 'related_posts_for_distinct_posts_options','related_posts_for_distinct_posts_options' );
}
add_action( 'admin_menu', 'related_posts_for_distinct_posts_menu' ); // include in admin menu
function related_posts_for_distinct_posts_options(){
	require_once( RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_DIR . 'admin/admin-options.php' );
}
require_once( RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_DIR . '/main.php' );
