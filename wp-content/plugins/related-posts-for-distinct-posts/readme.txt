=== Related Posts for Distinct Posts ===
Plugin Name: Related Posts for Distinct Posts
Plugin URI: 
Author: krishaweb
Author URI: http://krishaweb.com/
Contributors: krishaweb, manishamakhija
Tags: posts, related posts, related, distinct posts, post, individual
Requires at least: 4.4
Tested up to: 4.9.2
Stable tag: trunk
Copyright: (c) 2012-2018 KrishaWeb Technologies PVT LTD (info@krishaweb.com)
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Related Posts for Distinct Posts allows you to display custom posts as related posts for each different custom posts.

== Description ==

Related Posts for Distinct Posts is a WordPress Plugin that displays list of different default posts as well as custom post as related posts. You can search and select different related posts for every single individual post.


Features
•   Easy installation
•   Can display different list of related posts
•   Can link list of related posts to each and every distinct posts of default/custom post types
•   Usage of shortcode for displaying Related Posts anywhere you want
•	Support thumbnails of different sizes
• 	Support various post meta like comments, authors, excerpt etc
• 	Translation enabled


== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Download and upload the plugin to the /wp-content/plugins/
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. You can see the 'Related Posts for Distinct Posts' submenu inside the 'Appearance' menu.
4. You can enable the desired post type from plugin's global settings page.
