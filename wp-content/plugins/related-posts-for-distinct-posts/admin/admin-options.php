<?php
if (isset($_POST['rpdp_submit']) && wp_verify_nonce($_POST['_wpnonce'])) {

	$title_length = (is_numeric($_POST['title_length']) ? sanitize_text_field($_POST['title_length']): '');
	$content_limit = (is_numeric($_POST['content_limit']) ? sanitize_text_field($_POST['content_limit']): '10');
	$enabled_posttypes = '';
	if (isset($_POST['enabled_posttypes'])){
		$enabled_posttypes = $_POST['enabled_posttypes'];
	}
	update_option( 'rpdp_enabled_posttypes', $enabled_posttypes );
	$retrieve_post_types = get_post_types(array('public'=>true));
	unset($retrieve_post_types['attachment']);
	foreach($retrieve_post_types as $key => $posttype_name){
		if (isset($_POST[$posttype_name]) && $_POST[$posttype_name] != ""){
			$related_posttypes[$posttype_name] = $_POST[$posttype_name];
			if (!in_array($posttype_name,$related_posttypes[$posttype_name])){
				array_push($related_posttypes[$posttype_name],$posttype_name);
			}
		}else{
			$related_posttypes[$posttype_name] = array($posttype_name);
		}
	}
	if (!isset($_POST['enable_author_link'])){
		$author_link = '';
	}else{
		$author_link = $_POST['enable_author_link'];
	}
	update_option('rpdp_related_posttypes',$related_posttypes);
	$rest_fields = array('title_of_section'=> sanitize_text_field($_POST['title_of_section']),'no_of_posts'=>$_POST['no_of_posts'],'thumbnail_display'=>$_POST['thumbnail_display'],'thumbnail_size'=>$_POST['thumbnail_size'],'thumb_width'=>sanitize_text_field($_POST['thumb_width']),'thumb_height'=>sanitize_text_field($_POST['thumb_height']),'excerpt_display'=>$_POST['excerpt_display'],'content_limit'=>sanitize_text_field($_POST['content_limit']),'title_display'=>$_POST['title_display'],'title_length'=> $title_length,'post_date_display'=>$_POST['post_date_display'],'comment_count_display'=>$_POST['comment_count_display'],'author_display'=>$_POST['author_display'],'enable_author_link'=>$author_link,'link_btn_display'=>$_POST['link_btn_display'],'link_btn_label'=>sanitize_text_field($_POST['link_btn_label']));
	update_option('rpdp_general_options',$rest_fields);
}
$retrieve_options = get_option('rpdp_general_options');
?>
	<div class="rpdp-main-wrapper">
		<h2><?php _e('Related Posts for Distinct Posts','related-posts-for-distinct-posts'); ?></h2>
		<form method="POST" name="rpdp_setting_form" class="rpdp_setting_form">
			<div class="rpdp_content_box">
				<?php wp_nonce_field(); ?>
				<div class="title-no-section rpdp-clearfix">
					<div class="rpdp-form-group">
						<div class="rpdp-form-label">
							<label for="title-of-section"><?php _e('Title of Related Posts Section','related-posts-for-distinct-posts'); ?></label>
						</div>
						<div class="rpdp-form-input">
							<input type="text" value="<?php echo ($retrieve_options['title_of_section'] != "" ? $retrieve_options['title_of_section'] : ""); ?>" name="title_of_section">
						</div>
					</div>

					<div class="rpdp-form-group">
						<div class="rpdp-form-label">
							<label for="no-of-posts"><?php _e('No. of Related Posts to display','related-posts-for-distinct-posts'); ?> </label>
						</div>
						<div class="rpdp-form-input rpdp-form-select">
							<select name="no_of_posts"><?php
								for($no = 1; $no <= 4; $no++){ ?>
									<option value="<?php echo $no; ?>" <?php echo ( ( $retrieve_options['no_of_posts'] == $no) ? 'selected' : '' ); ?> ><?php echo $no; ?></option><?php
								} ?>
							</select>
						</div>
					</div>
				</div>
				<div class="rpdp-posttypes">
					<div class="rpdp-form-group enable-pt">
						<div class="rpdp-form-label">
							<label for="enable-for-posttype"><?php _e('Enable Related Posts for:','related-posts-for-distinct-posts'); ?></label>
						</div>
						<div class="rpdp-form-input rpdp-clearfix">
						<?php $arg = array('public'=>true); $post_types = get_post_types($arg);
						unset($post_types['attachment']);
						$retrieve_enabled = get_option('rpdp_enabled_posttypes');
						foreach($post_types as $key => $post_type_name){ ?>
							<div class="rpdp-form-checkbox">
							<input type="checkbox" value="<?php echo $post_type_name; ?>" name="enabled_posttypes[]" <?php echo ($retrieve_enabled != '' ? (in_array($post_type_name,$retrieve_enabled) ? 'checked':'') : ''); ?> > <?php echo $post_type_name; ?></div>
							<?php
						}?>
						</div>
					</div>
					<div class="rpdp-related-posttype-fields">
						<?php 	$retrieve_related_posttype = get_option('rpdp_related_posttypes');
								foreach($post_types as $key => $post_type_name){
									$related_posttype_selection = $post_type_name; ?>
									<div class="rpdp-form-group <?php echo (in_array($post_type_name,$retrieve_enabled)?'':'field_hidden'); ?> pt-<?php echo $post_type_name; ?>">
										<div class="rpdp-form-label">
											<label for=""><?php printf(__('Select related post types for %s:','related-posts-for-distinct-posts'), $post_type_name); ?></label>
										</div>
										<div class="rpdp-form-input rpdp-clearfix">
											<?php
											foreach($post_types as $key => $posttype_name){ ?>
												<div class="rpdp-form-checkbox">
												<input type="checkbox" value="<?php echo $posttype_name; ?>" <?php echo ((($related_posttype_selection == $posttype_name) || ( (isset($retrieve_related_posttype[$related_posttype_selection]) && $retrieve_related_posttype[$related_posttype_selection] != '' && in_array($post_type_name,$retrieve_enabled) )  ? (in_array($posttype_name,$retrieve_related_posttype[$related_posttype_selection])):'')) ? 'checked' : ''); ?> name="<?php echo $related_posttype_selection; ?>[]"> <?php echo $posttype_name; ?> </div>
												<?php
											}?>
										</div>
									</div>
						<?php  	} ?>
					</div>

				</div>
			</div>
			<div class="rpdp-tab">
				<button class="rpdp-tablinks defaultOpen active" data-id="rpdp-tab1"><?php _e('Thumbnail','related-posts-for-distinct-posts'); ?></button>
				<button class="rpdp-tablinks" data-id="rpdp-tab2"><?php _e('Other Components settings','related-posts-for-distinct-posts');?></button>
			</div>
			<div class="rpdp-tabcontent" id="rpdp-tab1">
				<div class="rpdp-form-group rpdp-thumbnail-select">
					<div class="rpdp-form-label">
						<label for="thumbnail" class="rpdp-h4"><?php _e('Thumbnail','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-input rpdp-form-select">
						<select name="thumbnail_display">
							<option value="show" <?php echo ($retrieve_options['thumbnail_display'] == "show" ? "selected" : ""); ?> ><?php _e('Show','related-posts-for-distinct-posts'); ?></option>
							<option value="hide" <?php echo ($retrieve_options['thumbnail_display'] == "hide" ? "selected" : ""); ?> ><?php _e('Hide','related-posts-for-distinct-posts'); ?></option>
						</select>
					</div>
				</div>

				<div class="rpdp-form-group">
					<div class="rpdp-form-label">
						<label for="thumbnail-size" class="rpdp-h4"><?php _e('Thumbnail size','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-input rpdp-clearfix">
						<?php global $_wp_additional_image_sizes;
						foreach (get_intermediate_image_sizes() as $image_size_names){ ?>
							<div class="rpdp-form-checkbox">
							<input type="radio" value="<?php echo $image_size_names; ?>" name="thumbnail_size" <?php echo ($retrieve_options['thumbnail_size']==$image_size_names ? 'checked':''); ?> ><?php echo $image_size_names.' '.'<div class="rpdp-image-size-pixel">('.(get_option("{$image_size_names}_size_w")!='' ? get_option("{$image_size_names}_size_w"): $_wp_additional_image_sizes[$image_size_names]['width']).'X'.(get_option("{$image_size_names}_size_h")!="" ? get_option("{$image_size_names}_size_h"): $_wp_additional_image_sizes[$image_size_names]['height']).')</div>'; ?></div>
						<?php
						} ?>
						<div class="rpdp-form-custom-size rpdp-clearfix">
							<input type="radio" value="custom" name="thumbnail_size" <?php echo ($retrieve_options['thumbnail_size']=='custom' ? 'checked':''); ?>><?php _e('custom size','related-posts-for-distinct-posts'); ?>
						</div>
					</div>

					<div class="rpdp-form-group rpdp-custom-size <?php echo ($retrieve_options['thumbnail_size']=='custom' ? '' :'field_hidden'); ?>">
						<div class="rpdp-form-label">
							<label for="thumbnail-size"><?php _e('Width X Height','related-posts-for-distinct-posts'); ?></label>
						</div>
						<div class="rpdp-form-input">
							<input type="number" min="0" name="thumb_width" value="<?php echo ( (isset($retrieve_options['thumb_width']) && $retrieve_options['thumb_width'] != "") ? $retrieve_options['thumb_width'] : ""); ?>" > X <input type="number" min="0" name="thumb_height" value="<?php echo ( (isset($retrieve_options['thumb_height']) && $retrieve_options['thumb_height'] != "") ? $retrieve_options['thumb_height'] : ""); ?>" >
						</div>
					</div>

				</div>
			</div>
			<div class="rpdp-tabcontent rpdp-post-excerpt rpdp-clearfix" id="rpdp-tab2">
				<div class="rpdp-form-label">
					<label for="post-title" class="rpdp-h4"><?php _e('Post Settings','related-posts-for-distinct-posts'); ?></label>
				</div>
				<div class="rpdp-form-group">
					<div class="rpdp-form-label">
						<label for="title-length"><?php _e('Post Title','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-input rpdp-form-select">
						<select name="title_display">
							<option value="show" <?php echo ($retrieve_options['title_display'] == "show" ? "selected" : ""); ?>><?php _e('Show','related-posts-for-distinct-posts'); ?></option>
							<option value="hide" <?php echo ($retrieve_options['title_display'] == "hide" ? "selected" : ""); ?> ><?php _e('Hide','related-posts-for-distinct-posts'); ?></option>
						</select>
					</div>
				</div>

				<div class="rpdp-form-group">
					<div class="rpdp-form-label">
						<label for="title-length"><?php _e('Maximum length of Post Title','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-input">
						<input type="number" min="0" value="<?php echo ($retrieve_options['title_length'] != "" ? $retrieve_options['title_length'] : ""); ?>" name="title_length">
					</div>
				</div>

				<div class="rpdp-form-group">
					<div class="rpdp-form-label">
						<label for="date"><?php _e('Date','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-input rpdp-form-select">
						<select name="post_date_display">
							<option value="show" <?php echo ($retrieve_options['post_date_display'] == "show" ? "selected" : ""); ?> ><?php _e('Show','related-posts-for-distinct-posts'); ?></option>
							<option value="hide" <?php echo ($retrieve_options['post_date_display'] == "hide" ? "selected" : ""); ?> ><?php _e('Hide','related-posts-for-distinct-posts'); ?></option>
						</select>
					</div>
					<span><?php _e('Date format will be same as in General settings','related-posts-for-distinct-posts'); ?></span>
				</div>

				<div class="rpdp-form-group">
					<div class="rpdp-form-label">
						<label for="comment_count"><?php _e('Comment Count','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-input rpdp-form-select">
						<select name="comment_count_display">
							<option value="show" <?php echo ($retrieve_options['comment_count_display'] == "show" ? "selected" : ""); ?> ><?php _e('Show','related-posts-for-distinct-posts'); ?></option>
							<option value="hide" <?php echo ($retrieve_options['comment_count_display'] == "hide" ? "selected" : ""); ?> ><?php _e('Hide','related-posts-for-distinct-posts'); ?></option>
						</select>
					</div>
				</div>

				<div class="rpdp-form-group">
					<div class="rpdp-form-label">
						<label for="author">Author</label>
					</div>
					<div class="rpdp-form-input rpdp-form-select">
						<select name="author_display">
							<option value="show" <?php echo ($retrieve_options['author_display'] == "show" ? "selected" : ""); ?> ><?php _e('Show','related-posts-for-distinct-posts'); ?></option>
							<option value="hide" <?php echo ($retrieve_options['author_display'] == "hide" ? "selected" : ""); ?> ><?php _e('Hide','related-posts-for-distinct-posts'); ?></option>
						</select>
					</div>
				</div>

				<div class="rpdp-form-group">
					<div class="rpdp-form-label">
						<label for="author"><?php _e('Enable Author Link','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-input">
						<input type="checkbox" name="enable_author_link" value="yes" <?php echo ( (isset($retrieve_options['enable_author_link']) && $retrieve_options['enable_author_link'] != '') ? 'checked' : ''); ?> ><?php _e('Tick to enable link','related-posts-for-distinct-posts'); ?>
					</div>
				</div>
				<div class="excerpt rpdp-clearfix">
					<div class="rpdp-form-label">
						<label for="excerpt" class="rpdp-h4"><?php _e('Excerpt Settings','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-group">
						<div class="rpdp-form-label">
							<label for="title-length"><?php _e('Excerpt','related-posts-for-distinct-posts'); ?></label>
						</div>
						<div class="rpdp-form-input rpdp-form-select">
							<select name="excerpt_display">
								<option value="show" <?php echo ($retrieve_options['excerpt_display'] == "show" ? "selected" : ""); ?> ><?php _e('Show','related-posts-for-distinct-posts'); ?></option>
								<option value="hide" <?php echo ($retrieve_options['excerpt_display'] == "hide" ? "selected" : ""); ?> ><?php _e('Hide','related-posts-for-distinct-posts'); ?></option>
							</select>
						</div>
					</div>

					<div class="rpdp-form-group">
						<div class="rpdp-form-label">
							<label for="content-limit"><?php _e('Excerpt Word limit(in words)','related-posts-for-distinct-posts'); ?></label>
						</div>
						<div class="rpdp-form-input">
							<input type="number" min="0" value="<?php echo ($retrieve_options['content_limit'] != "" ? $retrieve_options['content_limit'] : ""); ?>" name="content_limit">
						</div>
					</div>
				</div>

				<div class="read-more rpdp-clearfix">
					<div class="rpdp-form-label">
						<label for="read-more" class="rpdp-h4"><?php _e('Button Settings','related-posts-for-distinct-posts'); ?></label>
					</div>
					<div class="rpdp-form-group">
						<div class="rpdp-form-label">
							<label for="title-length"><?php _e('Display Link Button','related-posts-for-distinct-posts'); ?></label>
						</div>
						<div class="rpdp-form-input rpdp-form-select">
							<select name="link_btn_display">
								<option value="show" <?php echo ($retrieve_options['link_btn_display'] == "show" ? "selected" : ""); ?> ><?php _e('Show','related-posts-for-distinct-posts'); ?></option>
								<option value="hide" <?php echo ($retrieve_options['link_btn_display'] == "hide" ? "selected" : ""); ?> ><?php _e('Hide','related-posts-for-distinct-posts'); ?></option>
							</select>
						</div>
					</div>

					<div class="rpdp-form-group">
						<div class="rpdp-form-label">
							<label for="content-limit"><?php _e('Label for link button','related-posts-for-distinct-posts'); ?></label>
						</div>
						<div class="rpdp-form-input">
							<input type="text" value="<?php echo ($retrieve_options['link_btn_label'] != "" ? $retrieve_options['link_btn_label'] : ""); ?>" name="link_btn_label">
						</div>
					</div>
				</div>
			</div>
			<div class="rpdp-form-group">
				<div class="rpdp-form-input rpdp_submit_btn">
					<input type="submit" value="save" name="rpdp_submit" class="rpdp_submit">
				</div>
			</div>
		</form>
	</div>