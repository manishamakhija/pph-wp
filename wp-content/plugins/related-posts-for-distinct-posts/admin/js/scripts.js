jQuery(document).ready(function(jQuery){
	jQuery('#rpdp-tab1').show();
});
	
	// tabs button click prevent
	jQuery(document).on('click','.rpdp_setting_form button.rpdp-tablinks',function(e){
		e.preventDefault();
	})

	// related post type selection hide/show
	jQuery(document).on('click','.rpdp_setting_form .enable-pt .rpdp-form-input input[type="checkbox"]',function(){
		var value = jQuery(this).val();
		
		if (jQuery(this).prop('checked') == true){
			jQuery('.pt-'+value).show();
		}
		else if (jQuery(this).prop('checked') == false){
			jQuery('.pt-'+value).hide();	
		}
	})

		//tabs
	jQuery(document).on('click','.rpdp-tab .rpdp-tablinks',function(evt){
		var tabName = jQuery(this).attr('data-id');

	  	// Get all elements with class="tabcontent" and hide them
	  	jQuery('.rpdp-tabcontent').each(function(){
	  		jQuery(this).css({
	  			display: 'none'
	  		});
	  	})

	  	// Get all elements with class="tablinks" and remove the class "active"
	  	jQuery('.rpdp-tablinks').removeClass('active');

	  	// Show the current tab, and add an "active" class to the button that opened the tab
	  	jQuery('#'+tabName).css({display:'block'});
 	 	jQuery(this).addClass('active');
	})

	// custom size thumbnail
	jQuery(document).on('click','.rpdp_setting_form input[type="radio"]',function(){
		if (jQuery(this).val() == 'custom'){
			jQuery('.rpdp-custom-size').show();
		}else{
			jQuery('.rpdp-custom-size').hide();
		}
	})

	// Add (+) related posts
	jQuery(document).on('click','.rpdp-posts-lists-div li',function(e){
		e.preventDefault();
		var selectedPost = jQuery(this).html();
		var dataId = jQuery(this).find('a').attr('data-post_id');
		jQuery('.rpdp-selected-posts-div ul').append('<li>'+selectedPost+'</li>');
		jQuery('.rpdp-selected-posts-div [data-post_id="'+dataId+'"] span.rpdp-button-add').addClass('rpdp-button-remove');
		jQuery('.rpdp-selected-posts-div [data-post_id="'+dataId+'"] span').removeClass('rpdp-button-add');
		jQuery('.rpdp-selected-posts-div ul li [data-post_id="'+dataId+'"]').append('<input type="hidden" name="display_posts[]" value="'+dataId+'">');
		jQuery(this).addClass('disabled');
	})

	// Remove (-) related posts
	jQuery(document).on('click','.rpdp-selected-posts-div li',function(e){
		e.preventDefault();
		var dataId = jQuery(this).find('a').attr('data-post_id');
		jQuery(this).remove();
		jQuery('.rpdp-posts-lists-div a[data-post_id="'+dataId+'"]').parent('li').removeClass('disabled');
	})


	// filter related posts by dropdown 
	jQuery(document).on('change','select[name="connected_posttypes"]',function(){
		var selectedPosttype = jQuery(this).val();
		var searchText = jQuery('input[name="rpdp_search"]').val();
		jQuery('.rpdp-posts-lists-div ul').empty();
	    var send_data = {
	        action: "rpdp_retrieve_posts",
	        sort: "dropdownSelect",
	        searchtext: searchText, 
	        selectedposttype: selectedPosttype,
	        currentpost: jQuery('input[name="rpdp_post_id"]').val()
        }

       jQuery.post(rpdp_ajax_object.ajax_url, send_data, function( response ) {
        var ajax_result = jQuery.parseJSON(response);
        // alert('test');
        if(ajax_result.result == '1'){
          jQuery('.rpdp-posts-lists-div ul').append('<li>'+ajax_result.content+'</li>');
        }else{
        	jQuery('.rpdp-posts-lists-div ul').append('<li>'+ajax_result.message+'</li>');
        }    

      }); 
	})

	// filter related posts by search 
	var typingTimer;                //timer identifier
	var doneTypingInterval = 1000;  //time in ms, 5 second for example

	//on keyup, start the countdown
	jQuery('input[name="rpdp_search"]').keyup(function(){
	    clearTimeout(typingTimer);
	    if (jQuery(this).val) {
	        typingTimer = setTimeout(function(){
	            jQuery('.rpdp-posts-lists-div ul').empty();
				 var send_data = {
			        action: "rpdp_retrieve_posts",
			        sort: "searchText",
			        searchtext: jQuery('input[name="rpdp_search"]').val(),
			        selectedposttype: jQuery('select[name="connected_posttypes"]').val(),
			        currentpost: jQuery('input[name="rpdp_post_id"]').val()
		        }

		       jQuery.post(rpdp_ajax_object.ajax_url, send_data, function( response ) {
		        var ajax_result = jQuery.parseJSON(response);
		        if(ajax_result.result == '1'){
		          jQuery('.rpdp-posts-lists-div ul').append('<li>'+ajax_result.content+'</li>');
		        }else{
		        	jQuery('.rpdp-posts-lists-div ul').append('<li>'+ajax_result.message+'</li>');
		        }    

		      }); 

	        }, doneTypingInterval);
	    }
	});