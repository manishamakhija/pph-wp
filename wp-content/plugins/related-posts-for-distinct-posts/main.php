<?php
class Related_posts_for_distinct_posts{

	function __construct(){
		// if(isset($_GET['page']) && $_GET['page'] == 'related_posts_for_distinct_posts_options'){
        	add_action('admin_enqueue_scripts', array($this,'include_scripts'));
        // }

        // adding metabox in enabled post types
        add_action('admin_menu', array($this,'rpdp_distinct_posts_selection'));
        // image size for thumbnail in admin post page of listing
        add_image_size('list_icon',24,24,true);
        // adding shortcode for displaying related posts on frontside
        add_shortcode('rpdp_related_posts',array($this, 'display_related_posts'));
        // save selected related posts in admin posts page
        add_action('save_post', array($this, 'save_rpdp_distinct_posts_selection'));
        // include frontend styles and scripts
        add_action( 'wp_enqueue_scripts', array($this, 'rpdp_frontend_scripts' ));
        // delete posts
        add_action('wp_trash_post', array($this, 'rpdp_wp_trash_post'));
	}

	// admin side include scripts and styles
	function include_scripts(){
		wp_enqueue_style('rpdp-admin-css', RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL . 'admin/css/rpdp-admin.css');
		wp_enqueue_style('rpdp-media-admin-css', RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL . 'admin/css/rpdp-media-admin.css');
		wp_enqueue_script('rpdp-admin-script', RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL . 'admin/js/scripts.js', array(),false,true);
		wp_localize_script( 'rpdp-admin-script', 'rpdp_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}

	function rpdp_frontend_scripts(){
		wp_enqueue_style('rpdp-frontend-styles', RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL . '/css/rpdp-frontend-style.css');
		wp_enqueue_style('rpdp-frontend-media-styles', RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL . '/css/rpdp-frontend-media.css');
	    wp_enqueue_script('rpdp-frontend-scripts', RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL . '/js/rpdp-frontend-script.js',array('jquery'),'',true);
	}

	function rpdp_distinct_posts_selection(){
		$retrieve_enable_posttype = get_option('rpdp_enabled_posttypes');
		if (!empty($retrieve_enable_posttype)){
			foreach($retrieve_enable_posttype as $active_posttype ){
			      add_meta_box(
			        'rpdp-distinct-posts-div',
			        __( 'Select Related Posts','related-posts-for-distinct-posts' ),
			        array($this,'related_posts_selection_callback'),$active_posttype,'normal','high'
			      );
			}
		}
	}

	function related_posts_selection_callback( $post ){
		$current_posttype = get_post_type($post->ID);
		$all_connected_pt = get_option('rpdp_related_posttypes');
		$connected_posttype = $all_connected_pt[$current_posttype];
		$retrieve_save_posts = get_post_meta($post->ID,'rpdp_connected_posts',true);
		?>
		<div class="rpdp_search_posts_box rpdp-clearfix">
			<div class="rpdp_search_posts_box_left">
				<div class="rpdp_posts_box">
					<div class="rpdp_search_posts rpdp-clearfix">
						<div class="search-posts"><div class="rpdp-form-input"><input type="text" name="rpdp_search" placeholder="<?php _e('search...','related-posts-for-distinct-posts'); ?>"></div>
						<input type="hidden" name="rpdp_post_id" value="<?php echo $post->ID; ?>"></div>
						<div class="posttype-select rpdp-form-select"><select name="connected_posttypes">
							<option value=""><?php _e('Select Post type','related-posts-for-distinct-posts'); ?></option>
						<?php
							foreach($connected_posttype as $connect_pt){
								echo '<option value="'.$connect_pt.'">'.$connect_pt.'</option>';
							}
						?>
						</select></div>
					</div>
					<div class="rpdp-posts-lists-div rpdp-posts-div">
						<div class="rpdp-posts-list-heading rpdp-clearfix">
							<div class="rpdp-name-block"><?php _e('Name','related-posts-for-distinct-posts'); ?></div>
							<div class="rpdp-category-block"><?php _e('Category','related-posts-for-distinct-posts'); ?></div>
						</div>
						<div class="rpdp_posts_box_list">
							<ul>
								<?php
								 foreach($connected_posttype as $connect){
									$query = new WP_Query(array('post_type'=>$connect,'posts_per_page'=>-1,'order'=>'ASC','post__not_in'=>array($post->ID)));
									while($query->have_posts()):$query->the_post();
										$saved = ($retrieve_save_posts != "" ? (in_array(get_the_id(),$retrieve_save_posts) ? 'class="disabled"' :'') : '');
										echo '<li '.$saved.'>
												<a href="'.get_permalink().'" data-post_id="'.get_the_id().'" class="rpdp-clearfix">
													<div class="rpdp-posts-name-left">
														<div class="item-thumbnail">'.get_the_post_thumbnail(get_the_id(),'list_icon').'</div>
														<span>'.get_the_title().'</span>
													</div>
													<div class="rpdp-posts-category-right">
														<span class="item-info">'.$connect.'</span>
														<span class="rpdp-button-add"></span>
													</div>
												</a>
											</li>';
									endwhile;
								} ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="rpdp_search_posts_box_right">
				<div class="rpdp_posts_box">
					<div class="rpdp-selected-posts-div rpdp-posts-div">
						<div class="rpdp-posts-list-heading rpdp-clearfix">
							<div class="rpdp-name-block"><?php _e('Name','related-posts-for-distinct-posts'); ?></div>
							<div class="rpdp-category-block"><?php _e('Category','related-posts-for-distinct-posts'); ?></div>
						</div>
						<div class="rpdp_posts_box_list">
							<ul>
								<?php
								if ($retrieve_save_posts){
									foreach($retrieve_save_posts as $key => $id){
										echo '<li>
												<a href="'.get_permalink($id).'" data-post_id="'.$id.'" class="rpdp-clearfix">
													<div class="rpdp-posts-name-left">
														<div class="item-thumbnail">'.get_the_post_thumbnail($id,'list_icon').'</div>
														<span>'.get_the_title($id).'</span>

													</div>
													<div class="rpdp-posts-category-right">
														<span class="item-info">'.get_post_type($id).'</span>
														<span class="rpdp-button-remove"></span>
													</div>
												</a>
												<input type="hidden" value="'.$id.'" name="display_posts[]">
											</li>';
									}
								} ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	//save metabox value of select distinct posts
	function save_rpdp_distinct_posts_selection($post_id){
		if (isset($_POST['display_posts'])){
			$save_selected_post = $_POST['display_posts'];
		}else{
			$save_selected_post = '';
		}
		update_post_meta( $post_id, 'rpdp_connected_posts', $save_selected_post );
	}

	// remove posts while trashing 
	function rpdp_wp_trash_post ($post_id) {
    $posts_having_rpdp = new WP_Query(array('meta_key'  => 'rpdp_connected_posts','post_type'=>'any'));
    while($posts_having_rpdp->have_posts()):$posts_having_rpdp->the_post();
    	$connected_related_posts = ''; $connection_exists = false;
    	$connected_related_posts = get_post_meta(get_the_id(),'rpdp_connected_posts',true);
    	$connection_exists = array_search($post_id,$connected_related_posts);
    	if ($connection_exists === false){
    	}else{
			array_splice($connected_related_posts,$connection_exists,1);
	       	update_post_meta(get_the_id(),'rpdp_connected_posts',$connected_related_posts );
    	} 
    endwhile; wp_reset_postdata();
	}
	

	function display_related_posts(){
		global $post;
		$general_options = get_option('rpdp_general_options');
		$post_count = ($general_options['no_of_posts'] != "" ? $general_options['no_of_posts'] : '3');
		$selected_posts = get_post_meta($post->ID,'rpdp_connected_posts',true);

		if (empty($selected_posts)){
			$current_post_type = get_post_type($post->ID);
			$rpdp_query = new WP_Query(array('post_type'=>$current_post_type,'posts_per_page'=>$post_count,'orderby' => 'rand','post__not_in'=>array($post->ID)));
			$selected_posts = array();
			while($rpdp_query->have_posts()):$rpdp_query->the_post();
				array_push($selected_posts,get_the_id());
			endwhile; wp_reset_postdata();
		}
		// checking for enabled post types
		$current_posttype = get_post_type($post->ID);
		$all_enabled_pt = (!empty(get_option('rpdp_enabled_posttypes')) ? get_option('rpdp_enabled_posttypes') : array());
		if (!in_array($current_posttype,$all_enabled_pt)){
			echo '<h4>'.__('Please Enable this Post type from admin settings.','related-posts-for-distinct-posts').'</h4>';
		}else{
			if (!empty($selected_posts)){
				ob_start();
			?>
				<div class="rpdp-display-posts-wrapper">
					<?php 
					if ($general_options['title_of_section'] != ""){ 	 ?>
						<div class="rpdp_display_posts_title">
							<h3><?php echo $general_options['title_of_section']; ?></h3>
						</div><?php 
					} ?>
					<div class="rpdp-display-posts">
						<ul class="rpdp_<?php echo $post_count; ?>_column_box d-flex flex-wrap"> <?php  $count = 1;
								foreach($selected_posts as $selected_post_id){
									if ($count <= $post_count){ ?>
										<li><?php global $_wp_additional_image_sizes;
											if ($general_options['thumbnail_display'] == 'show'){ ?>
												<div class="rpdp-feature-thumbnail thumb-image" > <?php
													$image_attr = self::get_image_attributes($selected_post_id,$general_options); ?>
													<img src="<?php echo $image_attr['src']; ?>" width="<?php echo $image_attr['width']; ?>"  height="<?php echo $image_attr['height']; ?>" class="pph-thumb-img">
												</div><?php
											} ?>
												<div class="rpdp-posts-meta-wrap"> <?php
													if ($general_options['author_display'] == 'show' && post_type_supports( get_post_type($selected_post_id), 'author' ) ){
														$user_info = get_user_by('ID',get_post_field('post_author',$selected_post_id));
														$author_link = '<a href="'.get_author_posts_url( get_post_field('post_author',$selected_post_id) ).'">'.$user_info->display_name.'</a>';
														?>
														<div class="rpdp-author"><?php
															_e('By ','related-posts-for-distinct-posts');  
															echo ( (isset($general_options['enable_author_link']) && $general_options['enable_author_link'] != '') ?  $author_link : $user_info->display_name); ?>
														</div>
													<?php
													}

													if ($general_options['comment_count_display'] == 'show' && post_type_supports( get_post_type($selected_post_id), 'comments' ) ){  ?>
														<div class="rpdp-comments"><?php
														$comment_count = get_comments_number( $selected_post_id );
														echo $comment_count.' '. _n( 'Comment', 'Comments', $comment_count );
															?>
														</div>
													<?php
													} ?>
												</div>

											<?php
											if ($general_options['title_display'] == 'show'){ ?>
												<div class="rpdp-post-title thumb-text">
													<h4><a href="<?php echo get_permalink($selected_post_id); ?>"><?php echo ($general_options['title_length'] != "" ? (wp_trim_words(get_the_title($selected_post_id),$general_options['title_length'])): get_the_title($selected_post_id)); ?></a></h4>
													<div class="article-details">
														<?php $post_categories = wp_get_post_categories( $post->ID ); $count_cat = count($post_categories); $i = 1;
															foreach($post_categories as $cat_id){
																echo '<a href="'.get_category_link($cat_id).'">'.get_cat_name($cat_id).'</a>';
																echo $count_cat == $i ? '' : ', ';
																$i++;
															} 
															if ($general_options['post_date_display'] == 'show'){  ?>
																<span class="separator">|</span>
																<span><?php $date_format = get_option('date_format');
																	echo get_the_date($date_format,$selected_post_id); ?></span><?php 
															} ?>
													</div>
												</div>
											<?php
											} ?>

										 	<?php
											if ($general_options['excerpt_display'] == 'show'){ ?>
												<div class="rpdp-post-excerpt">
													<?php
														if ( has_excerpt ( $selected_post_id ) ){
															echo ($general_options['content_limit']!='' ? wp_trim_words(get_the_excerpt($selected_post_id),$general_options['content_limit']) : get_the_excerpt($selected_post_id) );
														}else{
															$content_post = get_post($selected_post_id);
															echo ($general_options['content_limit']!='' ?wp_trim_words($content_post->post_content,$general_options['content_limit']) : $content_post->post_content );
														}
													?>
												</div>
											<?php
											} ?>

											<?php 
											if ($general_options['link_btn_display'] == 'show'){ ?>
												<div class="rpdp-read-more"><a href="<?php echo get_permalink($selected_post_id); ?>"><?php printf(__('%s','related-posts-for-distinct-posts'),($general_options['link_btn_label'] !='' ? $general_options['link_btn_label'] : 'Read More')); ?></a></div>
												<?php 
											}  ?>

										</li> <?php
									}
									$count++;
								}
							?>
						</ul>
					</div>
				</div>
			<?php
			return ob_get_clean();
			}
		}
	}

	private function get_image_attributes($selected_post_id,$general_options){
		global $_wp_additional_image_sizes;
	 	$width =  ($general_options['thumbnail_size'] != 'custom' ? ( (isset($_wp_additional_image_sizes[$general_options['thumbnail_size']]['width']) && $_wp_additional_image_sizes[$general_options['thumbnail_size']]['width'] != '' ) ? $_wp_additional_image_sizes[$general_options['thumbnail_size']]['width'] : get_option("{$general_options['thumbnail_size']}_size_w")) : $general_options['thumb_width'] );
	 	$height =  ($general_options['thumbnail_size'] != 'custom' ? ( (isset($_wp_additional_image_sizes[$general_options['thumbnail_size']]['height']) && $_wp_additional_image_sizes[$general_options['thumbnail_size']]['height'] != '') ? $_wp_additional_image_sizes[$general_options['thumbnail_size']]['height'] : get_option("{$general_options['thumbnail_size']}_size_h")) : $general_options['thumb_height'] );
	 	$src = (get_the_post_thumbnail_url($selected_post_id) != "" ? get_the_post_thumbnail_url($selected_post_id,($general_options['thumbnail_size']!='custom' ? $general_options['thumbnail_size'] : array($general_options['thumb_width'],$general_options['thumb_height']))) : apply_filters('rpdp_add_default_thumbnail',self::get_image_cropped($width,$height)));
	 	$image_attr = array('src'=>$src,'width'=>$width,'height'=>$height);
	 	return $image_attr;
	}

	private function get_image_cropped($width,$height){
		$new_image = 'default-'.$width.'x'.$height.'.jpg';
		$dir_path = WP_CONTENT_DIR.'/default-images/';
		if (!is_dir($dir_path) && !wp_mkdir_p($dir_path) ){
			mkdir($dir_path, octdec("0777"),true);
		}
		if (!is_writable($dir_path)){
			chmod($dir_path, octdec("0777"));
		}
		if (is_dir($dir_path) && is_writable($dir_path)){
			$img = wp_get_image_editor( RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL.'images/placeholder.png' );
			if ( ! is_wp_error( $img ) ) {
				$img->resize( $width, $height, true );
				$img_path = $dir_path.$new_image;
				$img->save($img_path);
				return content_url().'/default-images/'.$new_image;
			}
		}else{
			return RELATED_POSTS_FOR_DISTINCT_POSTS_PLUGIN_URL.'images/placeholder.png';
		}
	}
}

// object create
$related_posts_obj = new Related_posts_for_distinct_posts();

add_action( 'wp_ajax_rpdp_retrieve_posts', 'rpdp_retrieve_posts' );
add_action( 'wp_ajax_nopriv_rpdp_retrieve_posts', 'rpdp_retrieve_posts' );
function rpdp_retrieve_posts() {
	$post_id = sanitize_text_field( $_POST['currentpost'] );
	$related_posttypes = get_option('rpdp_related_posttypes');
	$current_posttype = get_post_type($post_id);
	$connected_posttypes = $related_posttypes[$current_posttype];
	$searchresult = sanitize_text_field( $_POST['searchtext'] );
	if (isset($_POST['sort']) && $_POST['sort'] == 'searchText' ){
		$resultant_posts = array();
	}
	$all_posts = '';
	foreach($connected_posttypes as $connect_pt){
		$retrieve_posts_query = new WP_Query(array('post_type'=>$connect_pt,'posts_per_page'=>-1,'order'=>'ASC','post__not_in'=>array($post_id)));
		if ( (isset($_POST['sort']) && $_POST['sort'] == 'searchText') || ( $_POST['sort'] == 'dropdownSelect' && $_POST['selectedposttype'] == "" ) ){
			// if ($_POST['selectedposttype'] == ""){
				$retrieve_posts_query = new WP_Query(array('post_type'=>$connect_pt,'posts_per_page'=>-1,'order'=>'ASC','post__not_in'=>array($post_id),'s'=>$searchresult));	
			// }
		}
		while($retrieve_posts_query->have_posts()):$retrieve_posts_query->the_post();
			if (isset($_POST['sort']) && $_POST['sort'] == 'searchText' ){
				if ($_POST['selectedposttype'] != "" && $_POST['selectedposttype'] == $connect_pt ){
					array_push($resultant_posts,get_the_id());
				}
				
				// if (stripos(get_the_title(),$searchresult) !== false){
				elseif ($_POST['selectedposttype'] == "")
					array_push($resultant_posts,get_the_id());
				// }
			}
			$all_posts .= 	'<li>
								<a href="'.get_permalink().'" data-post_id="'.get_the_id().'" class="rpdp-clearfix">
									<div class="rpdp-posts-name-left">
										<div class="item-thumbnail">'.get_the_post_thumbnail(get_the_id(),'list_icon').'</div>
										<span>'.get_the_title().'</span>
									</div>
									<div class="rpdp-posts-category-right">
										<span class="item-info">'.$connect_pt.'</span>
										<span class="rpdp-button-add"></span>
									</div>
								</a>
							</li>';

		endwhile; wp_reset_postdata();
	}
	if (isset($_POST['sort']) && $_POST['sort'] == 'dropdownSelect'){
		$selectedPosttype = sanitize_text_field( $_POST['selectedposttype'] );
		$searchtext = sanitize_text_field( $_POST['searchtext'] );
		// $result_posts_list = array();
		if ($selectedPosttype != ''){
			if ($searchtext != ""){
				$query = new WP_Query(array('post_type'=>$selectedPosttype,'posts_per_page'=>-1,'order'=>'ASC','post__not_in'=>array($post_id),'s'=>$searchtext));
			}else{
				$query = new WP_Query(array('post_type'=>$selectedPosttype,'posts_per_page'=>-1,'order'=>'ASC','post__not_in'=>array($post_id)));
			}
			$posts_list = '';
			if ($query->have_posts()){
				while($query->have_posts()):$query->the_post();
					$posts_list	.=  '<li>
										<a href="'.get_permalink().'" data-post_id="'.get_the_id().'" class="rpdp-clearfix">
											<div class="rpdp-posts-name-left">
												<div class="item-thumbnail">'.get_the_post_thumbnail(get_the_id(),'list_icon').'</div>
												<span>'.get_the_title().'</span>
											</div>
											<div class="rpdp-posts-category-right">
												<span class="item-info">'.$selectedPosttype.'</span>
												<span class="rpdp-button-add"></span>
											</div>
										</a>
									</li>';
				endwhile;
				$msg = array('result'=>'1', 'message'=>__('Success','related-posts-for-distinct-posts'), 'content'=>$posts_list);
			}else{
				$msg = array('result'=>'0', 'message'=>__('No post found','related-posts-for-distinct-posts') );
			}
		}else{
				$msg = array('result'=>'1', 'message'=>__('Success','related-posts-for-distinct-posts'), 'content'=>$all_posts);
			}
	}elseif (isset($_POST['sort']) && $_POST['sort'] == 'searchText' ){

		$resultant_html = '';
		if (!empty($resultant_posts)){
			foreach($resultant_posts as $key => $ids){
				$resultant_html .= '<li>
										<a href="'.get_permalink($ids).'" data-post_id="'.$ids.'" class="rpdp-clearfix">
											<div class="rpdp-posts-name-left">
												<div class="item-thumbnail">'.get_the_post_thumbnail($ids,'list_icon').'</div>
												<span>'.get_the_title($ids).'</span>
											</div>
											<div class="rpdp-posts-category-right">
												<span class="item-info">'.get_post_type($ids).'</span>
												<span class="rpdp-button-add"></span>
											</div>
										</a>
									</li>';
			}
		}elseif ($searchresult == "" && $_POST['selectedposttype'] == ""){   // for empty search, display all posts
			$resultant_html = $all_posts;
		}else{
			$resultant_html = '<li>'.__('No posts matching your search found','related-posts-for-distinct-posts').'</li>';
		}
		$msg = array('result'=>'1','message'=>__('search successfully done','related-posts-for-distinct-posts'),'content'=>$resultant_html);
	}
	else{
		$msg = array('result'=>'0', 'message'=>__('Unauthorized Access','related-posts-for-distinct-posts') );
	}
	echo json_encode($msg);
	exit;
}